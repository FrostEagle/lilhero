package com.fininsight.littlehero;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fininsight.littlehero.extras.Constants;
import com.gigamole.navigationtabbar.ntb.NavigationTabBar;

public class PaidActivity extends AppCompatActivity implements View.OnClickListener{
    private Animation animationMoveFromBottom, animationMoveFromBottomPaid;
    private FragmentManager fragmentManager;
    private ViewPager viewPager;
    private NavigationTabBar navigationTabBar;
    private RelativeLayout rlPaid;
    private ImageView imgPaidBtnExpert, imgAcceptExpert;
    private ImageView imgPaidBtnNoExpert, imgAcceptNoExpert;
    //This view paid subscriptions header
    private RelativeLayout rlCardWithExpert, rlCardNoExpert, rlExpert, rlNoExpert;
    private ImageView imgCardNoExpert, imgCardExpert;
    private TextView tvCardNoExpert, tvCardExpert;
    private ScrollView scExpert, scNoExpert;
    private ImageView imgPaidClose;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paid);
        animationMoveFromBottom = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_from_bottom_popup);
        animationMoveFromBottomPaid = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_from_bottom_popup);
        imgAcceptExpert = (ImageView) findViewById(R.id.img_paid_accept_expert);
        imgPaidBtnExpert = (ImageView) findViewById(R.id.img_btn_paid_subscr_expert);

        imgAcceptNoExpert = (ImageView) findViewById(R.id.img_paid_accept);
          imgPaidBtnNoExpert = (ImageView) findViewById(R.id.img_btn_paid_no_expert);
        scExpert = (ScrollView)findViewById(R.id.sc_expert);
        scExpert.fullScroll(View.FOCUS_UP);
        rlExpert = (RelativeLayout) findViewById(R.id.sc_expert_paid);
        rlNoExpert = (RelativeLayout) findViewById(R.id.sc_no_expert_paid);

        rlCardNoExpert = (RelativeLayout) findViewById(R.id.rl_card_no_expert);
        rlCardWithExpert = (RelativeLayout) findViewById(R.id.rl_card_with_expert);

        tvCardNoExpert = (TextView) findViewById(R.id.tv_card_paid);
        tvCardExpert = (TextView) findViewById(R.id.tv_card_with_chat_paid);

        imgCardNoExpert = (ImageView) findViewById(R.id.img_card_no_expert);
        imgCardExpert = (ImageView) findViewById(R.id.img_card_chat_expert);

        rlPaid = (RelativeLayout) findViewById(R.id.rl_paid_subscr);
        imgPaidClose = (ImageView) findViewById(R.id.imgv_back_paid);
        rlPaid.setVisibility(View.VISIBLE);
        rlPaid.startAnimation(animationMoveFromBottomPaid);
        imgPaidClose.setOnClickListener(this);
        imgPaidBtnExpert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.whatIsAccount = 1;
                sendBroadcast(new Intent(Constants.BROADCAST_ACTION_FULL_TASK));
                rlCardNoExpert.setVisibility(View.GONE);
                imgPaidBtnExpert.setVisibility(View.GONE);
                imgAcceptExpert.setVisibility(View.VISIBLE);
            }
        });
        imgPaidBtnNoExpert.setOnClickListener(this);
        rlCardNoExpert.setOnClickListener(this);
        rlCardWithExpert.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgv_back_paid:
            finish();
                break;
//            case R.id.img_btn_paid_subscr_expert:
//                rlCardNoExpert.setVisibility(View.GONE);
//                imgPaidBtnExpert.setVisibility(View.GONE);
//                imgAcceptExpert.setVisibility(View.VISIBLE);
            case R.id.img_btn_paid_no_expert:
                MainActivity.whatIsAccount = 1;
                sendBroadcast(new Intent(Constants.BROADCAST_ACTION_FULL_TASK));
                rlCardWithExpert.setVisibility(View.GONE);
                imgPaidBtnNoExpert.setVisibility(View.GONE);
                imgAcceptNoExpert.setVisibility(View.VISIBLE);
            case R.id.rl_card_no_expert:
                rlExpert.setVisibility(View.GONE);
                rlNoExpert.setVisibility(View.VISIBLE);
                imgCardNoExpert.setImageDrawable(getResources().getDrawable(R.drawable.card_promo_pressed));
                tvCardNoExpert.setTextColor(getResources().getColor(R.color.colorWhite));
                imgCardExpert.setImageDrawable((getResources().getDrawable(R.drawable.card_with_chat_default)));
                tvCardExpert.setTextColor(getResources().getColor(R.color.colorWhiteTrans));
                break;
            case R.id.rl_card_with_expert:
                rlNoExpert.setVisibility(View.GONE);
                rlExpert.setVisibility(View.VISIBLE);
                imgCardNoExpert.setImageDrawable(getResources().getDrawable(R.drawable.card_promo_default));
                tvCardNoExpert.setTextColor(getResources().getColor(R.color.colorWhiteTrans));
                imgCardExpert.setImageDrawable((getResources().getDrawable(R.drawable.card_with_chat_pressed)));
                tvCardExpert.setTextColor(getResources().getColor(R.color.colorWhite));
                break;
        }
    }
}
