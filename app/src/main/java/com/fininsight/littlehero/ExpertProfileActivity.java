package com.fininsight.littlehero;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class ExpertProfileActivity extends AppCompatActivity implements View.OnClickListener{
    private RelativeLayout rlBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expert_profile);
        rlBack = (RelativeLayout) findViewById(R.id.rl_image_back);
        rlBack.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_image_back:
                finish();
                break;
        }
    }
}
