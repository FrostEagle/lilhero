package com.fininsight.littlehero.classForData;


public class Comment {
    private String id;
    private int avatar;
    private String name;
    private String content;

    public String getTimeComment() {
        return timeComment;
    }

    public void setTimeComment(String timeComment) {
        this.timeComment = timeComment;
    }

    private String timeComment;

    public Comment(int avatar, String name, String content, String timeComment) {
        this.avatar = avatar;
        this.name = name;
        this.content = content;
        this.timeComment = timeComment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
