package com.fininsight.littlehero.classForData;


public class Achievements {
    private int id;
    // 0 - no achievements 1 - achievements done!
    private int achievementsState;
    private String titleAchievements;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAchievementsState() {
        return achievementsState;
    }

    public void setAchievementsState(int achievementsState) {
        this.achievementsState = achievementsState;
    }

    public String getTitleAchievements() {
        return titleAchievements;
    }

    public void setTitleAchievements(String titleAchievements) {
        this.titleAchievements = titleAchievements;
    }
    public Achievements (int id, int achievementsState, String titleAchievements) {
        this.id = id;
        setAchievementsState(achievementsState);
        setTitleAchievements(titleAchievements);
    }
}
