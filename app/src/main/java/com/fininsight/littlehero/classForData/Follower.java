package com.fininsight.littlehero.classForData;

/**
 * Created by FrostEagle on 11.08.2016.
 */
public class Follower {
    private int id;
    private String name;
    private int supermen;
    private int friendsWho;
    private String avatar;
    private String avatarThumb;
    public int avatarId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSupermen() {
        return supermen;
    }

    public void setSupermen(int supermen) {
        this.supermen = supermen;
    }

    public int getFriendsWho() {
        return friendsWho;
    }

    public void setFriendsWho(int friendsWho) {
        this.friendsWho = friendsWho;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatarThumb() {
        return avatarThumb;
    }

    public void setAvatarThumb(String avatarThumb) {
        this.avatarThumb = avatarThumb;
    }

    public Follower (int id, String name, int supermen, int friendsWho, int avatarId) {
        this.id = id;
        this.name = name;
        this.supermen = supermen;
        this.friendsWho = friendsWho;
        this.avatarId = avatarId;
        this.avatarThumb = avatarThumb;
    }
}
