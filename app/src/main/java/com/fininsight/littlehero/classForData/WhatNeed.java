package com.fininsight.littlehero.classForData;

/**
 * Created by FrostEagle on 20.08.2016.
 */
public class WhatNeed {
    private int id;
    private String content;

    public WhatNeed(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
