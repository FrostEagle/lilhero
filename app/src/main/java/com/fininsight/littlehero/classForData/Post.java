package com.fininsight.littlehero.classForData;


public class Post {
    private String idPost;
    private int avatarAuthorPost;
    private String authorName;
    private int like;
    private String timePost;
    private String contentPost;
    private int firstPhotoPost;
    private int secondPhotoPost;
    private int taskAvatar;
    private String taskContent;
    private int superman;
    private int countPhoto;
    private int isMe;

    public int getIsMe() {
        return isMe;
    }

    public void setIsMe(int isMe) {
        this.isMe = isMe;
    }

    public int getCountPhoto() {
        return countPhoto;
    }

    public void setCountPhoto(int countPhoto) {
        this.countPhoto = countPhoto;
    }

    public Post(int avatarAuthorPost, String authorName, String timePost, String contentPost, int firstPhotoPost, int secondPhotoPost, int superman, int countPhoto) {
        this.avatarAuthorPost = avatarAuthorPost;
        this.countPhoto = countPhoto;
        this.authorName = authorName;
        this.timePost = timePost;
        this.contentPost = contentPost;
        this.firstPhotoPost = firstPhotoPost;
        this.secondPhotoPost = secondPhotoPost;
        this.superman = superman;
    }
    public Post(int avatarAuthorPost, String authorName, String timePost, String contentPost, int firstPhotoPost, int secondPhotoPost, int superman, int countPhoto, int isMe) {
        this.avatarAuthorPost = avatarAuthorPost;
        this.countPhoto = countPhoto;
        this.authorName = authorName;
        this.timePost = timePost;
        this.contentPost = contentPost;
        this.firstPhotoPost = firstPhotoPost;
        this.secondPhotoPost = secondPhotoPost;
        this.superman = superman;
        this.isMe = isMe;
    }
        public Post(int avatarAuthorPost, String authorName, String timePost, String contentPost, int superman, int isMe) {
            this.avatarAuthorPost = avatarAuthorPost;
            this.authorName = authorName;
            this.timePost = timePost;
            this.contentPost = contentPost;
            this.superman = superman;
            this.isMe = isMe;
        }
     public Post(int avatarAuthorPost, String authorName, String timePost, String contentPost, int firstPhotoPost, int secondPhotoPost, int taskAvatar, String taskContent, int superman) {
        this.avatarAuthorPost = avatarAuthorPost;
        this.authorName = authorName;
        this.timePost = timePost;
        this.contentPost = contentPost;
        this.firstPhotoPost = firstPhotoPost;
        this.secondPhotoPost = secondPhotoPost;
        this.taskAvatar = taskAvatar;
        this.taskContent = taskContent;
        this.superman = superman;
    }

    public String getIdPost() {
        return idPost;
    }

    public void setIdPost(String idPost) {
        this.idPost = idPost;
    }

    public int getAvatarAuthorPost() {
        return avatarAuthorPost;
    }

    public void setAvatarAuthorPost(int avatarAuthorPost) {
        this.avatarAuthorPost = avatarAuthorPost;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public String getTimePost() {
        return timePost;
    }

    public void setTimePost(String timePost) {
        this.timePost = timePost;
    }

    public String getContentPost() {
        return contentPost;
    }

    public void setContentPost(String contentPost) {
        this.contentPost = contentPost;
    }

    public int getFirstPhotoPost() {
        return firstPhotoPost;
    }

    public void setFirstPhotoPost(int firstPhotoPost) {
        this.firstPhotoPost = firstPhotoPost;
    }

    public int getSecondPhotoPost() {
        return secondPhotoPost;
    }

    public void setSecondPhotoPost(int secondPhotoPost) {
        this.secondPhotoPost = secondPhotoPost;
    }

    public int getTaskAvatar() {
        return taskAvatar;
    }

    public void setTaskAvatar(int taskAvatar) {
        this.taskAvatar = taskAvatar;
    }

    public String getTaskContent() {
        return taskContent;
    }

    public void setTaskContent(String taskContent) {
        this.taskContent = taskContent;
    }

    public int getSuperman() {
        return superman;
    }

    public void setSuperman(int superman) {
        this.superman = superman;
    }
}
