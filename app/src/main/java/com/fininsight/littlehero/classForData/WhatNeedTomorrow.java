package com.fininsight.littlehero.classForData;

import java.util.List;


public class WhatNeedTomorrow {
    private int id;
    private String content;
    private List<WhatNeed> whatNeedList;

    public List<WhatNeed> getWhatNeedList() {
        return whatNeedList;
    }

    public void setWhatNeedList(List<WhatNeed> whatNeedList) {
        this.whatNeedList = whatNeedList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public WhatNeedTomorrow(String content, List<WhatNeed> whatNeeds) {
        this.content = content;
        this.whatNeedList = whatNeeds;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
