package com.fininsight.littlehero.task;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.cards.ActivityCardsSwipe;
import com.fininsight.littlehero.extras.Constants;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SwipeTaskRecyclerViewAdapter extends RecyclerSwipeAdapter<SwipeTaskRecyclerViewAdapter.SimpleViewHolder> {


    private Context mContext;
    private ArrayList<Task> taskList;

    public SwipeTaskRecyclerViewAdapter(Context context, ArrayList<Task> objects) {
        this.mContext = context;
        this.taskList = objects;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.swipe_row_task_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        final Task item = taskList.get(position);

        if (item.getSpecial() == 1) {
            viewHolder.imgTaskSpecial.setVisibility(View.VISIBLE);
        } else viewHolder.imgTaskSpecial.setVisibility(View.INVISIBLE);

        viewHolder.civImageId.setImageResource(item.getAvatar());
        viewHolder.tvContentTask.setText(item.getContent());
        viewHolder.tvTime.setText(item.getTime());
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        // Drag From Right
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper));

        viewHolder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            viewHolder.swipeLayout.close();
                mItemManger.closeAllItems();
//                Intent myIntent = new Intent(mContext, CardOpenActivity.class);
//                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                mContext.startActivity(myIntent);
                Intent i2 = new Intent(mContext, ActivityCardsSwipe.class);
                mContext.startActivity(i2);
                Activity activity = (Activity) mContext;
                activity.overridePendingTransition( R.anim.animatino_move_from_bottom_activity, R.anim.animation_fadeout);

            }
        });

        viewHolder.rlMessageExpert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.swipeLayout.close();
                if (item.getExpertOn()==1) {
                }else {
                    Intent intent = new Intent(Constants.BROADCAST_ACTION_UPDATE_DETAIL);
                    mContext.sendBroadcast(intent);
                }
            }
        });
        if (item.getCompleteTask()==1) {
            viewHolder.civImageId.setImageDrawable(mContext.getResources().getDrawable(R.drawable.accept_task_card));
            viewHolder.tvContentTask.setTextColor(mContext.getResources().getColor(R.color.textProfile));
            viewHolder.tvTime.setVisibility(View.INVISIBLE);
            viewHolder.imgTimer.setVisibility(View.INVISIBLE);
            viewHolder.swipeLayout.close();
            viewHolder.rlAcceptTask.setVisibility(View.GONE);
        } else {
            viewHolder.rlAcceptTask.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewHolder.civImageId.setImageDrawable(mContext.getResources().getDrawable(R.drawable.accept_task_card));
                    viewHolder.tvContentTask.setTextColor(mContext.getResources().getColor(R.color.textProfile));
                    viewHolder.tvTime.setVisibility(View.INVISIBLE);
                    viewHolder.imgTimer.setVisibility(View.INVISIBLE);
                    viewHolder.swipeLayout.close();
                    viewHolder.rlAcceptTask.setVisibility(View.GONE);
                }
            });
        }
        mItemManger.bindView(viewHolder.itemView, position);
    }
    @Override
    public int getItemCount() {
        return taskList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        private SwipeLayout swipeLayout;
        private CircleImageView civImageId;
        private RelativeLayout rlMessageExpert, rlAcceptTask;
        private ImageView imgTaskSpecial;
        private ImageView imgTimer;
        private TextView tvContentTask, tvTime;

        public SimpleViewHolder(View itemView) {
            super(itemView);

            imgTaskSpecial = (ImageView) itemView.findViewById(R.id.img_task_special);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            rlMessageExpert = (RelativeLayout) itemView.findViewById(R.id.rl_chat_expert);
            rlAcceptTask = (RelativeLayout) itemView.findViewById(R.id.rl_accept_task);
            civImageId = (CircleImageView) itemView.findViewById(R.id.img_task_avatar);
            tvContentTask = (TextView) itemView.findViewById(R.id.tv_content_task);
            tvTime = (TextView)  itemView.findViewById(R.id.tv_time_task);
            imgTimer = (ImageView) itemView.findViewById(R.id.img_timer);

        }
    }
}
