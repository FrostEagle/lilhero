package com.fininsight.littlehero.task;


public class Task {
    private int id;
    private String content;
    private int special;
    private String time;
    private int avatar;

    public int getExpertOn() {
        return expertOn;
    }

    public void setExpertOn(int expertOn) {
        this.expertOn = expertOn;
    }

    private int expertOn;

    public int getCompleteTask() {
        return completeTask;
    }

    public void setCompleteTask(int completeTask) {
        this.completeTask = completeTask;
    }

    private int completeTask;

    public Task(int avatar, int id, String content, int special, String time, int completeTask, int expertOn) {
        this.id = id;
        this.content = content;
        this.special = special;
        this.time = time;
        this.avatar = avatar;
        this.completeTask = completeTask;
        this.expertOn = expertOn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getSpecial() {
        return special;
    }

    public void setSpecial(int special) {
        this.special = special;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }
}
