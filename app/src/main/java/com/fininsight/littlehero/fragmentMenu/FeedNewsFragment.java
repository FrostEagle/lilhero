package com.fininsight.littlehero.fragmentMenu;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.NewPostActivity;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.PostRecyclerAdapter;
import com.fininsight.littlehero.busProviderCallback.BusProvider;
import com.fininsight.littlehero.busProviderCallback.ClassForBusProvider;
import com.fininsight.littlehero.busProviderCallback.DeletePostClass;
import com.fininsight.littlehero.fragment.FilterNewsFragment;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.DeletePostModel;
import com.fininsight.littlehero.models.postModels.PostModel;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.delete.DeletePostsClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetPostsClient;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FeedNewsFragment extends Fragment implements View.OnClickListener {
    private RelativeLayout rlFilterNews, rlNewPost;
    private View view;
    private FragmentTransaction fragTransaction;
    private RecyclerView rvPost;
    private List<PostModel> listPost;
    private PostRecyclerAdapter postAdapter;
    private AccessToken accessToken;
    private ExpandableRelativeLayout expandableLayout;
    private TextView tvError;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_feed_news, container, false);
        rvPost = (RecyclerView) view.findViewById(R.id.rv_post);
        rlNewPost  = (RelativeLayout) view.findViewById(R.id.rl_add);
        tvError = (TextView) view.findViewById(R.id.tv_error);
        expandableLayout = (ExpandableRelativeLayout) view.findViewById(R.id.expandableLayout);
        expandableLayout.collapse();
        listPost = new ArrayList<>();
        BusProvider.getInstance().register(this);
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());
        getPosts();
        rlFilterNews= (RelativeLayout) view.findViewById(R.id.rl_settings);
        rlFilterNews.setOnClickListener(this);
        rlNewPost.setOnClickListener(this);
        return view;
    }

    private void getPosts() {
        GetPostsClient getPosts = ServiceGenerator.createService(GetPostsClient.class, accessToken, getContext());
        Call<ArrayList<PostModel>> call = getPosts.getPost(1, true, true);
        call.enqueue(new Callback<ArrayList<PostModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PostModel>> call, Response<ArrayList<PostModel>> response) {
                if (response.isSuccessful()) {
                    listPost = response.body();
                    postAdapter = new PostRecyclerAdapter(listPost, getContext());
                    rvPost.setAdapter(postAdapter);
                    rvPost.setLayoutManager(new LinearLayoutManager(getContext()));
                }
            }
            @Override
            public void onFailure(Call<ArrayList<PostModel>> call, Throwable t) {
            }
        });
    }

    @Subscribe
    public void openMyMenu(ClassForBusProvider event) {
        //locationEvents.add(0, event.toString());
        getPosts();
        openExpandAddPost();
    }
    @Subscribe
    public void deletePost(DeletePostClass event) {
        //locationEvents.add(0, event.toString());
        postFollow(event.getId());
    }
    private void openExpandAddPost(){
      //  expandableLayout.setVisibility(View.VISIBLE);
        expandableLayout.setBackgroundColor(getResources().getColor(R.color.accept_quest));
        tvError.setText("Ваш пост опубликован");
        expandableLayout.expand();
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                expandableLayout.collapse();
            }
        }, 1*5000);
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void openExpandDeletePost(){
      //  expandableLayout.setVisibility(View.VISIBLE);
        expandableLayout.setBackgroundColor(getResources().getColor(R.color.exit_app));
        tvError.setText("Ваш пост удален");
        expandableLayout.expand();
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                expandableLayout.collapse();
            }
        }, 1*5000);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_settings:
                FilterNewsFragment filterFragment = new FilterNewsFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, filterFragment);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.rl_add:
                    getContext().startActivity(new Intent(getContext(), NewPostActivity.class));
//                NewPostFragment newPostFragment = new NewPostFragment();
//                fragTransaction = getFragmentManager().beginTransaction();
//                fragTransaction.replace(R.id.frame_for_fragment, newPostFragment);
//                fragTransaction.addToBackStack(null);
//                fragTransaction.commit();
//                Intent intent = new Intent(Constants.BROADCAST_ACTION_HIDE_BOTTOM_BAR);
//                    intent.putExtra(Constants.PARAM_STATUS, Constants.HIDE_BAR);
//                    getContext().sendBroadcast(intent);
                break;

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BusProvider.getInstance().unregister(this);
    }
    private void postFollow(String id) {
        DeletePostsClient deleteClient = ServiceGenerator.createService(DeletePostsClient.class, accessToken, getContext());
        Call<DeletePostModel> callDevice = deleteClient.deletePost(id);
        callDevice.enqueue(new Callback<DeletePostModel>() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<DeletePostModel> call, Response<DeletePostModel> response) {
                ResponseBody errorBo = response.errorBody();
                int responseCode = response.code();
                if (response.isSuccessful()) {
                    getPosts();
                    openExpandDeletePost();
                }
            }
            @Override
            public void onFailure(Call<DeletePostModel> call, Throwable t) {
            }
        });
    }
}
