package com.fininsight.littlehero.fragmentMenu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.WhatNeedTomorrowAdapter;
import com.fininsight.littlehero.classForData.WhatNeed;
import com.fininsight.littlehero.classForData.WhatNeedTomorrow;

import java.util.ArrayList;
import java.util.List;

public class TaskMainTomorrowFragment extends Fragment implements View.OnClickListener {

    private RelativeLayout rlYeasterday;
    private List<WhatNeed> mWhatNeedList;
    private List<WhatNeedTomorrow> whatNeedTomorrowList;
    private WhatNeedTomorrowAdapter tomorrowAdapter;
    private RecyclerView rvWhatNeedTomorrow;
    private RelativeLayout rlHeader;
    private Animation animationFadeIn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_main_tomorrow, container, false);
        mWhatNeedList = new ArrayList<>();
        animationFadeIn = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.animation_fadein);
        rlHeader = (RelativeLayout) view.findViewById(R.id.rl_header);
        rlHeader.startAnimation(animationFadeIn);
        whatNeedTomorrowList = new ArrayList<>();
        rvWhatNeedTomorrow = (RecyclerView) view.findViewById(R.id.rv_what_need_today_task);
        initWhatNeed();
        initWhatNeedTomorrow();
        tomorrowAdapter = new WhatNeedTomorrowAdapter(whatNeedTomorrowList , getContext());
        rvWhatNeedTomorrow.setLayoutManager(new LinearLayoutManager(getContext()));
        rvWhatNeedTomorrow.setAdapter(tomorrowAdapter);


        rlYeasterday = (RelativeLayout) view.findViewById(R.id.rl_yesterday);
        rlYeasterday.setOnClickListener(this);

        return view;
    }
    public void initWhatNeed() {
        mWhatNeedList.add(new WhatNeed("Красные и синие краски, неважно гуашь, акварель или специальные детские "));
        mWhatNeedList.add(new WhatNeed("Пластиковые одноразовые стаканчики, большой ватман, подрамник с полотном"));
    }
    public void initWhatNeedTomorrow() {
        whatNeedTomorrowList.add(new WhatNeedTomorrow("ЗАДАНИЯ ПЕТЕЧКИ",mWhatNeedList));
        whatNeedTomorrowList.add(new WhatNeedTomorrow("ЗАДАНИЯ МАШЕНЬКИ",mWhatNeedList));
        whatNeedTomorrowList.add(new WhatNeedTomorrow("ЗАДАНИЯ ВАНЕЧКИ",mWhatNeedList));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_yesterday:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
}
