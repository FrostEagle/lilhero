package com.fininsight.littlehero.fragmentMenu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.daimajia.swipe.util.Attributes;
import com.fininsight.littlehero.MainActivity;
import com.fininsight.littlehero.PaidActivity;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.MainPostRecyclerAdapter;
import com.fininsight.littlehero.adapter.WhatNeedAdapter;
import com.fininsight.littlehero.classForData.Post;
import com.fininsight.littlehero.classForData.WhatNeed;
import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.task.SwipeTaskRecyclerViewAdapter;
import com.fininsight.littlehero.task.Task;

import java.util.ArrayList;
import java.util.List;


public class TaskMainFragment extends Fragment implements View.OnClickListener{
    private RecyclerView rvTaskMain, rvWhatNeed;
    private ArrayList<Task> mDataSet;
    private ArrayList<Task> mDataSetFree;
    private List<WhatNeed> mWhatNeedList;
    private WhatNeedAdapter adapterNeed;
    private SwipeTaskRecyclerViewAdapter mAdapter;
    private SwipeTaskRecyclerViewAdapter mAdapterFree;
    private RecyclerView rvPost;
    private List<Post> listPost;
    private MainPostRecyclerAdapter postAdapter;
    private RelativeLayout rlYesterday, rlTomorrow;
    private FragmentTransaction fragTransaction;
    private RecyclerView rvTaskMainFree;
    private RelativeLayout rlTaskMainFree;
    private ImageView imgBtnSubscr;
    private RelativeLayout rlHeader;
    private Animation animationFadeIn;
    private NestedScrollView scTask;
    private ImageView imgParam;
    private AnimationDrawable mAnimationDrawable = null;
    private final static int DURATION = 150;
    private CollapsingToolbarLayout coolapseToolbar;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getContext().unregisterReceiver(br);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_main, container, false);
        animationFadeIn = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.animation_fadein);
        rlYesterday = (RelativeLayout) view.findViewById(R.id.rl_yesterday);
        rlHeader = (RelativeLayout) view.findViewById(R.id.rl_header);
        rlHeader.startAnimation(animationFadeIn);
        scTask = (NestedScrollView)  view.findViewById(R.id.sc_task);
        rlTomorrow = (RelativeLayout) view.findViewById(R.id.rl_tomorrow);
        rvTaskMain = (RecyclerView) view.findViewById(R.id.rv_task_main);
        imgParam = (ImageView) view.findViewById(R.id.img_pram);
        rvWhatNeed = (RecyclerView) view.findViewById(R.id.rv_what_need_today_task);
        rlTaskMainFree = (RelativeLayout) view.findViewById(R.id.rl_task_main_free);
        imgBtnSubscr = (ImageView) view.findViewById(R.id.img_btn_subscr);
        coolapseToolbar = (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);
        coolapseToolbar.setContentScrim(getResources().getDrawable(R.drawable.task_main_header));
       // rvTaskMainFree = (RecyclerView) view.findViewById(R.id.rv_task_main_free);
        mDataSet = new ArrayList<>();
        mWhatNeedList = new ArrayList<>();
        mDataSetFree = new ArrayList<>();
        initWhatNeed();
      //  initData();
        initFirstData();
        scTask.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

            }
        });
        adapterNeed = new WhatNeedAdapter(mWhatNeedList , getContext());
        rvWhatNeed.setAdapter(adapterNeed);
        rvWhatNeed.setLayoutManager(new LinearLayoutManager(getContext()));

        mAdapter = new SwipeTaskRecyclerViewAdapter(getContext(), mDataSet);
        mAdapter.setMode(Attributes.Mode.Single);
        rvTaskMain.setAdapter(mAdapter);
        rvTaskMain.setLayoutManager(new LinearLayoutManager(getContext()));
        getContext().registerReceiver(br, new IntentFilter(Constants.BROADCAST_ACTION_FULL_TASK));
//        mAdapterFree = new SwipeTaskRecyclerViewAdapter(getContext(), mDataSetFree);
//        mAdapterFree.setMode(Attributes.Mode.Single);
//        rvTaskMainFree.setAdapter(mAdapterFree);
//        rvTaskMainFree.setLayoutManager(new LinearLayoutManager(getContext()));

        if (MainActivity.whatIsAccount == 0) {
            rlTaskMainFree.setVisibility(View.VISIBLE);
        } else rlTaskMainFree.setVisibility(View.GONE);
        rvPost = (RecyclerView) view.findViewById(R.id.rv_post);
        listPost = postListCreate();
        postAdapter = new MainPostRecyclerAdapter(listPost, getContext());
        rvPost.setAdapter(postAdapter);
        rvPost.setLayoutManager(new LinearLayoutManager(getContext()));
        imgBtnSubscr.setOnClickListener(this);
        rlTomorrow.setOnClickListener(this);
        rlYesterday.setOnClickListener(this);
      //  startFrameAnimation();
        return view;
    }
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void startFrameAnimation() {
        BitmapDrawable frame1 = (BitmapDrawable) getResources().getDrawable(R.drawable.headshead);
        BitmapDrawable frame2 = (BitmapDrawable) getResources().getDrawable(R.drawable.headshead_2);
        BitmapDrawable frame3 = (BitmapDrawable) getResources().getDrawable(R.drawable.headshead_3);
        BitmapDrawable frame4 = (BitmapDrawable) getResources().getDrawable(R.drawable.headshead_4);


        mAnimationDrawable = new AnimationDrawable();

        mAnimationDrawable.setOneShot(true);
        mAnimationDrawable.addFrame(frame1, DURATION);
        mAnimationDrawable.addFrame(frame2, DURATION);
        mAnimationDrawable.addFrame(frame3, DURATION);
        mAnimationDrawable.addFrame(frame4, DURATION);


        imgParam.setImageDrawable(mAnimationDrawable);

        if (!mAnimationDrawable.isRunning()) {
            mAnimationDrawable.setVisible(true, true);
            mAnimationDrawable.start();
        }
    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            rlTaskMainFree.setVisibility(View.GONE);
            mDataSet.clear();
            initData();
            mAdapter.notifyDataSetChanged();
        }
    };
    public void initData() {
        mDataSet.add(new Task(R.drawable.unicorn_card_avatar, 1, "Снежинки на ладошках",  0, "10 мин",0,1));
        mDataSet.add(new Task(R.drawable.brain_card_avatar, 2, "Подойдет любая крупа но мы все-же рекомендуем использовать гречневую ",1 ,"20 мин",0,1));
        mDataSet.add(new Task(R.drawable.biceps_card_avatar, 3, "Здравствуйте, Инна",0,"30 мин",0,0));
        mDataSet.add(new Task(R.drawable.smile_card_avatar, 4, "Видела вас вчера, ваш малыш так вырос",0,"20 мин",0,0));
        mDataSet.add(new Task(R.drawable.crown_card_avatar, 5, "Видела вас вчера, ваш малыш так вырос",0,"20 мин",1,0));
    }
    public void initFirstData() {
        mDataSet.add(new Task(R.drawable.unicorn_card_avatar, 1, "Снежинки на ладошках",  0, "10 мин",0,1));
    }
    public void initWhatNeed() {
        mWhatNeedList.add(new WhatNeed("Красные и синие краски, неважно гуашь, акварель или специальные детские "));
        mWhatNeedList.add(new WhatNeed("Пластиковые одноразовые стаканчики, большой ватман, подрамник с полотном"));
    }
    private List<Post> postListCreate() {
        List<Post> data = new ArrayList<>();
        data.add(new Post(R.drawable.main_avatar, "@anna_avanova", "2 недели назад", "Супер задание! Мы с моим пупсом нагулялись и наигрались вдоволь. Смотрите какие фотографии вышли. ", R.drawable.photo_baby1, R.drawable.photo_baby2, 1, 2));
        return data;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_btn_subscr:
                startActivity(new Intent(getContext(), PaidActivity.class));
                break;
            case R.id.rl_yesterday:
                TaskMainYesterdayFragment yesFrag = new TaskMainYesterdayFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, yesFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.rl_tomorrow:
                TaskMainTomorrowFragment tomFrag = new TaskMainTomorrowFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, tomFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
        }
    }
}
