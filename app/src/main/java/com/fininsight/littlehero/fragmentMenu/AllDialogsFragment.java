package com.fininsight.littlehero.fragmentMenu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.daimajia.swipe.util.Attributes;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.message.SwipeDialogsRecyclerViewAdapter;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.chatModels.ChatModel;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetChatsClient;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AllDialogsFragment extends Fragment {
    private ArrayList<ChatModel> chatsList;
    private RecyclerView mRecyclerView;
    private RelativeLayout rlNoChat;
    private int freeChat = 0;
    private AccessToken accessToken;
    private SwipeDialogsRecyclerViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_dialogs, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        rlNoChat = (RelativeLayout) view.findViewById(R.id.rl_no_chat);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        chatsList = new ArrayList<>();
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());
        GetChatsClient getChatsClient = ServiceGenerator.createService(GetChatsClient.class, accessToken, getContext());
        Call<ArrayList<ChatModel>> call = getChatsClient.getChatsModel();
        call.enqueue(new Callback<ArrayList<ChatModel>>() {
            @Override
            public void onResponse(Call<ArrayList<ChatModel>> call, Response<ArrayList<ChatModel>> response) {
                ResponseBody errorBo = response.errorBody();
                if (response.isSuccessful()) {
                    chatsList = response.body();
                    adapter = new SwipeDialogsRecyclerViewAdapter(getContext(), chatsList);
                    adapter.setMode(Attributes.Mode.Single);
                    mRecyclerView.setAdapter(adapter);
                   // checkChat();
                }
            }
            @Override
            public void onFailure(Call<ArrayList<ChatModel>> call, Throwable t) {
            }
        });

        getContext().registerReceiver(br, new IntentFilter(Constants.BROADCAST_ACTION_UPDATE_FREE_CHAT));

        return view;
    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            checkChat();
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getContext().unregisterReceiver(br);
    }

    private void checkChat() {
        freeChat = 0;
        for (int i = 0;i<chatsList.size();i++) {
//            if (mDataSet.get(i).getExpert()==0) {
//                freeChat++;
//            }
        }
        if (freeChat==0) {
            rlNoChat.setVisibility(View.VISIBLE);
        }
    }

}
