package com.fininsight.littlehero.fragmentMenu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.daimajia.swipe.util.Attributes;
import com.fininsight.littlehero.PaidActivity;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.MainPostRecyclerAdapter;
import com.fininsight.littlehero.classForData.Post;
import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.task.SwipeTaskRecyclerViewAdapter;
import com.fininsight.littlehero.task.Task;

import java.util.ArrayList;
import java.util.List;


public class TaskMainYesterdayFragment extends Fragment implements View.OnClickListener{
    private RecyclerView rvTaskMain;
    private ArrayList<Task> mDataSet;
    private SwipeTaskRecyclerViewAdapter mAdapter;
    private RecyclerView rvPost;
    private List<Post> listPost;
    private MainPostRecyclerAdapter postAdapter;
    private RelativeLayout rlTomorrow;
    private RelativeLayout rlTaskMainFree;
    private ImageView imgBtnSubscr;
    private RelativeLayout rlHeader;
    private Animation animationFadeIn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_main_yesterday, container, false);
        animationFadeIn = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.animation_fadein);
        rlHeader = (RelativeLayout) view.findViewById(R.id.rl_header);
        rlHeader.startAnimation(animationFadeIn);
        rvTaskMain = (RecyclerView) view.findViewById(R.id.rv_task_main);
        rlTomorrow = (RelativeLayout) view.findViewById(R.id.rl_tomorrow);
        rvTaskMain.setLayoutManager(new LinearLayoutManager(getContext()));
        mDataSet = new ArrayList<>();
        initData();
        CollapsingToolbarLayout coolapseToolbar = (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar);
        coolapseToolbar.setContentScrim(getResources().getDrawable(R.drawable.bg_yesterday_header));
        rlTaskMainFree = (RelativeLayout) view.findViewById(R.id.rl_task_main_free);
        imgBtnSubscr = (ImageView) view.findViewById(R.id.img_btn_subscr);
        // rvTaskMainFree = (RecyclerView) view.findViewById(R.id.rv_task_main_free);
        mDataSet = new ArrayList<>();
        initFirstData();
        getContext().registerReceiver(br, new IntentFilter(Constants.BROADCAST_ACTION_FULL_TASK));
        mAdapter = new SwipeTaskRecyclerViewAdapter(getContext(), mDataSet);
        mAdapter.setMode(Attributes.Mode.Single);
        rvTaskMain.setAdapter(mAdapter);
        rvPost = (RecyclerView) view.findViewById(R.id.rv_post);
        listPost = postListCreate();
        postAdapter = new MainPostRecyclerAdapter(listPost, getContext());
        rvPost.setAdapter(postAdapter);
        rvPost.setLayoutManager(new LinearLayoutManager(getContext()));
        rlTomorrow.setOnClickListener(this);
        imgBtnSubscr.setOnClickListener(this);
        return view;
    }
    private BroadcastReceiver br = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            rlTaskMainFree.setVisibility(View.GONE);
            mDataSet.clear();
            initData();
            mAdapter.notifyDataSetChanged();
        }
    };
    public void initData() {
        mDataSet.add(new Task(R.drawable.unicorn_card_avatar, 1, "Снежинки на ладошках",  0, "10 мин",0,1));
        mDataSet.add(new Task(R.drawable.brain_card_avatar, 2, "Подойдет любая крупа но мы все-же рекомендуем использовать гречневую ",1 ,"20 мин",0,1));
        mDataSet.add(new Task(R.drawable.biceps_card_avatar, 3, "Здравствуйте, Инна",0,"30 мин",0,0));
        mDataSet.add(new Task(R.drawable.smile_card_avatar, 4, "Видела вас вчера, ваш малыш так вырос",0,"20 мин",1,0));
        mDataSet.add(new Task(R.drawable.crown_card_avatar, 5, "Видела вас вчера, ваш малыш так вырос",0,"20 мин",0,0));
    }
    public void initFirstData() {
        mDataSet.add(new Task(R.drawable.unicorn_card_avatar, 1, "Снежинки на ладошках",  0, "10 мин",0,1));
    }
    private List<Post> postListCreate() {
        List<Post> data = new ArrayList<>();
        data.add(new Post(R.drawable.main_avatar, "@anna_avanova", "2 недели назад", "Супер задание! Мы с моим пупсом нагулялись и наигрались вдоволь. Смотрите какие фотографии вышли. ", R.drawable.photo_baby1, R.drawable.photo_baby2, 1, 2));
        return data;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_btn_subscr:
                startActivity(new Intent(getContext(), PaidActivity.class));
                break;
            case R.id.rl_tomorrow:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
}
