package com.fininsight.littlehero;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.ErrorModel;
import com.fininsight.littlehero.models.babyModels.ChildModel;
import com.fininsight.littlehero.photos.ProcessingPhotos;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ErrorUtils;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.NewBabyCreateClient;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fininsight.littlehero.extras.Constants.SELECT_FILE;


public class AddChildrenFromSettingsFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener{
    private RelativeLayout rlChildrenPhotoAdd, rlGirlChildrenSex, rlBoyChildrenSex;
    private TextView tvDateBirth, tvSettingsSave;
    private ImageView imgCheckGirlSex, imgCheckBoySex;
    private RelativeLayout imgBack;
    // if sex 0 - girl, 1 - boy
    private int sex = 0;
    private ProcessingPhotos mProcessingPhotos;
    private String mCurrentPhotoPath;
    private Call<ChildModel> call;
    private ImageView imgAvatar;
    private AccessToken accessToken;
    private EditText edtChildrenName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_children_from_settings, container, false);
        mProcessingPhotos = new ProcessingPhotos(getContext());
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());

        tvSettingsSave = (TextView) view.findViewById(R.id.tv_settings_save);
        tvDateBirth = (TextView) view.findViewById(R.id.tv_date_birth);
        edtChildrenName = (EditText) view.findViewById(R.id.edt_children_name);
        rlChildrenPhotoAdd = (RelativeLayout) view.findViewById(R.id.rl_photo_children_add);
        imgBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        rlBoyChildrenSex = (RelativeLayout) view.findViewById(R.id.rl_boy_children_sex);
        rlGirlChildrenSex = (RelativeLayout) view.findViewById(R.id.rl_girl_children_sex);
        imgAvatar = (ImageView) view.findViewById(R.id.img_avatar);
        imgCheckBoySex = (ImageView) view.findViewById(R.id.img_check_boy_sex);
        imgCheckGirlSex = (ImageView) view.findViewById(R.id.img_check_girl_sex);
        tvDateBirth.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        tvSettingsSave.setOnClickListener(this);
        rlChildrenPhotoAdd.setOnClickListener(this);
        rlBoyChildrenSex.setOnClickListener(this);
        rlGirlChildrenSex.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_settings_save:
                postNewChildren();
                break;
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.rl_photo_children_add:
                selectImage();
                break;
            case R.id.tv_date_birth:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getActivity().getFragmentManager(), "DateDialog");
                break;
            case R.id.rl_boy_children_sex:
                imgCheckGirlSex.setVisibility(View.INVISIBLE);
                imgCheckBoySex.setVisibility(View.VISIBLE);
                sex = 1;
                break;
            case R.id.rl_girl_children_sex:
                imgCheckGirlSex.setVisibility(View.VISIBLE);
                imgCheckBoySex.setVisibility(View.INVISIBLE);
                sex = 0;
                break;
        }
    }
    public void selectImage() {
        final CharSequence[] items = {"Сделать фото", "Выбрать фото из галереи", "Отмена"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Добавьте фото!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Сделать фото")) {
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                            File photoFile = null;
                            try {
                                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                photoFile = mProcessingPhotos.createImageFile();
                                Uri contentUri = Uri.fromFile(photoFile);
                                mediaScanIntent.setData(contentUri);
                                getContext().sendBroadcast(mediaScanIntent);
                            } catch (IOException ignored) {
                            }
                            if (photoFile != null) {
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                                getActivityStarterFragment().startActivityForResult(takePictureIntent, 0);
                            }
                        }
                    }
                } else if (items[item].equals("Выбрать фото из галереи")) {
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        getActivityStarterFragment().startActivityForResult(Intent.createChooser(intent, "Select File"),  Constants.SELECT_FILE);
                    }
                } else if (items[item].equals("Отмена")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private Fragment getActivityStarterFragment() {
        if (getParentFragment() != null) {
            return getParentFragment();
        }
        return this;
    }
    public void callOnActivityResultOnChildFragments(Fragment parent, int requestCode, int resultCode, Intent data) {
        FragmentManager childFragmentManager = parent.getChildFragmentManager();
        if (childFragmentManager != null) {
            List<Fragment> childFragments = childFragmentManager.getFragments();
            if (childFragments == null) {
                return;
            }
            for (Fragment child : childFragments) {
                if (child != null && !child.isDetached() && !child.isRemoving()) {
                    child.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = " ";
        String dayofMonthStr = "";
        if (dayOfMonth<10) {
            dayofMonthStr="0"+dayOfMonth;
        } else dayofMonthStr = String.valueOf(dayOfMonth);
        if (++monthOfYear < 10) {
            date = year + "-" + "0" + (monthOfYear) + "-" + dayofMonthStr;
        } else date = year + "-" + (monthOfYear) + "-" + dayofMonthStr;


        tvDateBirth.setText(date);
    }
    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                            photoFile = mProcessingPhotos.createImageFile();
                            Uri contentUri = Uri.fromFile(photoFile);
                            mediaScanIntent.setData(contentUri);
                            getContext().sendBroadcast(mediaScanIntent);
                        } catch (IOException ignored) {
                        }
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, 0);
                        }
                    }
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), Constants.SELECT_FILE);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callOnActivityResultOnChildFragments(this, requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            int REQUEST_CAMERA = 0;
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Bitmap myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                imgAvatar.setImageBitmap(myBitmap);
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(getContext(), selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                mCurrentPhotoPath = selectedImagePath;
                Bitmap myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                imgAvatar.setImageBitmap(myBitmap);
            }
        } else {
            File fdelete = new File(mCurrentPhotoPath);
            if (fdelete.exists()) {
                boolean deleted = fdelete.delete();
                if (deleted) {
                    Log.d("camera", "deleted");
                    getContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(mCurrentPhotoPath))));
                } else {
                    Log.d("camera", "not deleted");
                }
            }
        }
    }
    private void postNewChildren() {
        NewBabyCreateClient dateClient = ServiceGenerator.createService(NewBabyCreateClient.class, accessToken, getContext());
        if (mCurrentPhotoPath != null) {
            File destination = new File(mProcessingPhotos.compressImage(mCurrentPhotoPath));
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), destination);
            MultipartBody.Part body = MultipartBody.Part.createFormData("photo", destination.getName(), requestFile);
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), edtChildrenName.getText().toString());
            RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sex));
            RequestBody date = RequestBody.create(MediaType.parse("text/plain"), tvDateBirth.getText().toString());
            call = dateClient.registerNewBaby(name, gender, date, body);
        } else {
            call = dateClient.registerNewBaby(edtChildrenName.getText().toString(), sex, tvDateBirth.getText().toString());
        }
        call.enqueue(new Callback<ChildModel>() {
            @Override
            public void onResponse(Call<ChildModel> call, Response<ChildModel> response) {
                if (response.errorBody() != null) {
                    ErrorModel error = ErrorUtils.parseError(response);
                    String errorText = error.status();
                }
                if (response.isSuccessful()) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
            @Override
            public void onFailure(Call<ChildModel> call, Throwable t) {
            }
        });

    }
}
