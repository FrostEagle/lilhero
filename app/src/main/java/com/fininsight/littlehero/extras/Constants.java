package com.fininsight.littlehero.extras;


public class Constants {
    public final static String BROADCAST_ACTION_UPDATE_DETAIL = "com.fininsight.motospace.Taskexpert";
    public final static String BROADCAST_ACTION_FULL_TASK = "com.fininsight.motospace.FullTask";
    public final static String BROADCAST_ACTION_UPDATE_FREE_CHAT = "com.fininsight.motospace.AllDialogs";
    public final static String BROADCAST_ACTION_HIDE_BOTTOM_BAR = "com.fininsight.motospace.HideBar";
    public final static int HIDE_BAR = 1;
    public final static int SHOW_BAR = 0;
    public final static String PARAM_STATUS = "status";
    public final static String SERVER_ADDRESS = "http://little-hero.gmg-spb.ru/api/";
    public static final String TOKENREFRESH = "TokenRefresh";
    public final static String clientID = "842736bfa2";
    public final static String clientSecret = "facbf0fc3bf8fa38d8acb98ad798346283946";
    public final static String scope = "read:user,write:user,read:chats,write:chats,read:posts,write:posts,read:misc,write:misc";
    public final static String oauth_loggedin = "oauth.loggedin";
    public final static String oauth_accesstoken = "oauth.accesstoken";
    public final static String oauth_refreshtoken = "oauth.refreshtoken";
    public final static String oauth_tokentype = "oauth.tokentype";

    public final static int SELECT_FILE = 1;


}
