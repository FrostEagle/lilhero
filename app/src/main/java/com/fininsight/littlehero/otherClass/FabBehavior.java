package com.fininsight.littlehero.otherClass;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.fininsight.littlehero.R;


public class FabBehavior extends CoordinatorLayout.Behavior<RelativeLayout>{
    private Animation animationHide, animationShow;
    public FabBehavior(Context context, AttributeSet attrs) {
        super();
        animationHide = AnimationUtils.loadAnimation(context, R.anim.fab_hide);
        animationShow = AnimationUtils.loadAnimation(context, R.anim.fab_show);
    }

    public void onNestedScroll(CoordinatorLayout coordinatorLayout, RelativeLayout child, View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);

        if (child.getVisibility() == View.VISIBLE && dyConsumed > 0) {
            child.startAnimation(animationHide);
            child.setVisibility(View.GONE);
        } else if (child.getVisibility() == View.GONE && dyConsumed < 0) {
            child.startAnimation(animationShow);
            child.setVisibility(View.VISIBLE);
        }
    }

    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, RelativeLayout child, View directTargetChild, View target, int nestedScrollAxes) {
        return nestedScrollAxes == ViewCompat.SCROLL_AXIS_VERTICAL;
    }
}
