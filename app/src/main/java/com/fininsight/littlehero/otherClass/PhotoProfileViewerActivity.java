package com.fininsight.littlehero.otherClass;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.R;
import com.liuguangqiang.swipeback.SwipeBackActivity;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrPosition;
import com.squareup.picasso.Picasso;

import uk.co.senab.photoview.PhotoViewAttacher;


public class PhotoProfileViewerActivity extends SwipeBackActivity {
    private ViewPager viewPager;
    private MyPagerAdapter myPagerAdapter;
    private ImageView imgHeader;
    private TextView tvCountPhoto;
    private ProgressBar pbImageViwer;
    private int count;
    private PhotoViewAttacher mAttacher;
    private RelativeLayout rlClose;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);
        count = getIntent().getIntExtra("count",0);
        viewPager = (HackyViewPager)findViewById(R.id.myviewpager);
        myPagerAdapter = new MyPagerAdapter();
        viewPager.setAdapter(myPagerAdapter);
        pbImageViwer = (ProgressBar) findViewById(R.id.pb_image_viewer);
        rlClose = (RelativeLayout) findViewById(R.id.rl_close_popup);
        tvCountPhoto = (TextView) findViewById(R.id.tv_what_photos);
        tvCountPhoto.setText(1+"/"+String.valueOf(count));
        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                tvCountPhoto.setText(String.valueOf(position+1)+"/"+String.valueOf(count));

            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        SlidrConfig config = new SlidrConfig.Builder()
                .position(SlidrPosition.VERTICAL)
                .sensitivity(0.15f)
                .scrimStartAlpha(0.5f)
                .scrimEndAlpha(0f)
                .velocityThreshold(2400)
                .distanceThreshold(0.20f)
                .edge(false)
                .edgeSize(0.35f)
                .build();
        Slidr.attach(this, config);
    }

    private class MyPagerAdapter extends PagerAdapter {
      //  int NumberOfPages = 5;
        @Override
        public int getCount() {
            return count;
        }
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            final ImageView imageView = new ImageView(PhotoProfileViewerActivity.this);
          //  pbImageViwer.setVisibility(View.VISIBLE);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            LayoutParams imageParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(imageParams);
            Picasso.with(PhotoProfileViewerActivity.this).load("http://little-hero.gmg-spb.ru/images/photos/"+getIntent().getStringExtra("id")+"/"+position).into(imageView, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                    mAttacher = new PhotoViewAttacher(imageView);
                    if (pbImageViwer != null) {
                        pbImageViwer.setVisibility(View.GONE);
                    }
                }
                @Override
                public void onError() {
                }
            });
            LinearLayout layout = new LinearLayout(PhotoProfileViewerActivity.this);
            layout.setOrientation(LinearLayout.VERTICAL);
            LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            layout.setBackgroundResource(R.color.colorBlack);
            layout.setLayoutParams(layoutParams);
            layout.addView(imageView);
            container.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout)object);
        }

    }
}
