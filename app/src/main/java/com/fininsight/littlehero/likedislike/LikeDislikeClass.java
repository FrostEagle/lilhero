package com.fininsight.littlehero.likedislike;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.DeletePostModel;
import com.fininsight.littlehero.models.postModels.LikeModel;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.delete.UnLikeClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.LikeClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LikeDislikeClass {
    private AccessToken accessToken;
    private Context context;

    public LikeDislikeClass(Context context) {
        this.context = context;
    }

    public void likePost(String id) {
        accessToken = AccessTokenReturnObject.accessTokenReturn(context);
        LikeClient likeClient = ServiceGenerator.createService(LikeClient.class, accessToken, context);
        Call<LikeModel> callDevice = likeClient.likePost(id);
        callDevice.enqueue(new Callback<LikeModel>() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<LikeModel> call, Response<LikeModel> response) {
                ResponseBody errorBo = response.errorBody();
                int responseCode = response.code();
                if (response.isSuccessful()) {

                }
            }
            @Override
            public void onFailure(Call<LikeModel> call, Throwable t) {
            }
        });
    }
    public void unLikePost(String id) {
        accessToken = AccessTokenReturnObject.accessTokenReturn(context);
        UnLikeClient unLikeClient = ServiceGenerator.createService(UnLikeClient.class, accessToken, context);
        Call<DeletePostModel> callDevice = unLikeClient.unLikePost(id);
        callDevice.enqueue(new Callback<DeletePostModel>() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<DeletePostModel> call, Response<DeletePostModel> response) {
                ResponseBody errorBo = response.errorBody();
                int responseCode = response.code();
                if (response.isSuccessful()) {

                }
            }
            @Override
            public void onFailure(Call<DeletePostModel> call, Throwable t) {
            }
        });
    }
}
