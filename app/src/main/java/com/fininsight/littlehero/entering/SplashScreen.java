package com.fininsight.littlehero.entering;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.MainActivity;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.application.GlobalValue;
import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.EmailRecoveryModel;
import com.fininsight.littlehero.models.ErrorModel;
import com.fininsight.littlehero.models.RegisterNewDeviceModel;
import com.fininsight.littlehero.models.RegistrationModel;
import com.fininsight.littlehero.models.UserModel;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ErrorUtils;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.APIOauthClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetUserClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.RegisterNewDeviceClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.RegistrationClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.ResetEmailClient;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.jaredrummler.android.device.DeviceName;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity implements View.OnClickListener {
    private ImageView imgLogo, imgLogin, imgRegistration, imgMainBg, imgEnter, imgRecoveryEmail;
    private RelativeLayout imgBackSplash;
    private Animation animationMove, animationFadeIn, animationFadeInLogin, animationClick, animationClickDown;
    private Animation animationClickInstagram, animationClickVk, animationClickEmail, animationClickFacebook, animationClickOdnoklassniki;
    //animation first screen to enter;
    private Animation animationMoveFromRightSocialEnter, animationMoveToLeftLogin, animationMoToLeftSocialEnter, animationEnterBack, animationFadeInTv;
    //back to first screen login
    private Animation animationFadeInBack, animationFadeOutBack;
    //from screen login to screen enter with email
    private Animation animationMoveFromRightDialogEmail, animationMoveToLeftSocial, animationMoveToLeftDialogemail, animationMoveToRightSocial;
    //from screen login to recovery screen
    private Animation animationMoveFromRightRecovery, animationMoveToLeftLoginRecovery, animationMoveToLeftRecovery, animationMoveToRightLoginRecovery, animationClickRecovery;
    //from screen login to screen enter with email
    private Animation animationMoveFromRightRegistration, animationMoveToLeftSocialRegistration, animationMoveToLeftDialogRegistration, animationMoveToRightRegistration;
    private RelativeLayout rlSocialEnter, rlEnterMainLogin, rlEnterLogin, rlRegSplash, rlRecovery;
    // imageScreen = 0; - Главное меню
    // imageScreen = 1; - Меню соц сетей для входа
    // imageScreen = 2; - Диалог для входа
    // imageScreen = 4; - Диалог для восстановления пароля
    // imageScreen = 3; - Диалог для регистрации
    //login screen
    private EditText edtEmailLoginApp, edtPasswordLoginApp, edtRecoveryEmail;
    private ImageView imgEnterLoginApp, imgRegistrationEmail;
    private int imageScreen = 0;
    //regOrLogin = 0; - Регистрация
    //regOrLogin = 1; - Логин
    private int regOrLogin = 0;
    //чтобы убрать зацикливание при нажатии на забыть пароль
    private int forget = 0;
    private ImageView imgEnterInstagram, imgEnterFacebook, imgEnterVk, imgEnterOdnoklassniki;
    //global tv
    private TextView tvEnter;
    private TextView tvForgetPassword;
    private Animation animationZomeOut;
    private ProgressBar pbSplashScreen;
    private Animation animationZomeOutRegistration, animationZomeOutRecovery;
    //registration login
    private EditText edtEmailReg, edtNickReg, edtPasswordReg, edtNameSurnameReg;
    private SharedPreferences prefs;
    private ExpandableRelativeLayout expandableLayout;
    private TextView tvError;
    private String android_id;
    private AccessToken accessToken;
    private String deviceName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        initUI();
        animationDeclare();
        android_id = Settings.Secure.getString(SplashScreen.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        deviceName = DeviceName.getDeviceName();
        prefs = getSharedPreferences(Constants.TOKENREFRESH, Context.MODE_PRIVATE);
        accessToken = AccessTokenReturnObject.accessTokenReturn(SplashScreen.this);
        if (!prefs.getString(Constants.oauth_accesstoken, "").equals("")) {
            GetUserClient getUserClient = ServiceGenerator.createService(GetUserClient.class, accessToken, SplashScreen.this);
            Call<UserModel> call = getUserClient.getUserModels(android_id);
            call.enqueue(new Callback<UserModel>() {
                @Override
                public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                    ResponseBody errorBo = response.errorBody();
                    int responseCode = response.code();
                    if (response.isSuccessful()) {
                        GlobalValue.userModel = response.body();
                        startActivity(new Intent(SplashScreen.this, MainActivity.class));
                        finish();
                    }
                    if (responseCode == 404) {
                        RegisterNewDeviceClient regNewDeviceClient = ServiceGenerator.createService(RegisterNewDeviceClient.class, accessToken, SplashScreen.this);
                        Call<RegisterNewDeviceModel> callDevice = regNewDeviceClient.registerNewDevice(android_id, deviceName);
                        callDevice.enqueue(new Callback<RegisterNewDeviceModel>() {
                            @Override
                            public void onResponse(Call<RegisterNewDeviceModel> call, Response<RegisterNewDeviceModel> response) {
                                ResponseBody errorBo = response.errorBody();
                                int responseCode = response.code();
                                if (response.isSuccessful()) {
                                    startActivity(new Intent(SplashScreen.this, MainActivity.class));
                                    finish();
                                }
                            }
                            @Override
                            public void onFailure(Call<RegisterNewDeviceModel> call, Throwable t) {
                            }
                        });
                    }
                }
                @Override
                public void onFailure(Call<UserModel> call, Throwable t) {
                }
            });
        } else {
            startIntroApp();
        }
        imgEnter.setOnClickListener(this);
        imgRegistration.setOnClickListener(this);
        imgLogin.setOnClickListener(this);
        imgBackSplash.setOnClickListener(this);
        tvForgetPassword.setOnClickListener(this);
        imgEnterLoginApp.setOnClickListener(this);
        imgRegistrationEmail.setOnClickListener(this);
        imgEnterFacebook.setOnClickListener(this);
        imgEnterInstagram.setOnClickListener(this);
        imgEnterVk.setOnClickListener(this);
        imgEnterOdnoklassniki.setOnClickListener(this);
        imgRecoveryEmail.setOnClickListener(this);
    }

    private void startIntroApp() {
        animationZomeOutRegistration.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //ПОявляется крутилка убераем регу с экрана
                pbSplashScreen.setVisibility(View.VISIBLE);
                rlRegSplash.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                // startActivity(new Intent(SplashScreen.this, StatusEnteringActivity.class));

            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        animationZomeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                pbSplashScreen.setVisibility(View.VISIBLE);
                rlEnterLogin.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onAnimationEnd(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        animationZomeOutRecovery.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                pbSplashScreen.setVisibility(View.VISIBLE);
                rlRecovery.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onAnimationEnd(Animation animation) {

            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        //close enter with recovery anim
        animationMoveToRightLoginRecovery.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                rlRecovery.clearAnimation();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        //close enter with email anim
        animationMoveToRightRegistration.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                rlRegSplash.clearAnimation();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        animationMoveToRightSocial.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                rlEnterLogin.clearAnimation();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        logoRegLoginAnim();
        //сдесь магия
        animationFadeOutBack.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                rlSocialEnter.clearAnimation();
                rlEnterMainLogin.clearAnimation();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        imgLogo.startAnimation(animationMove);
        startBgAnim();
    }

    private void enterLoginApp() {
        if (!isValidEmail(edtEmailLoginApp.getText().toString())) {
            tvError.setText("Не валидный email");
            expandableLayout.expand();
        } else if (!isValidPassword(edtPasswordLoginApp.getText().toString())) {
            tvError.setText("Не верный пароль");
            expandableLayout.expand();
        } else {
            rlEnterLogin.startAnimation(animationZomeOut);
            APIOauthClient apiClient = ServiceGenerator.createServiceAuth(APIOauthClient.class);
            Call<AccessToken> call = apiClient.getNewAccessToken(edtEmailLoginApp.getText().toString(),
                                                                    edtPasswordLoginApp.getText().toString(),
                                                                    Constants.clientID,
                                                                    Constants.clientSecret,
                                                                    Constants.scope,
                                                                    "password");
            call.enqueue(new Callback<AccessToken>() {
                @Override
                public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                    ResponseBody errorBo = response.errorBody();
                    int recponce = response.code();
                    if (response.errorBody()!=null) {
                    ErrorModel error = ErrorUtils.parseError(response);
                    String errorText = error.status();
                        if (errorText.equals("invalid_credentials")) {
                            pbSplashScreen.setVisibility(View.INVISIBLE);
                            rlEnterLogin.setVisibility(View.VISIBLE);
                            tvError.setText("Данные не верны попробуй изменить логин или пароль");
                            expandableLayout.expand();
                        }
                    }
                    if (response.isSuccessful()) {
                        AccessToken token = response.body();
                        prefs.edit().putBoolean(Constants.oauth_loggedin, true).apply();
                        prefs.edit().putString(Constants.oauth_accesstoken, token.getAccessToken()).apply();
                        prefs.edit().putString(Constants.oauth_refreshtoken, token.getRefreshToken()).apply();
                        prefs.edit().putString(Constants.oauth_tokentype, token.getTokenType()).apply();

                        startActivity(new Intent(SplashScreen.this, MainActivity.class));
                        finish();
                    }
                }
                @Override
                public void onFailure(Call<AccessToken> call, Throwable t) {

                }
            });
        }

    }

    private void registrationInApp() {
        //expandableLayout.collapse();
        if (!isValidUserName(edtNickReg.getText().toString())) {
            tvError.setText("Не верный ник");
            expandableLayout.expand();
        } else if (!isValidEmail(edtEmailReg.getText().toString())) {
            tvError.setText("Не валидный email");
            expandableLayout.expand();
        } else if (!isValidPassword(edtPasswordReg.getText().toString())) {
            tvError.setText("Не верный пароль, Пароль должен состоять из 8 и более символов и содержать хотябы одну латинскую(маленькую и большую) букву и хотя бы одну цифру, а также любые другие символы.");
            expandableLayout.expand();
        } else {
            rlRegSplash.startAnimation(animationZomeOutRegistration);
            RegistrationClient regClient = ServiceGenerator.createService(RegistrationClient.class);
            RegistrationModel user = new RegistrationModel(edtNickReg.getText().toString(), edtPasswordReg.getText().toString(), edtNameSurnameReg.getText().toString(), edtEmailReg.getText().toString());
            Call<RegistrationModel> call = regClient.createNewUser(user);
            call.enqueue(new Callback<RegistrationModel>() {
                @Override
                public void onResponse(Call<RegistrationModel> call, Response<RegistrationModel> response) {
                    RegistrationModel registrationModel = response.body();
                    if (response.errorBody()!=null) {
                    ErrorModel error = ErrorUtils.parseError(response);
                    String errorText = error.status();
                        if (errorText.equals("duplicate_email")) {
                            pbSplashScreen.setVisibility(View.INVISIBLE);
                            rlRegSplash.setVisibility(View.VISIBLE);
                            tvError.setText("Данный email уже кем-то занят попробуйте другой");
                            expandableLayout.expand();
                        }
                    }
                    if (response.isSuccessful()) {
                        Intent intent = new Intent(SplashScreen.this, ShowCaseActivity.class);
                        intent.putExtra("Email",edtEmailReg.getText().toString());
                        intent.putExtra("Password",edtPasswordReg.getText().toString());
                        startActivity(intent);
                        finish();

                    }
                }
                @Override
                public void onFailure(Call<RegistrationModel> call, Throwable t) {
                }
            });
        }
    } private void recoveryEmail() {
        //expandableLayout.collapse();
        if (!isValidEmail(edtRecoveryEmail.getText().toString())) {
            tvError.setText("Не валидный email");
            expandableLayout.expand();
        } else {
            rlRecovery.startAnimation(animationZomeOutRecovery);
            ResetEmailClient resetClient = ServiceGenerator.createService(ResetEmailClient.class);
            Call<EmailRecoveryModel> call = resetClient.recoveryEmail(edtRecoveryEmail.getText().toString());
            call.enqueue(new Callback<EmailRecoveryModel>() {
                @Override
                public void onResponse(Call<EmailRecoveryModel> call, Response<EmailRecoveryModel> response) {
                    if (response.errorBody()!=null) {
                    ErrorModel error = ErrorUtils.parseError(response);
                    String errorText = error.status();
                        if (errorText.equals("user_not_found")) {
                            pbSplashScreen.setVisibility(View.INVISIBLE);
                            rlRecovery.setVisibility(View.VISIBLE);
                            tvError.setText("Пользователя с таким email не существует");
                            expandableLayout.expand();
                        }
                    }
                    if (response.isSuccessful()) {
                        pbSplashScreen.setVisibility(View.INVISIBLE);
                        rlRecovery.setVisibility(View.VISIBLE);
                        tvError.setText("Письмо успешно отправленно, проверьте почту");
                        expandableLayout.expand();

                    }
                }
                @Override
                public void onFailure(Call<EmailRecoveryModel> call, Throwable t) {
                }
            });
        }
    }

    private void initUI() {
        imgRecoveryEmail = (ImageView) findViewById(R.id.img_recovery_send);
        edtRecoveryEmail = (EditText) findViewById(R.id.edt_recover_splash);
        tvError = (TextView) findViewById(R.id.tv_error);
        expandableLayout = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout);
        edtEmailReg = (EditText) findViewById(R.id.edt_email_reg_splash);
        edtNickReg = (EditText) findViewById(R.id.edt_nick_reg_splash);
        edtPasswordReg = (EditText) findViewById(R.id.edt_enter_password_reg_splash);
        edtNameSurnameReg = (EditText) findViewById(R.id.edt_name_reg_app);
        imgRegistrationEmail = (ImageView) findViewById(R.id.img_reg_account_email);
        edtEmailLoginApp = (EditText) findViewById(R.id.edt_enter_login_app);
        edtPasswordLoginApp = (EditText) findViewById(R.id.edt_enter_password_app);
        imgEnterLoginApp = (ImageView) findViewById(R.id.img_enter_app);
        imgMainBg = (ImageView) findViewById(R.id.img_splash);
        imgEnter = (ImageView) findViewById(R.id.img_enter);
        imgEnterInstagram = (ImageView) findViewById(R.id.img_enter_instagram);
        imgEnterFacebook = (ImageView) findViewById(R.id.img_enter_facebook);
        imgEnterVk = (ImageView) findViewById(R.id.img_enter_vkontake);
        imgEnterOdnoklassniki = (ImageView) findViewById(R.id.img_enter_odnoklassniki);
        imgBackSplash = (RelativeLayout) findViewById(R.id.rl_back);
        rlSocialEnter = (RelativeLayout) findViewById(R.id.rl_social_enter);
        rlEnterLogin = (RelativeLayout) findViewById(R.id.rl_enter_login);
        tvEnter = (TextView) findViewById(R.id.tv_enter);
        rlEnterMainLogin = (RelativeLayout) findViewById(R.id.rl_enter_main_login);
        imgLogo = (ImageView) findViewById(R.id.img_splash_logo);
        imgLogin = (ImageView) findViewById(R.id.img_login);
        imgRegistration = (ImageView) findViewById(R.id.img_registration);
        rlRegSplash = (RelativeLayout) findViewById(R.id.rl_reg_screen_splash);
        tvForgetPassword = (TextView) findViewById(R.id.tv_forget_password);
        rlRecovery = (RelativeLayout) findViewById(R.id.rl_recovery_password);
        pbSplashScreen = (ProgressBar) findViewById(R.id.pb_splash_screen);
    }

    private void animationDeclare() {
        animationMove = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_logo_anim);
        animationFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_fadein);
        animationFadeInTv = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_fadein);
        animationFadeInLogin = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_fadein);
        animationFadeInBack = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_fadein);
        animationFadeOutBack = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_fadeout);
        animationClick = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button);
        animationClickRecovery = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button);
        animationClickInstagram = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button);
        animationClickFacebook = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button);
        animationClickOdnoklassniki = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button);
        animationClickEmail = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button);
        animationClickVk = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button);
        animationClickDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.default_button);
        //back to first screen login
        animationMoveFromRightSocialEnter = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_social_from_right);
        animationMoveToLeftLogin = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_left);
        animationMoToLeftSocialEnter = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_social_to_left);
        animationEnterBack = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_right);
        //from screen login to screen enter with email
        animationMoveFromRightDialogEmail = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_social_from_right);
        animationMoveToLeftSocial = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_left);
        animationMoveToLeftDialogemail = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_social_to_left);
        animationMoveToRightSocial = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_right);
        //from screen registration to screen enter with email
        animationMoveFromRightRegistration = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_social_from_right);
        animationMoveToLeftSocialRegistration = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_left);
        animationMoveToLeftDialogRegistration = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_social_to_left);
        animationMoveToRightRegistration = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_right);
        //from screen registration to screen enter with email
        animationMoveFromRightRecovery = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_social_from_right);
        animationMoveToLeftLoginRecovery = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_left);
        animationMoveToLeftRecovery = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_social_to_left);
        animationMoveToRightLoginRecovery = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_right);

        animationZomeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_zoom_out);
        animationZomeOutRegistration = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_zoom_out);
        animationZomeOutRecovery = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_zoom_out);
    }

    @Override
    public void onBackPressed() {
        switch (imageScreen) {
            //нулевой экран
            case 0:
                finish();
                break;
            case 1:
                pbSplashScreen.setVisibility(View.INVISIBLE);
                expandableLayout.collapse();
                imgEnter.setEnabled(false);
                imgEnterVk.setEnabled(false);
                imgEnterOdnoklassniki.setEnabled(false);
                imgEnterInstagram.setEnabled(false);
                imgEnterFacebook.setEnabled(false);
                imgLogin.setEnabled(true);
                imgRegistration.setEnabled(true);
                rlSocialEnter.startAnimation(animationMoToLeftSocialEnter);
                rlEnterMainLogin.startAnimation(animationEnterBack);
                imgBackSplash.startAnimation(animationFadeOutBack);
                tvEnter.startAnimation(animationFadeOutBack);
                imageScreen = 0;
                break;
            case 2:
                pbSplashScreen.setVisibility(View.INVISIBLE);
                rlEnterLogin.setVisibility(View.INVISIBLE);
                rlRegSplash.setVisibility(View.INVISIBLE);
                expandableLayout.collapse();
                imgEnter.setEnabled(true);
                imgEnterVk.setEnabled(true);
                imgEnterOdnoklassniki.setEnabled(true);
                imgEnterInstagram.setEnabled(true);
                imgEnterFacebook.setEnabled(true);
                tvEnter.setText("Вход");
                tvEnter.startAnimation(animationFadeInTv);
                rlEnterLogin.startAnimation(animationMoveToLeftDialogemail);
                rlSocialEnter.startAnimation(animationMoveToRightSocial);
                imageScreen = 1;
                break;
            case 3:
                pbSplashScreen.setVisibility(View.INVISIBLE);
                rlEnterLogin.setVisibility(View.INVISIBLE);
                rlRegSplash.setVisibility(View.INVISIBLE);
                expandableLayout.collapse();
                imgEnter.setEnabled(true);
                imgEnterVk.setEnabled(true);
                imgEnterOdnoklassniki.setEnabled(true);
                imgEnterInstagram.setEnabled(true);
                imgEnterFacebook.setEnabled(true);
                tvEnter.setText("Вход");
                tvEnter.startAnimation(animationFadeInTv);
                rlRegSplash.startAnimation(animationMoveToLeftDialogRegistration);
                rlSocialEnter.startAnimation(animationMoveToRightRegistration);
                imageScreen = 1;
                break;
            case 4:
                pbSplashScreen.setVisibility(View.INVISIBLE);
                expandableLayout.collapse();
                imgEnter.setEnabled(false);
                imgEnterVk.setEnabled(false);
                imgEnterOdnoklassniki.setEnabled(false);
                imgEnterInstagram.setEnabled(false);
                imgEnterFacebook.setEnabled(false);
                tvEnter.setText("Входим");
                edtEmailLoginApp.setEnabled(true);
                edtPasswordLoginApp.setEnabled(true);
                imgEnterLoginApp.setEnabled(true);
                tvEnter.startAnimation(animationFadeInTv);
                rlRecovery.setVisibility(View.INVISIBLE);
                rlRecovery.startAnimation(animationMoveToLeftRecovery);
                rlEnterLogin.startAnimation(animationMoveToRightLoginRecovery);
                imageScreen = 2;
                forget = 0;
                break;
        }
    }

    private void logoRegLoginAnim() {
        //Logo move
        animationMove.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                imgRegistration.setVisibility(View.VISIBLE);
                imgRegistration.startAnimation(animationFadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        animationFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgLogin.setVisibility(View.VISIBLE);
                imgLogin.startAnimation(animationFadeInLogin);
                imgRegistration.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    private void startBgAnim() {
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(imgMainBg, "alpha", 1f, .1f);
        fadeOut.setDuration(3000);
        final AnimatorSet mAnimationSet = new AnimatorSet();
        mAnimationSet.play(fadeOut);
        mAnimationSet.start();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_enter:
                if (regOrLogin == 1) {
                    imgEnter.setEnabled(false);
                    imgEnterVk.setEnabled(false);
                    imgEnterOdnoklassniki.setEnabled(false);
                    imgEnterInstagram.setEnabled(false);
                    imgEnterFacebook.setEnabled(false);
                    tvEnter.setText("Входим");
                    tvEnter.startAnimation(animationFadeInTv);
                    imgEnter.startAnimation(animationClickEmail);
                    rlEnterLogin.startAnimation(animationMoveFromRightDialogEmail);
                    rlSocialEnter.startAnimation(animationMoveToLeftSocial);
                    imageScreen = 2;
                } else {
                    imgEnter.setEnabled(false);
                    imgEnterVk.setEnabled(false);
                    imgEnterOdnoklassniki.setEnabled(false);
                    imgEnterInstagram.setEnabled(false);
                    imgEnterFacebook.setEnabled(false);
                    tvEnter.setText("Создайте аккаунт");
                    tvEnter.startAnimation(animationFadeInTv);
                    imgEnter.startAnimation(animationClickEmail);
                    rlRegSplash.startAnimation(animationMoveFromRightRegistration);
                    rlSocialEnter.startAnimation(animationMoveToLeftSocialRegistration);
                    imageScreen = 3;
                }
                break;
            case R.id.img_registration:
                imageScreen = 1;
                imgRegistration.startAnimation(animationClick);
                imgBackSplash.startAnimation(animationFadeInBack);
                tvEnter.startAnimation(animationFadeInBack);
                imgEnter.setEnabled(true);
                imgEnterVk.setEnabled(true);
                imgEnterOdnoklassniki.setEnabled(true);
                imgEnterInstagram.setEnabled(true);
                imgEnterFacebook.setEnabled(true);
                rlSocialEnter.startAnimation(animationMoveFromRightSocialEnter);
                rlEnterMainLogin.startAnimation(animationMoveToLeftLogin);
                regOrLogin = 0;
                tvEnter.setText("Регистрация");
                break;
            case R.id.rl_back:
                switch (imageScreen) {
                    case 1:
                        pbSplashScreen.setVisibility(View.INVISIBLE);
                        rlEnterLogin.setVisibility(View.INVISIBLE);
                        expandableLayout.collapse();
                        imgEnter.setEnabled(false);
                        imgEnterVk.setEnabled(false);
                        imgEnterOdnoklassniki.setEnabled(false);
                        imgEnterInstagram.setEnabled(false);
                        imgEnterFacebook.setEnabled(false);
                        imgLogin.setEnabled(true);
                        imgRegistration.setEnabled(true);
                        rlSocialEnter.startAnimation(animationMoToLeftSocialEnter);
                        rlEnterMainLogin.startAnimation(animationEnterBack);
                        imgBackSplash.startAnimation(animationFadeOutBack);
                        tvEnter.startAnimation(animationFadeOutBack);
                        imageScreen = 0;
                        break;
                    case 2:
                        pbSplashScreen.setVisibility(View.INVISIBLE);
                        rlEnterLogin.setVisibility(View.INVISIBLE);
                        rlRegSplash.setVisibility(View.INVISIBLE);
                        expandableLayout.collapse();
                        imgEnter.setEnabled(true);
                        imgEnterVk.setEnabled(true);
                        imgEnterOdnoklassniki.setEnabled(true);
                        imgEnterInstagram.setEnabled(true);
                        imgEnterFacebook.setEnabled(true);
                        tvEnter.setText("Вход");
                        tvEnter.startAnimation(animationFadeInTv);
                        rlEnterLogin.startAnimation(animationMoveToLeftDialogemail);
                        rlSocialEnter.startAnimation(animationMoveToRightSocial);
                        imageScreen = 1;
                        break;
                    case 3:
                        rlEnterLogin.setVisibility(View.INVISIBLE);
                        pbSplashScreen.setVisibility(View.INVISIBLE);
                        rlRegSplash.setVisibility(View.INVISIBLE);
                        expandableLayout.collapse();
                        imgEnter.setEnabled(true);
                        imgEnterVk.setEnabled(true);
                        imgEnterOdnoklassniki.setEnabled(true);
                        imgEnterInstagram.setEnabled(true);
                        imgEnterFacebook.setEnabled(true);
                        tvEnter.setText("Вход");
                        tvEnter.startAnimation(animationFadeInTv);
                        rlRegSplash.startAnimation(animationMoveToLeftDialogRegistration);
                        rlSocialEnter.startAnimation(animationMoveToRightRegistration);
                        imageScreen = 1;
                        break;
                    case 4:
                        pbSplashScreen.setVisibility(View.INVISIBLE);
                        expandableLayout.collapse();
                        imgEnter.setEnabled(false);
                        imgEnterVk.setEnabled(false);
                        imgEnterOdnoklassniki.setEnabled(false);
                        imgEnterInstagram.setEnabled(false);
                        imgEnterFacebook.setEnabled(false);
                        tvEnter.setText("Входим");
                        edtEmailLoginApp.setEnabled(true);
                        edtPasswordLoginApp.setEnabled(true);
                        imgEnterLoginApp.setEnabled(true);
                        tvEnter.startAnimation(animationFadeInTv);
                        rlRecovery.startAnimation(animationMoveToLeftRecovery);
                        rlEnterLogin.startAnimation(animationMoveToRightLoginRecovery);
                        imageScreen = 2;
                        forget = 0;
                        break;
                }
                break;
            case R.id.img_login:
                imageScreen = 1;
                imgLogin.startAnimation(animationClick);
                imgBackSplash.startAnimation(animationFadeInBack);
                tvEnter.startAnimation(animationFadeInBack);
                imgEnter.setEnabled(true);
                imgEnterVk.setEnabled(true);
                imgEnterOdnoklassniki.setEnabled(true);
                imgEnterInstagram.setEnabled(true);
                imgEnterFacebook.setEnabled(true);
                rlSocialEnter.startAnimation(animationMoveFromRightSocialEnter);
                rlEnterMainLogin.startAnimation(animationMoveToLeftLogin);
                tvEnter.setText("Вход");
                regOrLogin = 1;
                break;
            case R.id.img_enter_app:
                imgEnterLoginApp.startAnimation(animationClick);
                enterLoginApp();
                break;
            case R.id.img_reg_account_email:
                imgRegistrationEmail.startAnimation(animationClick);
                registrationInApp();
                break;
            case R.id.img_enter_odnoklassniki:
                imgEnterOdnoklassniki.startAnimation(animationClickOdnoklassniki);
                break;
            case R.id.img_enter_vkontake:
                imgEnterVk.startAnimation(animationClickVk);
                break;
            case R.id.img_enter_facebook:
                imgEnterFacebook.startAnimation(animationClickFacebook);
                break;
            case R.id.img_enter_instagram:
                imgEnterInstagram.startAnimation(animationClickInstagram);
                break;
            case R.id.tv_forget_password:
                if (forget == 0) {
                    expandableLayout.collapse();
                    edtEmailLoginApp.setEnabled(false);
                    edtPasswordLoginApp.setEnabled(false);
                    imgEnterLoginApp.setEnabled(false);
                    imageScreen = 4;
                    tvEnter.setText("Напоминание пароля");
                    tvEnter.startAnimation(animationFadeInTv);
                    rlRecovery.startAnimation(animationMoveFromRightRecovery);
                    rlEnterLogin.startAnimation(animationMoveToLeftLoginRecovery);
                    forget = 1;
                }
                break;
            case R.id.img_recovery_send:
                imgRecoveryEmail.startAnimation(animationClickRecovery);
                recoveryEmail();
                break;
        }
    }

    // validating email id
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9.%-]+(\\.[_A-Za-z0-9.%+-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private boolean isValidUserName(String userName) {
        String USERNAME_PATTERN = "^[A-Za-z0-9_-]{3,15}$";
        Pattern pattern = Pattern.compile(USERNAME_PATTERN);
        Matcher matcher = pattern.matcher(userName);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        boolean peremPass = false;
        if (pass.length() > 7) {
            if (pass.matches("^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=\\S+$).{8,}$")) {
                peremPass = true;
            }
        }
        return peremPass;
    }
}
