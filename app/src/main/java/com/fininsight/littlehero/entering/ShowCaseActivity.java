package com.fininsight.littlehero.entering;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.CursorLoader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.MainActivity;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.ShowCaseViewPager;
import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.ErrorModel;
import com.fininsight.littlehero.models.RegisterNewDeviceModel;
import com.fininsight.littlehero.models.UserModel;
import com.fininsight.littlehero.models.babyModels.ChildModel;
import com.fininsight.littlehero.models.babyModels.PregnancyModel;
import com.fininsight.littlehero.photos.ProcessingPhotos;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ErrorUtils;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.APIOauthClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetUserClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.NewBabyCreateClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.PregnancyDateClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.RegisterNewDeviceClient;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.github.pinball83.maskededittext.MaskedEditText;
import com.jaredrummler.android.device.DeviceName;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fininsight.littlehero.extras.Constants.SELECT_FILE;

public class ShowCaseActivity extends AppCompatActivity implements View.OnClickListener{
    private ImageView imgSun1, imgSun2, imgSun3;
    private ShowCaseViewPager viewPager;
    private MyPagerAdapter myPagerAdapter;
    private ImageView imgMainPicture, imgBgLast, imgLogo, imgBottom;
    private TextView tvClose, tvCountPhoto;
    private ProgressBar pbImageViwer;
    private int count;
    private List<Integer> imgBgList;
    private Animation animationFadeInBack, animationFadeOutBack, animationToBottom;
    private TextView tvTextShowcase;
    private RelativeLayout rlForAnim, rlForSun;
    private ImageView imgNewBaby, imgPregnant;
    // if 0 - pregnant, 1 - newbaby
    private int newBabyPregnant = 0;
    // if sex 0 - girl, 1 - boy
    private int sex = 0;
    private TextView tvSettingsSave;
    private RelativeLayout rlStatusPregnancy, rlAddChildren, rlGirl, rlBoy;
    //animation first screen to enter;
    private Animation animationMoveFromRightSocialEnter, animationMoveToLeftLogin, animationMoToLeftSocialEnter, animationEnterBack, animationMoveFromBottom;
    private int statusEntering = 0;
    private ImageView imgBabyBoy, imgBabyGirl;
    private Animation animationMoveToBottomAnother;
    private SharedPreferences prefs;
    private ExpandableRelativeLayout expandableLayout;
    private TextView tvError;
    private String android_id;
    private AccessToken accessToken;
    private String deviceName;
    private ProcessingPhotos mProcessingPhotos;
    private String mCurrentPhotoPath;
    private Call<ChildModel> call;
    private MaskedEditText edtCountMounthPregnant, edtDateBirth;
    private EditText edtNameReg;
    private CircleImageView imgAddPhoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_case);
        initUI();
        android_id = Settings.Secure.getString(ShowCaseActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        deviceName = DeviceName.getDeviceName();
        prefs = getSharedPreferences(Constants.TOKENREFRESH, Context.MODE_PRIVATE);
        mProcessingPhotos = new ProcessingPhotos(ShowCaseActivity.this);
        APIOauthClient apiClient = ServiceGenerator.createServiceAuth(APIOauthClient.class);
        Call<AccessToken> call = apiClient.getNewAccessToken(getIntent().getStringExtra("Email"),
                getIntent().getStringExtra("Password"),
                Constants.clientID,
                Constants.clientSecret,
                Constants.scope,
                "password");
        call.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                if (response.isSuccessful()) {
                    AccessToken token = response.body();
                    prefs.edit().putBoolean(Constants.oauth_loggedin, true).apply();
                    prefs.edit().putString(Constants.oauth_accesstoken, token.getAccessToken()).apply();
                    prefs.edit().putString(Constants.oauth_refreshtoken, token.getRefreshToken()).apply();
                    prefs.edit().putString(Constants.oauth_tokentype, token.getTokenType()).apply();
                    getUser();
                }
            }
            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {

            }
        });
        animationFadeInBack = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_fadein);
        animationFadeOutBack = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_fadeout);
        animationToBottom= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_to_bottom);
        animationMoveToBottomAnother = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_to_bottom);
        animationMoveToBottomAnother.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                tvSettingsSave.startAnimation(animationFadeInBack);
                rlStatusPregnancy.startAnimation(animationMoveFromBottom);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

       // tvTextShowcase.startAnimation(animationFadeInBack);
        tvTextShowcase.setText(R.string.speak_parent);
       // imgMainPicture.startAnimation(animationFadeInBack);
        imgMainPicture.setImageDrawable(getResources().getDrawable(R.drawable.first_showcase));
        rlForAnim.startAnimation(animationFadeInBack);

        //back to first screen login
        animationMoveFromRightSocialEnter = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_social_from_right);
        animationMoveToLeftLogin = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_left);
        animationMoveFromBottom = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_from_bottom);
        tvSettingsSave.setOnClickListener(this);
        imgNewBaby.setOnClickListener(this);
        imgPregnant.setOnClickListener(this);
        rlBoy.setOnClickListener(this);
        rlGirl.setOnClickListener(this);


        viewPager = (ShowCaseViewPager)findViewById(R.id.view_pager_show_case);
        myPagerAdapter = new MyPagerAdapter();
        viewPager.setAdapter(myPagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
               // imgMainPicture.startAnimation(animationFadeOutBack);

            }
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:

                        tvTextShowcase.setText(R.string.speak_parent);
                        imgMainPicture.setImageDrawable(getResources().getDrawable(R.drawable.first_showcase));
                        rlForAnim.startAnimation(animationFadeInBack);
                        imgSun1.setImageDrawable(getResources().getDrawable(R.drawable.sun_pressed));
                        imgSun2.setImageDrawable(getResources().getDrawable(R.drawable.sun_default));
                        imgSun3.setImageDrawable(getResources().getDrawable(R.drawable.sun_default));
                        break;
                    case 1:

                        tvTextShowcase.setText(R.string.Expirencechildren);
                        imgMainPicture.setImageDrawable(getResources().getDrawable(R.drawable.second_showcase));
                        rlForAnim.startAnimation(animationFadeInBack);
                        imgSun1.setImageDrawable(getResources().getDrawable(R.drawable.sun_default));
                        imgSun2.setImageDrawable(getResources().getDrawable(R.drawable.sun_pressed));
                        imgSun3.setImageDrawable(getResources().getDrawable(R.drawable.sun_default));
                        break;
                    case 2:
                        tvTextShowcase.setText(R.string.seealso);
                        imgMainPicture.setImageDrawable(getResources().getDrawable(R.drawable.third_showcase));
                        rlForAnim.startAnimation(animationFadeInBack);
                        imgSun1.setImageDrawable(getResources().getDrawable(R.drawable.sun_default));
                        imgSun2.setImageDrawable(getResources().getDrawable(R.drawable.sun_default));
                        imgSun3.setImageDrawable(getResources().getDrawable(R.drawable.sun_pressed));
                        break;
                    case 3:
                        viewPager.setLocked(true);
                        imgBgLast.setVisibility(View.VISIBLE);
                        imgLogo.startAnimation(animationFadeOutBack);
                        viewPager.startAnimation(animationFadeOutBack);
                        tvTextShowcase.startAnimation(animationFadeOutBack);
                        imgMainPicture.startAnimation(animationFadeOutBack);
                        imgSun1.startAnimation(animationFadeOutBack);
                        imgSun2.startAnimation(animationFadeOutBack);
                        imgSun3.startAnimation(animationFadeOutBack);
                        imgBottom.startAnimation(animationMoveToBottomAnother);
                        rlForSun.startAnimation(animationToBottom);
                        break;
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void getUser() {
        accessToken = AccessTokenReturnObject.accessTokenReturn(ShowCaseActivity.this);
        if (!prefs.getString(Constants.oauth_accesstoken, "").equals("")) {
            GetUserClient getUserClient = ServiceGenerator.createService(GetUserClient.class, accessToken, ShowCaseActivity.this);
            Call<UserModel> callGetUser = getUserClient.getUserModels(android_id);
            callGetUser.enqueue(new Callback<UserModel>() {
                @Override
                public void onResponse(Call<UserModel> callGetUser, Response<UserModel> response) {
                    int responseCode = response.code();
                    if (response.isSuccessful()) {
                        startActivity(new Intent(ShowCaseActivity.this, MainActivity.class));
                        finish();
                    }
                    if (responseCode == 404) {
                        RegisterNewDeviceClient regNewDeviceClient = ServiceGenerator.createService(RegisterNewDeviceClient.class, accessToken, ShowCaseActivity.this);
                        Call<RegisterNewDeviceModel> callDevice = regNewDeviceClient.registerNewDevice(android_id, deviceName);
                        callDevice.enqueue(new Callback<RegisterNewDeviceModel>() {
                            @Override
                            public void onResponse(Call<RegisterNewDeviceModel> call, Response<RegisterNewDeviceModel> response) {
                            }
                            @Override
                            public void onFailure(Call<RegisterNewDeviceModel> call, Throwable t) {
                            }
                        });
                    }
                }
                @Override
                public void onFailure(Call<UserModel> call, Throwable t) {
                }
            });
        }
    }

    private void initUI() {
        imgSun1 = (ImageView) findViewById(R.id.img_first_sun);
        imgSun2 = (ImageView) findViewById(R.id.img_second_sun);
        imgSun3 = (ImageView) findViewById(R.id.img_third_sun);
        imgLogo = (ImageView) findViewById(R.id.img_logo_intro);
        imgBgLast = (ImageView) findViewById(R.id.img_bg_splash);
        imgBottom = (ImageView) findViewById(R.id.img_bottom);
        tvTextShowcase = (TextView) findViewById(R.id.tv_text_showcase);
        imgMainPicture = (ImageView) findViewById(R.id.img_main_picture_showcase);
        rlForAnim = (RelativeLayout) findViewById(R.id.rl_for_showcase_anim);
        rlForSun = (RelativeLayout) findViewById(R.id.rl_for_sun);
        imgBgList = new ArrayList<>();
        imgBgList.add(R.drawable.splash_image1);
        imgBgList.add(R.drawable.splash_image2);
        imgBgList.add(R.drawable.splash_image3);
        imgBgList.add(R.drawable.bg_splash_last);

        edtNameReg = (EditText) findViewById(R.id.edt_name_baby_reg);
        tvError = (TextView) findViewById(R.id.tv_error);
        expandableLayout = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout);
        edtDateBirth = (MaskedEditText) findViewById(R.id.edt_date_birth);
        imgAddPhoto = (CircleImageView) findViewById(R.id.img_add_children_photo);
        //Status entering ACTIVITY
        edtCountMounthPregnant = (MaskedEditText) findViewById(R.id.edt_count_month_pregnant);
        imgNewBaby = (ImageView) findViewById(R.id.img_mother_new_baby);
        imgPregnant = (ImageView) findViewById(R.id.img_mother_pregnant);
        tvSettingsSave = (TextView) findViewById(R.id.tv_settings_save);
        rlStatusPregnancy = (RelativeLayout) findViewById(R.id.rl_status_pregnancy);
        rlAddChildren = (RelativeLayout) findViewById(R.id.rl_add_children);
        imgBabyBoy = (ImageView) findViewById(R.id.img_baby_boy);
        imgBabyGirl = (ImageView) findViewById(R.id.img_baby_girl);
        rlGirl = (RelativeLayout) findViewById(R.id.rl_girl);
        rlBoy = (RelativeLayout) findViewById(R.id.rl_boy);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_mother_new_baby:
                imgNewBaby.setImageDrawable(getResources().getDrawable(R.drawable.mother_newbaby_pressed));
                imgPregnant.setImageDrawable(getResources().getDrawable(R.drawable.pregnant_status_default));
                newBabyPregnant = 1;
                edtCountMounthPregnant.setVisibility(View.INVISIBLE);
                break;
            case R.id.img_mother_pregnant:
                imgPregnant.setImageDrawable(getResources().getDrawable(R.drawable.pregnant_status));
                imgNewBaby.setImageDrawable(getResources().getDrawable(R.drawable.mother_newbaby_default));
                newBabyPregnant = 0;
                edtCountMounthPregnant.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_settings_save:
                if (newBabyPregnant==0) {
                    accessToken = AccessTokenReturnObject.accessTokenReturn(ShowCaseActivity.this);
                    PregnancyDateClient dateClient = ServiceGenerator.createService(PregnancyDateClient.class, accessToken, ShowCaseActivity.this);
                    Call<PregnancyModel> call = dateClient.registerNewBaby(edtCountMounthPregnant.getText().toString());
                    call.enqueue(new Callback<PregnancyModel>() {
                        @Override
                        public void onResponse(Call<PregnancyModel> call, Response<PregnancyModel> response) {
                            ResponseBody errorBo = response.errorBody();
                            int recponce = response.code();
                            if (response.errorBody()!=null) {
                                ErrorModel error = ErrorUtils.parseError(response);
//                                String errorText = error.status();
//                                if (errorText.equals("invalid_credentials")) {
                                //                              }
                                expandableLayout.expand();
                            }
                            if (response.isSuccessful()) {
                                startActivity(new Intent(ShowCaseActivity.this, MainActivity.class));
                                finish();
                            }
                        }
                        @Override
                        public void onFailure(Call<PregnancyModel> call, Throwable t) {
                        }
                    });

                } else {
                    if (statusEntering == 0) {
                        rlStatusPregnancy.startAnimation(animationMoveToLeftLogin);
                        rlAddChildren.startAnimation(animationMoveFromRightSocialEnter);
                        statusEntering++;
                    } else {
                        postNewChildren();
                    }
                }
                break;
            case R.id.rl_boy:
                imgBabyBoy.setImageDrawable(getResources().getDrawable(R.drawable.first_baby_male_pressed));
                imgBabyGirl.setImageDrawable(getResources().getDrawable(R.drawable.first_baby_female_default));
                sex = 1;
                break;
            case R.id.rl_girl:
                imgBabyGirl.setImageDrawable(getResources().getDrawable(R.drawable.first_baby_female_pressed));
                imgBabyBoy.setImageDrawable(getResources().getDrawable(R.drawable.first_baby_male_default));
                sex = 0;
                break;
        }
    }

    private class MyPagerAdapter extends PagerAdapter {
        //  int NumberOfPages = 5;
        @Override
        public int getCount() {
            return imgBgList.size();
        }
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            final ImageView imageView = new ImageView(ShowCaseActivity.this);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            ViewGroup.LayoutParams imageParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(imageParams);
            imageView.setImageDrawable(getResources().getDrawable(imgBgList.get(position)));

            // tvCountPhoto.setText(String.valueOf(viewPager.getCurrentItem())+" из "+String.valueOf(MainActivity.vehiclesList.get(count).getPhotos().size()));
            LinearLayout layout = new LinearLayout(ShowCaseActivity.this);
            layout.setOrientation(LinearLayout.VERTICAL);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layout.setBackgroundResource(R.color.colorBlack);
            layout.setLayoutParams(layoutParams);
            layout.addView(imageView);
            container.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout)object);
        }

    }
    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                            photoFile = mProcessingPhotos.createImageFile();
                            Uri contentUri = Uri.fromFile(photoFile);
                            mediaScanIntent.setData(contentUri);
                            sendBroadcast(mediaScanIntent);
                        } catch (IOException ignored) {
                        }
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, 0);
                        }
                    }
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), Constants.SELECT_FILE);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            int REQUEST_CAMERA = 0;
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Bitmap myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                imgAddPhoto.setImageBitmap(myBitmap);
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                mCurrentPhotoPath = selectedImagePath;
                Bitmap myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                imgAddPhoto.setImageBitmap(myBitmap);
            }
        } else {
            File fdelete = new File(mCurrentPhotoPath);
            if (fdelete.exists()) {
                boolean deleted = fdelete.delete();
                if (deleted) {
                    Log.d("camera", "deleted");
                    this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(mCurrentPhotoPath))));
                } else {
                    Log.d("camera", "not deleted");
                }
            }
        }
    }
    private void postNewChildren() {
        NewBabyCreateClient dateClient = ServiceGenerator.createService(NewBabyCreateClient.class, accessToken, ShowCaseActivity.this);
        if (mCurrentPhotoPath != null) {
            File destination = new File(mProcessingPhotos.compressImage(mCurrentPhotoPath));
            // create RequestBody instance from file
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), destination);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body = MultipartBody.Part.createFormData("photo", destination.getName(), requestFile);
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), edtNameReg.getText().toString());
            RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sex));
            RequestBody date = RequestBody.create(MediaType.parse("text/plain"), edtDateBirth.getText().toString());
            call = dateClient.registerNewBaby(name, gender, date, body);
        } else {
            call = dateClient.registerNewBaby(edtNameReg.getText().toString(), sex, edtDateBirth.getText().toString());
        }
        call.enqueue(new Callback<ChildModel>() {
            @Override
            public void onResponse(Call<ChildModel> call, Response<ChildModel> response) {
                ResponseBody errorBo = response.errorBody();
                int recponce = response.code();
                if (response.errorBody() != null) {
                    ErrorModel error = ErrorUtils.parseError(response);
//                                String errorText = error.status();
//                                if (errorText.equals("invalid_credentials")) {
                    //                              }
                }
                if (response.isSuccessful()) {
                    startActivity(new Intent(ShowCaseActivity.this, MainActivity.class));
                    finish();
                }
            }
            @Override
            public void onFailure(Call<ChildModel> call, Throwable t) {
            }
        });

    }
}
