package com.fininsight.littlehero.cards.cardswipe;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.WhatNeedAdapter;
import com.fininsight.littlehero.cards.CardInfo;

import java.util.ArrayList;
import java.util.List;

public class CardPagerAdapter extends PagerAdapter implements CardAdapter {
    private Context context;
    private List<CardView> mViews =  new ArrayList<>();
    private List<CardInfo> cardInfoList;

    private float mBaseElevation;
    private WhatNeedAdapter adapterNeed;

    public CardPagerAdapter() {
    }
    public CardPagerAdapter(Context context, List<CardInfo> cardInfos) {
        this.context = context;
        this.cardInfoList = cardInfos;
        for (int i = 0; i < cardInfoList.size(); i++) {
            mViews.add(null);
        }
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mViews.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.row_adapter_cards, container, false);
        container.addView(view);
        final CardInfo item = cardInfoList.get(position);
        TextView tvCardTaskName = (TextView) view.findViewById(R.id.tv_card_task_name);
        TextView tvMainContentCard = (TextView) view.findViewById(R.id.tv_main_content_card);
        RelativeLayout rlDone = (RelativeLayout) view.findViewById(R.id.rl_done);
        RelativeLayout rlDoneTask = (RelativeLayout) view.findViewById(R.id.rl_done_for_task);
        ImageView imgPhotoCard = (ImageView) view.findViewById(R.id.img_photo_card);
        ImageView imgForRlTrans = (ImageView) view.findViewById(R.id.img_for_rl_done_trans);
        TextView tvComplete = (TextView) view.findViewById(R.id.tv_complete);
        TextView tvComment = (TextView) view.findViewById(R.id.tv_comment);
        RecyclerView rvWhatNeed = (RecyclerView) view.findViewById(R.id.rv_what_need_task_card);
        ScrollView scCard = (ScrollView) view.findViewById(R.id.sc_card);
        tvCardTaskName.setText(item.getTitleCard());
        tvMainContentCard.setText(item.getContentCard());
        if (item.isCompleteTask()) {
            rlDoneTask.setVisibility(View.VISIBLE);
            imgForRlTrans.setVisibility(View.VISIBLE);
        } else {
            rlDoneTask.setVisibility(View.INVISIBLE);
            imgForRlTrans.setVisibility(View.INVISIBLE);
        }
        if (item.isCommentMy()) {
            rvWhatNeed.setVisibility(View.GONE);
            tvMainContentCard.setVisibility(View.GONE);
            rlDone.setVisibility(View.VISIBLE);
            scCard.setVisibility(View.VISIBLE);
        } else {
            rvWhatNeed.setVisibility(View.VISIBLE);
            tvMainContentCard.setVisibility(View.VISIBLE);
            rlDone.setVisibility(View.GONE);
            scCard.setVisibility(View.GONE);
        }
        imgPhotoCard.setImageResource(item.getAvatarCard());
        tvComplete.setText(item.getCountCompleteTask()+" Выполнили");
        tvComment.setText(item.getCountComment()+" Комментариев");

        adapterNeed = new WhatNeedAdapter(item.getWhatNeedList() , context);
        rvWhatNeed.setAdapter(adapterNeed);
        rvWhatNeed.setLayoutManager(new LinearLayoutManager(context));
        CardView cardView = (CardView) view.findViewById(R.id.cardView);
        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }
        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);
        return view;
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

}
