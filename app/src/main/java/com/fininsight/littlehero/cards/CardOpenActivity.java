package com.fininsight.littlehero.cards;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fininsight.littlehero.CardCompleteActivity;
import com.fininsight.littlehero.MainActivity;
import com.fininsight.littlehero.NoPaidActivity;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.WhatNeedAdapter;
import com.fininsight.littlehero.classForData.WhatNeed;
import com.fininsight.littlehero.message.ChatExpertActivity;
import com.fininsight.littlehero.otherClass.ScaleCircleNavigator;
import com.makeramen.roundedimageview.RoundedImageView;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrPosition;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;

import java.util.ArrayList;
import java.util.List;

public class CardOpenActivity extends AppCompatActivity implements View.OnClickListener{

    private ViewPager mViewPager;
    private List<Integer> mDataList;
    private MyPagerAdapter myPagerAdapter;
    private List<WhatNeed> mWhatNeedList;
    private RecyclerView rvWhatNeed;
    private WhatNeedAdapter adapterNeed;
    private ImageView imgExpertChat, imgDoneCard;
    private Animation animationClickYes, animationClickNo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_open);

        imgExpertChat = (ImageView) findViewById(R.id.img_question_expert);
        imgDoneCard = (ImageView) findViewById(R.id.img_accept_task);
        animationClickYes = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button_pregnant);
        animationClickNo = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button_pregnant);
        mDataList = new ArrayList<>();
        mDataList.add(R.drawable.photo_baby1);
        mDataList.add(R.drawable.photo_baby2);
        mDataList.add(R.drawable.photo_baby1);
        mDataList.add(R.drawable.photo_baby2);
        mDataList.add(R.drawable.photo_baby1);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        myPagerAdapter = new MyPagerAdapter();
        mViewPager.setAdapter(myPagerAdapter);
        SlidrConfig config = new SlidrConfig.Builder()
                .primaryColor(getResources().getColor(R.color.colorPrimary))
                .secondaryColor(getResources().getColor(R.color.colorForLines))
                .position(SlidrPosition.TOP)
                .sensitivity(1f)
                .scrimStartAlpha(0.1f)
                .scrimEndAlpha(0f)
                .velocityThreshold(2400)
                .distanceThreshold(0.25f)
                .edge(true)
                .edgeSize(0.30f)
                .build();
        Slidr.attach(this, config);
      //  setDragEdge(SwipeBackLayout.DragEdge.TOP);
        rvWhatNeed = (RecyclerView) findViewById(R.id.rv_what_need_task_card);
        mWhatNeedList = new ArrayList<>();
        initWhatNeed();
        adapterNeed = new WhatNeedAdapter(mWhatNeedList , CardOpenActivity.this);
        rvWhatNeed.setAdapter(adapterNeed);
        rvWhatNeed.setLayoutManager(new LinearLayoutManager(CardOpenActivity.this));

        MagicIndicator magicIndicator13 = (MagicIndicator) findViewById(R.id.magic_indicator);
        ScaleCircleNavigator scaleCircleNavigator = new ScaleCircleNavigator(this);
        scaleCircleNavigator.setCircleCount(mDataList.size());
        scaleCircleNavigator.setSelectedCircleColor(getResources().getColor(R.color.exit_app));
        magicIndicator13.setNavigator(scaleCircleNavigator);
        ViewPagerHelper.bind(magicIndicator13, mViewPager);
        imgDoneCard.setOnClickListener(this);
        imgExpertChat.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_question_expert:
                if (MainActivity.whatIsAccount==0) {
                    imgExpertChat.startAnimation(animationClickNo);
                    startActivity(new Intent(CardOpenActivity.this, NoPaidActivity.class));
                    overridePendingTransition(R.anim.animatino_move_from_bottom_activity, R.anim.animation_fadeout);
                } else startActivity(new Intent(CardOpenActivity.this, ChatExpertActivity.class));
                break;
            case R.id.img_accept_task:
                imgDoneCard.startAnimation(animationClickYes);
                startActivity(new Intent(CardOpenActivity.this, CardCompleteActivity.class));
                break;
        }
    }


    private class MyPagerAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            return mDataList.size();
        }
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            final RoundedImageView imageView = new RoundedImageView(CardOpenActivity.this);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            ViewGroup.LayoutParams imageParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            imageView.setLayoutParams(imageParams);
       //     imageView.setCornerRadius(30);
            imageView.setImageDrawable(getResources().getDrawable(mDataList.get(position)));
            LinearLayout layout = new LinearLayout(CardOpenActivity.this);
            layout.setOrientation(LinearLayout.VERTICAL);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layout.setBackgroundResource(android.R.color.transparent);
            layout.setLayoutParams(layoutParams);
            layout.addView(imageView);
            container.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout)object);
        }
    }
    public void initWhatNeed() {
        mWhatNeedList.add(new WhatNeed("Красные и синие краски, неважно гуашь, акварель или специальные детские "));
        mWhatNeedList.add(new WhatNeed("Пластиковые одноразовые стаканчики, большой ватман, подрамник с полотном"));
    }
}
