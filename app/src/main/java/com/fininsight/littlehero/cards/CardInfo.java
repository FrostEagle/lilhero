package com.fininsight.littlehero.cards;


import com.fininsight.littlehero.classForData.WhatNeed;

import java.util.List;

public class CardInfo {
    private String titleCard;
    private int avatarCard;
    private int countCompleteTask;
    private int countComment;
    private String contentCard;
    private boolean completeTask;
    private List<WhatNeed> whatNeedList;
    private boolean commentMy;

    public boolean isCommentMy() {
        return commentMy;
    }

    public void setCommentMy(boolean commentMy) {
        this.commentMy = commentMy;
    }

    public CardInfo(String titleCard, int avatarCard, int countCompleteTask, int countComment, String contentCard, boolean completeTask, List<WhatNeed> whatNeedList, boolean commentMy) {
        this.titleCard = titleCard;
        this.avatarCard = avatarCard;
        this.countCompleteTask = countCompleteTask;
        this.countComment = countComment;
        this.contentCard = contentCard;
        this.completeTask = completeTask;
        this.whatNeedList = whatNeedList;
        this.commentMy = commentMy;
    }

    public String getTitleCard() {
        return titleCard;
    }

    public void setTitleCard(String titleCard) {
        this.titleCard = titleCard;
    }

    public int getAvatarCard() {
        return avatarCard;
    }

    public void setAvatarCard(int avatarCard) {
        this.avatarCard = avatarCard;
    }

    public int getCountCompleteTask() {
        return countCompleteTask;
    }

    public void setCountCompleteTask(int countCompleteTask) {
        this.countCompleteTask = countCompleteTask;
    }

    public int getCountComment() {
        return countComment;
    }

    public void setCountComment(int countComment) {
        this.countComment = countComment;
    }

    public String getContentCard() {
        return contentCard;
    }

    public void setContentCard(String contentCard) {
        this.contentCard = contentCard;
    }

    public boolean isCompleteTask() {
        return completeTask;
    }

    public void setCompleteTask(boolean completeTask) {
        this.completeTask = completeTask;
    }

    public List<WhatNeed> getWhatNeedList() {
        return whatNeedList;
    }

    public void setWhatNeedList(List<WhatNeed> whatNeedList) {
        this.whatNeedList = whatNeedList;
    }
}
