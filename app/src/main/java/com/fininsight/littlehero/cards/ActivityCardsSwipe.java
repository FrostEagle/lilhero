package com.fininsight.littlehero.cards;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.CompoundButton;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.cards.cardswipe.CardPagerAdapter;
import com.fininsight.littlehero.cards.cardswipe.ClickableViewPager;
import com.fininsight.littlehero.cards.cardswipe.ShadowTransformer;
import com.fininsight.littlehero.classForData.WhatNeed;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrPosition;

import java.util.ArrayList;
import java.util.List;

public class ActivityCardsSwipe extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    private Button mButton;
    private ClickableViewPager mViewPager;
    private CardPagerAdapter mCardAdapter;
    private ShadowTransformer mCardShadowTransformer;
    private List<CardInfo> cardInfoList;
    private List<WhatNeed> mWhatNeedList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cards_swipe);
        mViewPager = (ClickableViewPager) findViewById(R.id.viewPager);
        mViewPager.setOnViewPagerClickListener(new ClickableViewPager.OnClickListener() {
            @Override
            public void onViewPagerClick(ViewPager viewPager) {
                viewPager.getCurrentItem();
                Intent intent = new Intent(ActivityCardsSwipe.this, CardOpenActivity.class);
                startActivity(intent);
            //    overridePendingTransition( R.anim.animation_zoom_activity , R.anim.animation_fadeout );
            }
        });
        initWhatNeed();
        cardInfoCreate();
        mCardAdapter = new CardPagerAdapter(ActivityCardsSwipe.this, cardInfoList);

        mCardShadowTransformer = new ShadowTransformer(mViewPager, mCardAdapter);
        mCardShadowTransformer.enableScaling(true);
        mCardShadowTransformer.onPageSelected(4);

        mViewPager.setAdapter(mCardAdapter);
        mViewPager.setPageTransformer(true, mCardShadowTransformer);
        mViewPager.setOffscreenPageLimit(3);


        SlidrConfig config = new SlidrConfig.Builder()
                .primaryColor(getResources().getColor(R.color.colorPrimary))
                        .secondaryColor(getResources().getColor(R.color.colorForLines))
                                .position(SlidrPosition.TOP)
                                .sensitivity(1f)
                                .scrimStartAlpha(0.8f)
                                .scrimEndAlpha(0f)
                                .velocityThreshold(2400)
                                .distanceThreshold(0.25f)
                                .edge(true)
                                .edgeSize(0.40f)
                                .build();
        Slidr.attach(this, config);
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        mCardShadowTransformer.enableScaling(b);
    }

    public List<CardInfo> cardInfoCreate() {
        cardInfoList = new ArrayList<>();
        CardInfo card1 = new CardInfo("СМЕШНЫЕ ФОТО НА УЛИЦЕ СНЕГ НА ЛАДОШКЕ", R.drawable.photo_baby1, 2000, 143,
                "Подготовьте прежде вcего красные и синие краски, неважно гуашь, акварель или специальные детские и 2 пластиковых одноразовых стаканчика. В идеале, подготовьте большой ватман, подрамник с полотном формата 2*2 ",
                false, mWhatNeedList,false);  CardInfo card2 = new CardInfo("СМЕШНЫЕ ФОТО НА УЛИЦЕ СНЕГ НА ЛАДОШКЕ", R.drawable.photo_baby1, 2000, 143,
                "Подготовьте прежде вcего красные и синие краски, неважно гуашь, акварель или специальные детские и 2 пластиковых одноразовых стаканчика. В идеале, подготовьте большой ватман, подрамник с полотном формата 2*2 ",
                false, mWhatNeedList,false);  CardInfo card3 = new CardInfo("СМЕШНЫЕ ФОТО НА УЛИЦЕ СНЕГ НА ЛАДОШКЕ", R.drawable.photo_baby1, 2000, 143,
                "Подготовьте прежде вcего красные и синие краски, неважно гуашь, акварель или специальные детские и 2 пластиковых одноразовых стаканчика. В идеале, подготовьте большой ватман, подрамник с полотном формата 2*2 ",
                true, mWhatNeedList,false);  CardInfo card4 = new CardInfo("СМЕШНЫЕ ФОТО НА УЛИЦЕ СНЕГ НА ЛАДОШКЕ", R.drawable.photo_baby1, 2000, 143,
                "Подготовьте прежде вcего красные и синие краски, неважно гуашь, акварель или специальные детские и 2 пластиковых одноразовых стаканчика. В идеале, подготовьте большой ватман, подрамник с полотном формата 2*2 ",
                false, mWhatNeedList,false);  CardInfo card5 = new CardInfo("СМЕШНЫЕ ФОТО НА УЛИЦЕ СНЕГ НА ЛАДОШКЕ", R.drawable.photo_baby1, 2000, 143,
                "Подготовьте прежде вcего красные и синие краски, неважно гуашь, акварель или специальные детские и 2 пластиковых одноразовых стаканчика. В идеале, подготовьте большой ватман, подрамник с полотном формата 2*2 ",
                true, mWhatNeedList, false);

        cardInfoList.add(card1);
        cardInfoList.add(card2);
        cardInfoList.add(card3);
        cardInfoList.add(card4);
        cardInfoList.add(card5);

        return cardInfoList;
    }
    public void initWhatNeed() {
        mWhatNeedList = new ArrayList<>();
        mWhatNeedList.add(new WhatNeed("Красные и синие краски, неважно гуашь, акварель или специальные детские "));
        mWhatNeedList.add(new WhatNeed("Пластиковые одноразовые стаканчики, большой ватман, подрамник с полотном"));
    }
}
