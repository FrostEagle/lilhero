package com.fininsight.littlehero.serverAPI.classForRequestInterface.delete;

import com.fininsight.littlehero.models.DeletePostModel;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Path;


public interface DeletePostsClient {
    @DELETE("post/{id}")
    Call<DeletePostModel> deletePost(@Path("id") String delete);
}
