package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.postModels.CommentModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;


public interface SendCommentClient {
    @FormUrlEncoded
    @POST("post/{id}/comment")
    Call<CommentModel> sendComment(@Path("id") String id, @Field("message") String message);

    @Multipart
    @POST("post/{id}/comment")
    Call<CommentModel> sendComment(@Path("id") String id, @Part("message") RequestBody message, @Part MultipartBody.Part file);
}
