package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.RegistrationModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface RegistrationClient {
    @POST("user")
    Call<RegistrationModel> createNewUser(@Body RegistrationModel regMod);
}
