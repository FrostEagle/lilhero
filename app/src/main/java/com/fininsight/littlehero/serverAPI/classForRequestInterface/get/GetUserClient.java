package com.fininsight.littlehero.serverAPI.classForRequestInterface.get;


import com.fininsight.littlehero.models.UserModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetUserClient {
    @GET("user")
    Call<UserModel> getUserModels(@Query("device_id") String deviceId);
}
