package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.FollowerModel;

import retrofit2.Call;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by FrostEagle on 19.10.2016.
 */

public interface FollowClient {
    @PUT("user/{id}")
    Call<FollowerModel> postFollower(@Path("id") String owner);
}
