package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.babyModels.ChildModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface NewBabyCreateClient {
    @FormUrlEncoded
    @POST("user/child")
    Call<ChildModel> registerNewBaby(@Field("name") String name ,@Field("gender") Integer gender, @Field("date") String date);

    @Multipart
    @POST("user/child")
    Call<ChildModel> registerNewBaby(@Part("name") RequestBody name , @Part("gender") RequestBody gender, @Part("date") RequestBody date, @Part MultipartBody.Part file);
}
