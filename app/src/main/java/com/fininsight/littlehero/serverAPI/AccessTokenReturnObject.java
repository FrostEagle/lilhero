package com.fininsight.littlehero.serverAPI;

import android.content.Context;
import android.content.SharedPreferences;

import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.models.AccessToken;

/**
 * Created by FrostEagle on 11.10.2016.
 */

public class AccessTokenReturnObject {
    public static AccessToken accessTokenReturn(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.TOKENREFRESH, Context.MODE_PRIVATE);
        AccessToken accessToken = new AccessToken();
        accessToken.setAccessToken(prefs.getString(Constants.oauth_accesstoken, ""));
        accessToken.setRefreshToken(prefs.getString(Constants.oauth_refreshtoken, ""));
        accessToken.setTokenType(prefs.getString(Constants.oauth_tokentype, ""));
        accessToken.setClientID(Constants.clientID);
        accessToken.setClientSecret(Constants.clientSecret);
        accessToken.setScope(Constants.scope);
        return accessToken;
    }
}
