package com.fininsight.littlehero.serverAPI.classForRequestInterface.get;

import com.fininsight.littlehero.models.FollowersModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface GetFollowersClient {
    @GET("user/{id}/followers")
    Call<ArrayList<FollowersModel>> getUserModels(@Path("id") String owner, @Query("page") int page);
}
