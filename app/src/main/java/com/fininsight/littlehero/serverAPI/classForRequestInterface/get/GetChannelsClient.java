package com.fininsight.littlehero.serverAPI.classForRequestInterface.get;


import com.fininsight.littlehero.models.postModels.ChannelsModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GetChannelsClient {

    @GET("channels")
    Call<ChannelsModel> getChanel();
}
