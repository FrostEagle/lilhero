package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.UserModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


public interface EditUserClient {
    @Multipart
    @POST("user/edit")
    Call<UserModel> editProfileWithPhotoCover (@Query("device_id") String device_id, @Part("login") RequestBody login , @Part("email") RequestBody email, @Part("name") RequestBody name, @Part("bio") RequestBody bio, @Part MultipartBody.Part avatar, @Part MultipartBody.Part cover);
    @Multipart
    @POST("user/edit")
    Call<UserModel> editProfileWithPhotoAll (@Query("device_id") String device_id, @Part("login") RequestBody login , @Part("email") RequestBody email, @Part("name") RequestBody name, @Part("bio") RequestBody bio, @Part MultipartBody.Part avatar);
    @Multipart
    @POST("user/edit")
    Call<UserModel> editProfileWithCoverAll (@Query("device_id") String device_id, @Part("login") RequestBody login , @Part("email") RequestBody email, @Part("name") RequestBody name, @Part("bio") RequestBody bio, @Part MultipartBody.Part cover);
    @Multipart
    @POST("user/edit")
    Call<UserModel> editProfileWithOnlyPhotoCover (@Query("device_id") String device_id, @Part MultipartBody.Part avatar, @Part MultipartBody.Part cover);
    @Multipart
    @POST("user/edit")
    Call<UserModel> editProfileWithCover (@Query("device_id") String device_id, @Part MultipartBody.Part cover);
    @Multipart
    @POST("user/edit")
    Call<UserModel> editProfileWithPhoto (@Query("device_id") String device_id, @Part MultipartBody.Part avatar);

}
