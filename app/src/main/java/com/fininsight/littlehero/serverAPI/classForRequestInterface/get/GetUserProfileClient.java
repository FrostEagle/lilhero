package com.fininsight.littlehero.serverAPI.classForRequestInterface.get;


import com.fininsight.littlehero.models.UserModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GetUserProfileClient {
    @GET("user/{id}")
    Call<UserModel> getUserModels(@Path("id") String owner);
}
