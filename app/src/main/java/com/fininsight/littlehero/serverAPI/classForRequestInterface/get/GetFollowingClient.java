package com.fininsight.littlehero.serverAPI.classForRequestInterface.get;

import com.fininsight.littlehero.models.FollowerModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;



public interface GetFollowingClient {
    @GET("user/{id}/following")
    Call<ArrayList<FollowerModel>> getUserModels( @Path("id") String owner, @Query("page") int page);
}
