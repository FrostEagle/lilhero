package com.fininsight.littlehero.serverAPI.classForRequestInterface.get;

import com.fininsight.littlehero.models.postModels.PostModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by FrostEagle on 21.10.2016.
 */

public interface GetPostsProfileClient {
    @GET("posts/{user_id}")
    Call<ArrayList<PostModel>> getPost(@Path("user_id") String user_id, @Query("page") int page);
}
