package com.fininsight.littlehero.serverAPI.classForDataApi;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class RegistrationResponce implements Serializable {
        @SerializedName("_id")
        private String username;
        @SerializedName("login")
        private String password;
        @SerializedName("name")
        private String message;
        @SerializedName("email")
        private int responseCode;

        public RegistrationResponce(String username, String password, String message, int responseCode){
            this.username = username;
            this.password = password;
            this.message = message;
            this.responseCode = responseCode;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(int responseCode) {
            this.responseCode = responseCode;
        }

}
