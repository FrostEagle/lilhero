package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.postModels.LikeModel;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface LikeClient {
    @POST("post/{id}/like")
    Call<LikeModel> likePost(@Path("id") String id);

}
