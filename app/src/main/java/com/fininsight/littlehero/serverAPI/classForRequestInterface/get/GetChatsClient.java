package com.fininsight.littlehero.serverAPI.classForRequestInterface.get;

import com.fininsight.littlehero.models.chatModels.ChatModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;


public interface GetChatsClient {
    @GET("chats")
    Call<ArrayList<ChatModel>> getChatsModel();
}
