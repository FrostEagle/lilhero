package com.fininsight.littlehero.serverAPI;


import com.fininsight.littlehero.models.ErrorModel;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by FrostEagle on 12.10.2016.
 */

public class ErrorUtils {
    public static ErrorModel parseError(Response<?> response) {
        Converter<ResponseBody, ErrorModel> converter = ServiceGenerator.getRetrofit().responseBodyConverter(ErrorModel.class, new Annotation[0]);

        ErrorModel error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorModel();
        }

        return error;
    }
}
