package com.fininsight.littlehero.serverAPI.classForRequestInterface.message;

import com.fininsight.littlehero.models.chatModels.MessageListInChatModel;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by FrostEagle on 28.10.2016.
 */

public interface GetMessageClient {
    @GET("chat/{id}")
    Call<MessageListInChatModel> getMessage(@Path("id") String owner);
}
