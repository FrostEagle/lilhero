package com.fininsight.littlehero.serverAPI.classForRequestInterface.message;

import com.fininsight.littlehero.models.messageModels.ChatMessageModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface NewMessageClient {
    @FormUrlEncoded
    @POST("message")
    Call<ChatMessageModel> newMessageAddChatId(@Field("chat") String chatId, @Field("message") String message);
    @FormUrlEncoded
    @POST("message")
    Call<ChatMessageModel> newMessageAddWithUserId(@Field("user") String chatId, @Field("message") String message);

    @Multipart
    @POST("message")
    Call<ChatMessageModel> newMessageAdd(@Part("user") RequestBody channel,  @Part("message") RequestBody message, @Part MultipartBody.Part file);

}
