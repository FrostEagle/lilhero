package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.babyModels.PregnancyModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by FrostEagle on 12.10.2016.
 */

public interface PregnancyDateClient {
    @FormUrlEncoded
    @POST("user/pregnancy")
    Call<PregnancyModel> registerNewBaby(@Field("date") String date);
}
