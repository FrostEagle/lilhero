package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.EmailRecoveryModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;



public interface ResetEmailClient {
    @FormUrlEncoded
    @POST("reset")
    Call<EmailRecoveryModel> recoveryEmail(@Field("email") String email);
}
