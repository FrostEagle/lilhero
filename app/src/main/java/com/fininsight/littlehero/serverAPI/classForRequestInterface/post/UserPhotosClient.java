package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.UserModel;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by FrostEagle on 17.10.2016.
 */

public interface UserPhotosClient {
    @Multipart
    @POST("user/photos")
    Call<UserModel> registerNewBaby(@Part MultipartBody.Part file);
}
