package com.fininsight.littlehero.serverAPI;

import android.content.Context;
import android.content.SharedPreferences;

import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.APIOauthClient;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.Route;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ServiceGenerator {
    public static final String API_BASE_URL = "http://little-hero.gmg-spb.ru/api/";

    private static Context mContext;
    private static AccessToken mToken;

    public static Retrofit retrofit;
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static LoggingInterceptor login = new LoggingInterceptor();

    private static Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());


    public static <S> S createService(Class<S> serviceClass) {
        retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
    public static <S> S createServiceAuth(Class<S> serviceClass) {
         Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://little-hero.gmg-spb.ru/")
                .addConverterFactory(GsonConverterFactory.create());
        retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }
    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static <S> S createService(Class<S> serviceClass, AccessToken accessToken, Context c) {
        if(accessToken != null) {
            mContext = c;
            mToken = accessToken;
            final AccessToken token = accessToken;
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Accept", "application/json")
                            .header("Content-type", "application/json")
                            .header("Authorization", token.getTokenType() + " " + token.getAccessToken())
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }

            });
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request();
                    Response response = chain.proceed(request);
                    final String responseString = new String(response.body().bytes());
                    return response.newBuilder()
                            .body(ResponseBody.create(response.body().contentType(), responseString))
                            .build();
                }
            });
            httpClient.authenticator(new Authenticator() {
                @Override
                public Request authenticate(Route route, Response response) throws IOException {
                    if(responseCount(response) >= 2) {
                        return null;
                    }
                    APIOauthClient tokenClient = createService(APIOauthClient.class);
                    Call<AccessToken> call = tokenClient.getRefreshAccessToken(mToken.getRefreshToken(), mToken.getClientID(),  mToken.getClientSecret(), mToken.getScope(), "refresh_token");
                    try {
                        retrofit2.Response<AccessToken> tokenResponse = call.execute();
                        if(tokenResponse.code() == 200) {
                            AccessToken newToken = tokenResponse.body();
                            mToken = newToken;
                            SharedPreferences prefs = mContext.getSharedPreferences(Constants.TOKENREFRESH, Context.MODE_PRIVATE);
                            prefs.edit().putBoolean(Constants.oauth_loggedin, true).apply();
                            prefs.edit().putString(Constants.oauth_accesstoken, newToken.getAccessToken()).apply();
                            prefs.edit().putString(Constants.oauth_refreshtoken, newToken.getRefreshToken()).apply();
                            prefs.edit().putString(Constants.oauth_tokentype, newToken.getTokenType()).apply();

                            return response.request().newBuilder()
                                    .header("Authorization", newToken.getTokenType() + " " + newToken.getAccessToken())
                                    .build();
                        } else {
                            return null;
                        }
                    } catch(IOException e) {
                        return null;
                    }
                }
            });
        }
        OkHttpClient client = httpClient.build();
        retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }
    private static int responseCount(Response response) {
        int result = 1;
        while ((response = response.priorResponse()) != null) {
            result++;
        }
        return result;
    }
    private static class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request request = chain.request();
            Response response = chain.proceed(request);
            final String responseString = new String(response.body().bytes());
            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), responseString))
                    .build();
        }
    }
}
