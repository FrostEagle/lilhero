package com.fininsight.littlehero.serverAPI.classForRequestInterface.get;

import com.fininsight.littlehero.models.postModels.PostModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by FrostEagle on 21.10.2016.
 */

public interface GetPostsClient {
    @FormUrlEncoded
    @POST("posts")
    Call<ArrayList<PostModel>> getPost(@Query("page") int page, @Field("pregnant") boolean pregnant, @Field("infant") boolean infant);
}
