package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;


import com.fininsight.littlehero.models.RegisterNewDeviceModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegisterNewDeviceClient {
    @FormUrlEncoded

    @POST("user/device")
    Call<RegisterNewDeviceModel> registerNewDevice(@Field("device_id") String device_id,
                                                   @Field("name") String name,
                                                   @Field("client_id") String push_id);
    @FormUrlEncoded
    @POST("user/device")
    Call<RegisterNewDeviceModel> registerNewDevice(   @Field("device_id") String device_id,
                                                 @Field("name") String name);

}
