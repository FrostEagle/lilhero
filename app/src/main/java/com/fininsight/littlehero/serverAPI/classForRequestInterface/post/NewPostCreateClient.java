package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.postModels.PostModelTest;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface NewPostCreateClient {
    @FormUrlEncoded
    @POST("post")
    Call<PostModelTest> newPostAdd(@Field("channel") String channel, @Field("message") String message);

    @Multipart
    @POST("post")
    Call<PostModelTest> newPostAdd(@Part("channel") RequestBody channel, @Part("task") RequestBody task, @Part("message") RequestBody message, @Part MultipartBody.Part file);
    @Multipart
    @POST("post")
    Call<PostModelTest> newPostAdd(@Part("channel") RequestBody channel, @Part("message") RequestBody message, @Part MultipartBody.Part file);
}
