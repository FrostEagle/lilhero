package com.fininsight.littlehero.serverAPI.classForRequestInterface.post;

import com.fininsight.littlehero.models.RegistrationModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by FrostEagle on 16.10.2016.
 */

public interface EditUserPasswordClient {
    @FormUrlEncoded
    @POST("user/edit")
    Call<RegistrationModel> createNewUser(@Query("device_id") String deviceId, @Field("password") String password);
}
