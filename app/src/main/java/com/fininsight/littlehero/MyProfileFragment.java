package com.fininsight.littlehero;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.adapter.ChildrenMyProfileAdapter;
import com.fininsight.littlehero.application.GlobalValue;
import com.fininsight.littlehero.expandableMemo.MemoryChildrenFragment;
import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.fragmentFolower.FollowerFragment;
import com.fininsight.littlehero.fragmentFolower.FollowersFragment;
import com.fininsight.littlehero.menu_tab_four.MyPostFragment;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.ErrorModel;
import com.fininsight.littlehero.models.UserModel;
import com.fininsight.littlehero.models.babyModels.ChildModel;
import com.fininsight.littlehero.otherClass.PhotoProfileViewerActivity;
import com.fininsight.littlehero.photos.ProcessingPhotos;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ErrorUtils;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetUserClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.UserPhotosClient;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fininsight.littlehero.extras.Constants.SELECT_FILE;


public class MyProfileFragment extends Fragment implements View.OnClickListener{
    private View view;
    private Fragment fragment = null;
    private Class fragmentClass = null;
    private FragmentActivity myContext;
    private TextView tvMainName, tvNameSecond,tvCountFollowers, tvCountSubscriptions, tvCountPosts, tvForCountPosts, tvCountDone, tvTime;
    private FragmentTransaction fragTransaction;
    private LinearLayout linerAchievementsIcon, linerAchievementsIconText;
    private RelativeLayout rlMemory, rlAboutApp, rlSettings, rlAchievementsAll;
    private ImageView imgPhotoGirl, imgPhotoBoy;
    private String androidId;
    private AccessToken accessToken;
    private RecyclerView rvKids;
    private List<ChildModel> childrenList;
    private ChildrenMyProfileAdapter adapter;
    private GetUserClient getUserClient;
    private Call<UserModel> call;
    private LinearLayout topLinearLayout;
    private HorizontalScrollView scrollView;
    private ImageView imgAddNewPhoto;
    private int whatPhotoCount = 0;
    private String mCurrentPhotoPath;
    private Bitmap bmpPostPhoto;
    private ProcessingPhotos mProcessingPhotos;
    private Fragment fragmentActivity = this;
    private ImageView imgAvatar, imgCover;
    private UserModel getUserModels;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        initUI();
        mProcessingPhotos = new ProcessingPhotos(getContext());
        childrenList = new ArrayList<>();
        rvKids.setLayoutManager(new LinearLayoutManager(getContext()));
        androidId = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());
        if (GlobalValue.userModel!=null) {
           initValueUI(GlobalValue.userModel);
        } //else {
            getUserClient = ServiceGenerator.createService(GetUserClient.class, accessToken, getContext());
            call = getUserClient.getUserModels(androidId);
            call.enqueue(new Callback<UserModel>() {
                @Override
                public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                    getUserModels = response.body();
                    if (response.isSuccessful()) {
                        GlobalValue.userModel = response.body();
                            initValueUI(getUserModels);
                    }
                }
                @Override
                public void onFailure(Call<UserModel> call, Throwable t) {
                }
            });
      //  }
        imgAddNewPhoto.setOnClickListener(this);
        rlMemory.setOnClickListener(this);
        rlAboutApp.setOnClickListener(this);
        tvCountPosts.setOnClickListener(this);
        tvForCountPosts.setOnClickListener(this);
        rlSettings.setOnClickListener(this);
        tvCountFollowers.setOnClickListener(this);
        tvCountSubscriptions.setOnClickListener(this);
        rlAchievementsAll.setOnClickListener(this);
        linerAchievementsIcon.setOnClickListener(this);
        linerAchievementsIconText.setOnClickListener(this);

       return view;
    }

    private void initValueUI(UserModel getUserModels) {
        tvCountFollowers.setText(String.valueOf(getUserModels.getUser().getFollowers_counter()));
        tvCountSubscriptions.setText(String.valueOf(getUserModels.getUser().getFollowing_counter()));
        tvCountPosts.setText(String.valueOf(getUserModels.getUser().getPosts_counter()));
        tvMainName.setText(getUserModels.getUser().getName());
        tvCountDone.setText(String.valueOf(getUserModels.getUser().getDone_counter()));
        tvTime.setText(String.valueOf(getUserModels.getUser().getTime_counter()));
        tvNameSecond.setText(getUserModels.getUser().getLogin());
        //hack witout doubles photos
        topLinearLayout = new LinearLayout(getContext());
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        scrollView.removeAllViews();
        scrollView.addView(topLinearLayout);
        if (getUserModels.getUser().getPhotos()>0) {
            for (int i = 0;i<getUserModels.getUser().getPhotos();i++) {
                photoToPostAddImageViewFromServer(getUserModels.getUser().getLogin(),i);
            }
        }
        if (getUserModels.getUser().getAvatar_file()!=null) {
            Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/avatars/"+getUserModels.getUser().getLogin()).into(imgAvatar);
        } else  imgAvatar.setImageDrawable(getResources().getDrawable(R.drawable.no_photo_avatar));
        if (getUserModels.getUser().getCover_file()!=null) {
            Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/covers/"+getUserModels.getUser().getLogin()).into(imgCover);
        } else  imgCover.setImageDrawable(getResources().getDrawable(R.drawable.cover_profile_main));
        childrenList = getUserModels.getUser().getChilds();
        adapter = new ChildrenMyProfileAdapter(childrenList, getContext());
        rvKids.setAdapter(adapter);
    }

    private void initUI() {
        tvCountDone = (TextView) view.findViewById(R.id.tv_card_count_profile);
        tvTime = (TextView) view.findViewById(R.id.tv_time_count_profile);
        imgAvatar = (ImageView) view.findViewById(R.id.img_avatar);
        imgCover = (ImageView) view.findViewById(R.id.img_cover);
        topLinearLayout = new LinearLayout(getContext());
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        imgAddNewPhoto = (ImageView) view.findViewById(R.id.img_add_photo);
        scrollView = (HorizontalScrollView) view.findViewById(R.id.sc_photo);
        scrollView.addView(topLinearLayout);
        rvKids = (RecyclerView) view.findViewById(R.id.rv_kids);
        tvMainName = (TextView) view.findViewById(R.id.tv_main_name);
        tvNameSecond = (TextView) view.findViewById(R.id.tv_name_second);
        rlSettings = (RelativeLayout) view.findViewById(R.id.rl_settings);
        rlAboutApp = (RelativeLayout) view.findViewById(R.id.rl_about_app);
        tvCountSubscriptions = (TextView) view.findViewById(R.id.tv_count_subscriptions);
        tvCountPosts = (TextView) view.findViewById(R.id.tv_count_notation_profile);
        tvForCountPosts = (TextView) view.findViewById(R.id.tv_for_profile_post);
        tvCountFollowers = (TextView) view.findViewById(R.id.tv_count_subscribers);
        linerAchievementsIcon = (LinearLayout) view.findViewById(R.id.liner_for_achivements_imgview);
        linerAchievementsIconText = (LinearLayout) view.findViewById(R.id.linerFor_text_below_achivements);
        rlAchievementsAll = (RelativeLayout) view.findViewById(R.id.rl_achievements_all);
        rlMemory = (RelativeLayout) view.findViewById(R.id.rl_pamyatka);
    }
    private void postNewPhoto() {
        UserPhotosClient dateClient = ServiceGenerator.createService(UserPhotosClient.class, accessToken, getContext());
        if (mCurrentPhotoPath != null) {
            File destination = new File(mProcessingPhotos.compressImage(mCurrentPhotoPath));
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), destination);
            MultipartBody.Part body = MultipartBody.Part.createFormData("photos", destination.getName(), requestFile);
            call = dateClient.registerNewBaby(body);
            call.enqueue(new Callback<UserModel>() {
                @Override
                public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                    ResponseBody errorBo = response.errorBody();
                    int recponce = response.code();
                    if (response.errorBody() != null) {
                        ErrorModel error = ErrorUtils.parseError(response);
//                                String errorText = error.status();
//                                if (errorText.equals("invalid_credentials")) {
                        //                              }
                    }
                    if (response.isSuccessful()) {
                        photoToPostAddImageView();
                    }
                }
                @Override
                public void onFailure(Call<UserModel> call, Throwable t) {
                }
            });

        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            
            case R.id.img_add_photo:
                selectImage();
                break;
            case R.id.rl_about_app:
                AboutAppFragment aboutFrag = new AboutAppFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, aboutFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.rl_pamyatka:
                MemoryChildrenFragment memoFrag = new MemoryChildrenFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.add(R.id.frame_for_fragment, memoFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.tv_for_profile_post:
                Bundle argsPost = new Bundle();
                argsPost.putString("id", getUserModels.getUser().get_id());
                argsPost.putString("login", getUserModels.getUser().getLogin());
                MyPostFragment postFrag = new MyPostFragment();
                postFrag.setArguments(argsPost);
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.add(R.id.frame_for_fragment, postFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.tv_count_notation_profile:
                Bundle argsPost1 = new Bundle();
                argsPost1.putString("id", getUserModels.getUser().get_id());
                argsPost1.putString("login", getUserModels.getUser().getLogin());
                MyPostFragment postFrag1 = new MyPostFragment();
                postFrag1.setArguments(argsPost1);
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.add(R.id.frame_for_fragment, postFrag1);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.rl_achievements_all:
                AchievementsFragment achievFrag = new AchievementsFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, achievFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.liner_for_achivements_imgview:
                AchievementsFragment achievFrag1 = new AchievementsFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, achievFrag1);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.linerFor_text_below_achivements:
                AchievementsFragment achievFrag2 = new AchievementsFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, achievFrag2);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.tv_count_subscribers:
                Bundle args = new Bundle();
                args.putString("id", getUserModels.getUser().get_id());
                FollowersFragment msFragFollowers = new FollowersFragment();
                msFragFollowers.setArguments(args);
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, msFragFollowers );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.tv_count_subscriptions:
                Bundle args1 = new Bundle();
                args1.putString("id", getUserModels.getUser().get_id());
                FollowerFragment msFragFollower = new FollowerFragment();
                msFragFollower.setArguments(args1);
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, msFragFollower );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.rl_settings:
                MySettingsFragment msFrag = new MySettingsFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.add(R.id.frame_for_fragment, msFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
        }
    }
    public void selectImage() {
        final CharSequence[] items = {"Сделать фото", "Выбрать фото из галереи", "Отмена"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Добавьте фото!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Сделать фото")) {
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                            File photoFile = null;
                            try {
                                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                photoFile = mProcessingPhotos.createImageFile();
                                Uri contentUri = Uri.fromFile(photoFile);
                                mediaScanIntent.setData(contentUri);
                                getContext().sendBroadcast(mediaScanIntent);
                            } catch (IOException ignored) {
                            }
                            if (photoFile != null) {
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                                getActivityStarterFragment().startActivityForResult(takePictureIntent, 0);
                            }
                        }
                    }
                } else if (items[item].equals("Выбрать фото из галереи")) {
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        getActivityStarterFragment().startActivityForResult(Intent.createChooser(intent, "Select File"),  Constants.SELECT_FILE);
                    }
                } else if (items[item].equals("Отмена")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private Fragment getActivityStarterFragment() {
        if (getParentFragment() != null) {
            return getParentFragment();
        }
        return this;
    }
    public void callOnActivityResultOnChildFragments(Fragment parent, int requestCode, int resultCode, Intent data) {
        FragmentManager childFragmentManager = parent.getChildFragmentManager();
        if (childFragmentManager != null) {
            List<Fragment> childFragments = childFragmentManager.getFragments();
            if (childFragments == null) {
                return;
            }
            for (Fragment child : childFragments) {
                if (child != null && !child.isDetached() && !child.isRemoving()) {
                    child.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }
    //Для создания ImageView
    private void photoToPostAddImageView() {
        final RelativeLayout relativeLayout = new RelativeLayout(getContext());
        final RoundedImageView imageView = new RoundedImageView (getContext());
        imageView.setTag(whatPhotoCount);
        imageView.setCornerRadius(10);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(bmpPostPhoto);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toViewer = new Intent(getContext(), PhotoProfileViewerActivity.class);
                toViewer.putExtra("id", getUserModels.getUser().getLogin());
                toViewer.putExtra("count", getUserModels.getUser().getPhotos());
                startActivity(toViewer);
            }
        });
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getPx(100), getPx(100));
        lp.setMargins(getPx(10),getPx(0),getPx(0),getPx(0));
        relativeLayout.addView(imageView,lp);
        topLinearLayout.addView(relativeLayout);
        whatPhotoCount ++;
    }
    private void photoToPostAddImageViewFromServer(String id, int count) {
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        RoundedImageView imageView = new RoundedImageView (getContext());
        imageView.setTag(whatPhotoCount);
        imageView.setCornerRadius(10);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toViewer = new Intent(getContext(), PhotoProfileViewerActivity.class);
                toViewer.putExtra("id", getUserModels.getUser().getLogin());
                toViewer.putExtra("count", getUserModels.getUser().getPhotos());
                startActivity(toViewer);
            }
        });
        Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/photos/"+id+"/"+count).into(imageView);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getPx(100), getPx(100));
        lp.setMargins(getPx(10),getPx(0),getPx(0),getPx(0));
        relativeLayout.addView(imageView,lp);
        topLinearLayout.addView(relativeLayout);
        whatPhotoCount ++;
    }

    //set dp calculate automatically
    public int getPx(int dimensionDp) {
        float density = getResources().getDisplayMetrics().density;
        return (int) (dimensionDp * density + 0.5f);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callOnActivityResultOnChildFragments(this, requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            int REQUEST_CAMERA = 0;
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                bmpPostPhoto = bm;
                photoToPostAddImageView();
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(getContext(), selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                bmpPostPhoto = bm;
                mCurrentPhotoPath = selectedImagePath;
                postNewPhoto();
            }
        } else {
            File fdelete = new File(mCurrentPhotoPath);
            if (fdelete.exists()) {
                boolean deleted = fdelete.delete();
                if (deleted) {
                    Log.d("camera", "deleted");
                    getContext().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(mCurrentPhotoPath))));
                } else {
                    Log.d("camera", "not deleted");
                }
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                            photoFile = mProcessingPhotos.createImageFile();
                            Uri contentUri = Uri.fromFile(photoFile);
                            mediaScanIntent.setData(contentUri);
                            getContext().sendBroadcast(mediaScanIntent);
                        } catch (IOException ignored) {
                        }
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, 0);
                        }
                    }
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                }
                break;
        }
    }
}
