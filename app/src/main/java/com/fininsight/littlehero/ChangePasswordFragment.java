package com.fininsight.littlehero;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.ErrorModel;
import com.fininsight.littlehero.models.RegistrationModel;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ErrorUtils;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.EditUserPasswordClient;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePasswordFragment extends Fragment implements View.OnClickListener{
    private RelativeLayout imgBack;
    private EditText edtChangePassword;
    private TextView tvSettingSave;
    private ExpandableRelativeLayout expandableLayout;
    private TextView tvError;
    private String android_id;
    private AccessToken accessToken;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        imgBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        tvError = (TextView) view.findViewById(R.id.tv_error);
        //Клавиатура по верх разметки
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        expandableLayout = (ExpandableRelativeLayout) view.findViewById(R.id.expandableLayout);
        expandableLayout.collapse();
        edtChangePassword = (EditText) view.findViewById(R.id.edt_change_password);
        tvSettingSave = (TextView) view.findViewById(R.id.tv_settings_save);
        edtChangePassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
            expandableLayout.collapse();
            }
        });
        tvSettingSave.setOnClickListener(this);
        imgBack.setOnClickListener(this);

        return view;
    }

    private void changePassword(String changePass) {
        android_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());
        EditUserPasswordClient regClient = ServiceGenerator.createService(EditUserPasswordClient.class, accessToken, getContext());
        Call<RegistrationModel> call = regClient.createNewUser(android_id,changePass);
        call.enqueue(new Callback<RegistrationModel>() {
            @Override
            public void onResponse(Call<RegistrationModel> call, Response<RegistrationModel> response) {
                RegistrationModel registrationModel = response.body();
                if (response.errorBody()!=null) {
                    ErrorModel error = ErrorUtils.parseError(response);
                    String errorText = error.status();
                    if (errorText.equals("duplicate_email")) {
                        expandableLayout.expand();
                    }
                }
                if (response.isSuccessful()) {

                    getActivity().getSupportFragmentManager().popBackStack();

                }
            }
            @Override
            public void onFailure(Call<RegistrationModel> call, Throwable t) {
            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_settings_save:
                if (!isValidPassword(edtChangePassword.getText().toString())) {
                    expandableLayout.expand();
                } else {
                    changePassword(edtChangePassword.getText().toString());
                }
                break;
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
    private boolean isValidPassword(String pass) {
        boolean peremPass = false;
        if (pass.length() > 7) {
            //if (pass.matches("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})$")) {
            if (pass.matches("^(?=.*[0-9])(?=.*[a-zA-Z])(?=\\S+$).{8,}$")) {
                peremPass = true;
            }
        }
        return peremPass;
    }
}
