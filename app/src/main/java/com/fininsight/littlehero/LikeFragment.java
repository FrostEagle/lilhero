package com.fininsight.littlehero;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.fininsight.littlehero.adapter.FollowerAdapter;
import com.fininsight.littlehero.classForData.Follower;
import com.fininsight.littlehero.models.FollowerModel;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;


public class LikeFragment extends Fragment implements View.OnClickListener{

    private RecyclerView rvFollowers;
    private RelativeLayout rlBack;
    private List<FollowerModel> followerList;
    private FollowerAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_like, container, false);
        rlBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        rvFollowers = (RecyclerView) view.findViewById(R.id.rv_like);
        followerList = new ArrayList<>();
        adapter = new FollowerAdapter(followerList, getContext());
        rvFollowers.setAdapter(adapter);
        rvFollowers.setLayoutManager(new LinearLayoutManager(getContext()));
        rvFollowers.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).build());
        rlBack.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
    private List<Follower> followerListCreate() {
        List<Follower> data = new ArrayList<>();
        data.add(new Follower(1,"Jane",0,0,R.drawable.main_avatar));
        data.add(new Follower(2,"Sara",0,1,R.drawable.image));
        data.add(new Follower(3,"Elizabeth Taylor",0,0,R.drawable.image2));
        data.add(new Follower(4,"Ariel",1,0,R.drawable.image3));
        return data;
    }
}
