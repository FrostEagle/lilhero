package com.fininsight.littlehero;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CardCompleteActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView tvSend;
    private RelativeLayout rlBottom;
    private ImageView  imgVkSocial, imgFbSocial, imgOdnoklassnikiSocial, imgInstagramSocial, imgBack;
    private int eachFinal = 0;
    private String mCurrentPhotoPath;
    private Bitmap bmpToServer;
    private Bitmap bmpPostPhoto;
    private int SELECT_FILE = 1;
    private ImageView addPhotoPost;
    private LinearLayout topLinearLayout;
    private int whatPhotoCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_complete);
        tvSend = (TextView) findViewById(R.id.tv_send);
        rlBottom = (RelativeLayout) findViewById(R.id.rl_bottom_header);
        imgBack = (ImageView) findViewById(R.id.imgv_back);
        imgVkSocial = (ImageView) findViewById(R.id.img_vk_social_settings);
        imgFbSocial = (ImageView) findViewById(R.id.img_facebook_social_settings);
        imgOdnoklassnikiSocial = (ImageView) findViewById(R.id.img_odnoklassniki_social_settings);
        imgInstagramSocial = (ImageView) findViewById(R.id.img_instagram_social_settings);
        addPhotoPost = (ImageView) findViewById(R.id.add_photo_post);
        HorizontalScrollView scrollView = (HorizontalScrollView) findViewById(R.id.sc_photo_post);
        topLinearLayout = new LinearLayout(CardCompleteActivity.this);
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        scrollView.addView(topLinearLayout);
        addPhotoPost.setOnClickListener(this);
        rlBottom.setOnClickListener(this);
        imgVkSocial.setOnClickListener(this);
        imgFbSocial.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgOdnoklassnikiSocial.setOnClickListener(this);
        imgInstagramSocial.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_bottom_header:
                finish();
                break;
            case R.id.imgv_back:
                finish();
                break;
            case R.id.add_photo_post:
                selectImage();
                break;
            case R.id.img_vk_social_settings:
                chechWhat();
                imgVkSocial.setImageDrawable(getResources().getDrawable(R.drawable.vk_social_pressed));
                break;
            case R.id.img_facebook_social_settings:
                chechWhat();
                imgFbSocial.setImageDrawable(getResources().getDrawable(R.drawable.facebook_social_pressed));
                break;
            case R.id.img_odnoklassniki_social_settings:
                chechWhat();
                imgOdnoklassnikiSocial.setImageDrawable(getResources().getDrawable(R.drawable.odnoklassniki_social_pressed));
                break;
            case R.id.img_instagram_social_settings:
                chechWhat();
                imgInstagramSocial.setImageDrawable(getResources().getDrawable(R.drawable.instagramm_social_pressed));
                break;
        }
    }

    private void chechWhat() {
        if (eachFinal == 0) {
            tvSend.setText("Завершить и поделиться");
            eachFinal++;
        }
    }
    //Для создания ImageView
    private void photoToPostAddImageView() {
        final RelativeLayout relativeLayout = new RelativeLayout(CardCompleteActivity.this);
        final RoundedImageView imageView = new RoundedImageView (CardCompleteActivity.this);
        final ImageView imageViewDelete = new ImageView(CardCompleteActivity.this);
        imageView.setTag(whatPhotoCount);
        imageView.setCornerRadius(10);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageViewDelete.setTag(whatPhotoCount);
        imageView.setImageBitmap(bmpPostPhoto);
        imageViewDelete.setImageResource(R.drawable.delete_image_post);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getPx(95), getPx(95));
        lp.setMargins(getPx(10),getPx(10),getPx(10),0);
        RelativeLayout.LayoutParams lpDelete = new RelativeLayout.LayoutParams(getPx(25), getPx(25));
        lpDelete.setMargins(getPx(87),getPx(3),0,0);
        relativeLayout.addView(imageView,lp);
        relativeLayout.addView(imageViewDelete,lpDelete);
        topLinearLayout.addView(relativeLayout);
        imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topLinearLayout.removeView((View) v.getParent());
                Log.e("Tag",""+imageView.getTag());
                whatPhotoCount--;
            }
        });
        whatPhotoCount ++;
    }

    //set dp calculate automatically
    public int getPx(int dimensionDp) {
        float density = getResources().getDisplayMetrics().density;
        return (int) (dimensionDp * density + 0.5f);
    }
    private void selectImage() {
        final CharSequence[] items = {"Сделать фото", "Выбрать фото из галереи", "Отмена"};
        AlertDialog.Builder builder = new AlertDialog.Builder(CardCompleteActivity.this);
        builder.setTitle("Добавьте фото!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Сделать фото")) {
                    if (ActivityCompat.checkSelfPermission(CardCompleteActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(CardCompleteActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                            File photoFile = null;
                            try {
                                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                photoFile = createImageFile();
                                Uri contentUri = Uri.fromFile(photoFile);
                                mediaScanIntent.setData(contentUri);
                                sendBroadcast(mediaScanIntent);
                            } catch (IOException ignored) {
                            }
                            if (photoFile != null) {
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                                startActivityForResult(takePictureIntent, 0);
                            }
                        }
                    }
                } else if (items[item].equals("Выбрать фото из галереи")) {
                    if (ActivityCompat.checkSelfPermission(CardCompleteActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(CardCompleteActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                    }
                } else if (items[item].equals("Отмена")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            int REQUEST_CAMERA = 0;
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                bmpPostPhoto = bm;
                rotateImageToServer(mCurrentPhotoPath);
                photoToPostAddImageView();
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                bmpPostPhoto = bm;
                mCurrentPhotoPath = selectedImagePath;
                rotateImageToServer(selectedImagePath);
                photoToPostAddImageView();
            }
        } else {
            File fdelete = new File(mCurrentPhotoPath);
            if (fdelete.exists()) {
                boolean deleted = fdelete.delete();
                if (deleted) {
                    Log.d("camera", "deleted");
                    this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(mCurrentPhotoPath))));
                } else {
                    Log.d("camera", "not deleted");
                }
            }
        }
    }

    public void rotateImageToServer(String path) {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert ei != null;
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                bmpToServer = rotateImage(bmpPostPhoto, 270);
                break;
            case 1:
                bmpToServer = bmpPostPhoto;
                break;
            case 0:
                bmpToServer = bmpPostPhoto;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                bmpToServer = rotateImage(bmpPostPhoto, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bmpToServer = rotateImage(bmpPostPhoto, 180);
                break;
        }
    }

    public Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        return retVal;
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                            photoFile = createImageFile();
                            Uri contentUri = Uri.fromFile(photoFile);
                            mediaScanIntent.setData(contentUri);
                            sendBroadcast(mediaScanIntent);
                        } catch (IOException ignored) {
                        }
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, 0);
                        }
                    }
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                }
                break;
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    public String compressImage(String imageUri) {
        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float maxHeight = 2560.0f;
        float maxWidth = 1600.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];
        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        assert scaledBitmap != null;
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 89, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath());
        if (!file.exists()) {
            file.mkdirs();
        }
        return file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg";

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }
}
