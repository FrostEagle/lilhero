package com.fininsight.littlehero.post;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.LikeFragment;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.CommentAdapter;
import com.fininsight.littlehero.likedislike.LikeDislikeClass;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.ErrorModel;
import com.fininsight.littlehero.models.postModels.CommentModel;
import com.fininsight.littlehero.models.postModels.PostModel;
import com.fininsight.littlehero.photos.ProcessingPhotos;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ErrorUtils;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.SendCommentClient;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ViewPostFragment extends Fragment implements View.OnClickListener {
    private int like = 1;
    private RecyclerView rvPostComment;
    private List<CommentModel> commentList;
    private FragmentTransaction fragTransaction;
    private CommentAdapter comAdapter;
    private RelativeLayout rlBack;
    private LinearLayout linerPhoto;
    private ImageView imgMyPostProfileAvatar, imgSuperman, imgLike, imgPhotoFirst, imgPhotoSecond;
    private TextView tvContentPost, tvCountComment, tvNameAuthorPost, tvTime, tvCommentLike;
    private View view;
    private Bundle args;
    private PostModel postModel;
    private TextView tvSend;
    private EditText edtMsg;
    private AccessToken accessToken;
    private String mCurrentPhotoPath;
    private Call<CommentModel> call;
    private ProcessingPhotos mProcessingPhotos;
    private LikeDislikeClass likeDislike;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_post, container, false);
        initUI();
        args = getArguments();
        postModel = (PostModel) args.getSerializable("item");
        commentList = new ArrayList<>();
        commentList = postModel.getComments();
        comAdapter = new CommentAdapter(commentList, getContext());
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());
        likeDislike = new LikeDislikeClass(getContext());
        mProcessingPhotos = new ProcessingPhotos(getContext());
        rvPostComment.setAdapter(comAdapter);
        rvPostComment.setLayoutManager(new LinearLayoutManager(getContext()));
        Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/avatars/"+postModel.getUser_login()).into(imgMyPostProfileAvatar);
        if (postModel.getPhotos() > 1) {
            imgPhotoSecond.setVisibility(View.VISIBLE);
            Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/postphotos/"+postModel.get_id()+"/0").into(imgPhotoFirst);
            Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/postphotos/"+postModel.get_id()+"/1").into(imgPhotoSecond);
            linerPhoto.setVisibility(View.VISIBLE);
        } else if (postModel.getPhotos()==1) {
            Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/postphotos/"+postModel.get_id()+"/0").into(imgPhotoFirst);
            imgPhotoSecond.setVisibility(View.GONE);
            linerPhoto.setVisibility(View.VISIBLE);
        }  else linerPhoto.setVisibility(View.GONE);
        tvContentPost.setText(postModel.getMessage());
        tvCommentLike.setText("Нравится "+postModel.getLikes_counter());
        tvNameAuthorPost.setText(postModel.getUser_login());
        tvTime.setText(postModel.getCreated_at().substring(0,11));
        imgLike.setOnClickListener(this);
        tvCommentLike.setOnClickListener(this);
        rlBack.setOnClickListener(this);
        tvSend.setOnClickListener(this);
        return view;
    }

    private void initUI() {
        edtMsg = (EditText) view.findViewById(R.id.edt_msg);
        tvSend = (TextView) view.findViewById(R.id.tv_send);
        tvNameAuthorPost = (TextView) view.findViewById(R.id.tv_name_author_post);
        tvTime = (TextView) view.findViewById(R.id.tv_time_post);
        imgLike = (ImageView) view.findViewById(R.id.img_like);
        imgPhotoFirst = (ImageView) view.findViewById(R.id.img_first_photo_post);
        imgPhotoSecond = (ImageView) view.findViewById(R.id.img_second_photo_post);
        imgSuperman = (ImageView) view.findViewById(R.id.img_post_superman);
        imgMyPostProfileAvatar = (ImageView) view.findViewById(R.id.img_my_post_profile);
        rlBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        tvCommentLike = (TextView) view.findViewById(R.id.tv_comment_like);
        rvPostComment = (RecyclerView) view.findViewById(R.id.rv_post_comment);
        linerPhoto = (LinearLayout) view.findViewById(R.id.liner_photo_post);
        tvContentPost = (TextView) view.findViewById(R.id.tv_content_post);
        tvCountComment = (TextView) view.findViewById(R.id.tv_count_comment);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_like:
                if (postModel.isMe_like()) {
                    likeDislike.unLikePost(postModel.get_id());
                    postModel.setMe_like(false);
                    imgLike.setImageDrawable(getResources().getDrawable(R.drawable.like_not));
                } else {
                    likeDislike.likePost(postModel.get_id());
                    postModel.setMe_like(true);
                    imgLike.setImageDrawable(getResources().getDrawable(R.drawable.like));
                }
                break;
            case R.id.tv_comment:
                LikeFragment likeFrag = new LikeFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, likeFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.tv_send:
                sendComment();
                edtMsg.setText("");
                break;
        }
    }

    private void sendComment() {
        SendCommentClient postClient = ServiceGenerator.createService(SendCommentClient.class, accessToken, getContext());
        if (mCurrentPhotoPath != null) {
            File destination = new File(mProcessingPhotos.compressImage(mCurrentPhotoPath));
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), destination);
            MultipartBody.Part body = MultipartBody.Part.createFormData("image", destination.getName(), requestFile);
            RequestBody message = RequestBody.create(MediaType.parse("text/plain"), edtMsg.getText().toString());
            call = postClient.sendComment(postModel.get_id(), message, body);
        } else {
            call = postClient.sendComment(postModel.get_id(), edtMsg.getText().toString());
        }
        call.enqueue(new Callback<CommentModel>() {
            @Override
            public void onResponse(Call<CommentModel> call, Response<CommentModel> response) {
                if (response.errorBody() != null) {
                    ErrorModel error = ErrorUtils.parseError(response);
                    String errorText = error.status();
                }
                if (response.isSuccessful()) {
                    commentList.add(response.body());
                    rvPostComment.setAdapter(comAdapter);
                }
            }
            @Override
            public void onFailure(Call<CommentModel> call, Throwable t) {
            }
        });
    }
}
