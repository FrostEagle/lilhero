package com.fininsight.littlehero.post;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.fininsight.littlehero.R;

public class ViewPostActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);
    }
}
