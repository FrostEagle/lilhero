package com.fininsight.littlehero.models;


public class FollowerModel {
    private String _id;
    private String follower_id;
    private String follow_id;
    private UserForFollower following;
    private String relation;

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFollower_id() {
        return follower_id;
    }

    public void setFollower_id(String follower_id) {
        this.follower_id = follower_id;
    }

    public String getFollow_id() {
        return follow_id;
    }

    public void setFollow_id(String follow_id) {
        this.follow_id = follow_id;
    }

    public UserForFollower getFollowing() {
        return following;
    }

    public void setFollowing(UserForFollower following) {
        this.following = following;
    }
}
