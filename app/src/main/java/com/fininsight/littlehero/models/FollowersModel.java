package com.fininsight.littlehero.models;

/**
 * Created by FrostEagle on 19.10.2016.
 */

public class FollowersModel {
    private String _id;
    private String follower_id;
    private String follow_id;
    private UserForFollower followers;
    private String relation;

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public UserForFollower getFollowers() {
        return followers;
    }

    public void setFollowers(UserForFollower followers) {
        this.followers = followers;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getFollower_id() {
        return follower_id;
    }

    public void setFollower_id(String follower_id) {
        this.follower_id = follower_id;
    }

    public String getFollow_id() {
        return follow_id;
    }

    public void setFollow_id(String follow_id) {
        this.follow_id = follow_id;
    }

    public UserForFollower getFollowing() {
        return followers;
    }

    public void setFollowing(UserForFollower following) {
        this.followers = following;
    }
}
