package com.fininsight.littlehero.models;

/**
 * Created by FrostEagle on 15.10.2016.
 */

public class UserModel {
    private GetUserModels user;

    public GetUserModels getUser() {
        return user;
    }

    public void setUser(GetUserModels user) {
        this.user = user;
    }
}
