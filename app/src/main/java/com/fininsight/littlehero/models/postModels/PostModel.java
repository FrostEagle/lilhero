package com.fininsight.littlehero.models.postModels;

import java.io.Serializable;
import java.util.List;



public class PostModel implements Serializable {
    private String _id;
    private String user_id;
    private String channel;
    private String channel_id;
    private String task_id;
    private int expierence;
    private String message;
    private int likes_counter;
    private int comments_counter;
    private int photos;
    private List<LikeModel> likes;
    private List<CommentModel> comments;
    private String user_login;
    private String created_at;
    private String avatar_file;
    private boolean me_like;

    public String getAvatar_file() {
        return avatar_file;
    }

    public void setAvatar_file(String avatar_file) {
        this.avatar_file = avatar_file;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public int getExpierence() {
        return expierence;
    }

    public void setExpierence(int expierence) {
        this.expierence = expierence;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getLikes_counter() {
        return likes_counter;
    }

    public void setLikes_counter(int likes_counter) {
        this.likes_counter = likes_counter;
    }

    public int getComments_counter() {
        return comments_counter;
    }

    public void setComments_counter(int comments_counter) {
        this.comments_counter = comments_counter;
    }

    public int getPhotos() {
        return photos;
    }

    public void setPhotos(int photos) {
        this.photos = photos;
    }

    public List<LikeModel> getLikes() {
        return likes;
    }

    public void setLikes(List<LikeModel> likes) {
        this.likes = likes;
    }

    public List<CommentModel> getComments() {
        return comments;
    }

    public void setComments(List<CommentModel> comments) {
        this.comments = comments;
    }


    public boolean isMe_like() {
        return me_like;
    }

    public void setMe_like(boolean me_like) {
        this.me_like = me_like;
    }

    public String getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.channel_id = channel_id;
    }
}
