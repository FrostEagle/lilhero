package com.fininsight.littlehero.models.chatModels;

import java.util.ArrayList;

/**
 * Created by FrostEagle on 25.10.2016.
 */

public class ChatModel {
    private String _id;
    private ArrayList<ChattersModel> chatters;
    private ChatsMessageModel last_message;
    private boolean non_read;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public ArrayList<ChattersModel> getChatters() {
        return chatters;
    }

    public void setChatters(ArrayList<ChattersModel> chatters) {
        this.chatters = chatters;
    }

    public ChatsMessageModel getLast_message() {
        return last_message;
    }

    public void setLast_message(ChatsMessageModel last_message) {
        this.last_message = last_message;
    }

    public boolean isNon_read() {
        return non_read;
    }

    public void setNon_read(boolean non_read) {
        this.non_read = non_read;
    }
}
