package com.fininsight.littlehero.models.messageModels;


import java.util.ArrayList;

public class ChatMessageModel {
    public boolean incoming;
//    public String message;
//    private String idMessage;
//    private String createdAt;
//    private String photo;
//    private String photoToChat;
//    private String photoToChatThumb;
    public String _id;
    public String chat_id;
    public String user_id;
    public String message;
    public String image_file;
    public ArrayList<String> read;
    public ArrayList<String> hidden;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImage_file() {
        return image_file;
    }

    public void setImage_file(String image_file) {
        this.image_file = image_file;
    }

    public ArrayList<String> getRead() {
        return read;
    }

    public void setRead(ArrayList<String> read) {
        this.read = read;
    }

    public ArrayList<String> getHidden() {
        return hidden;
    }

    public void setHidden(ArrayList<String> hidden) {
        this.hidden = hidden;
    }
//    public String getPhotoToChatThumb() {
//        return photoToChatThumb;
//    }
//
//    public void setPhotoToChatThumb(String photoToChatThumb) {
//        this.photoToChatThumb = photoToChatThumb;
//    }
//    public String getPhotoToChat() {
//        return photoToChat;
//    }
//
//    public void setPhotoToChat(String photoToChat) {
//        this.photoToChat = photoToChat;
//    }
//
//
//    public String getPhoto() {
//        return photo;
//    }
//
//    public void setPhoto(String photo) {
//        this.photo = photo;
//    }
//
//    public String getCreatedAt() {
//        return createdAt;
//    }
//
//    public void setCreatedAt(String createdAt) {
//        this.createdAt = createdAt;
//    }
//
//    public String getIdMessage() {
//        return idMessage;
//    }
//
//    public void setIdMessage(String idMessage) {
//        this.idMessage = idMessage;
//    }

    public boolean isIncoming() {
        return incoming;
    }

    public void setIncoming(boolean incoming) {
        this.incoming = incoming;
    }

    public ChatMessageModel(boolean incoming, String message) {
        super();
        this.incoming = incoming;
        this.message = message;
    }
//    public ChatMessage(boolean incoming, String message, String createdAt) {
//        super();
//        this.incoming = incoming;
//        this.message = message;
//       // this.createdAt = createdAt;
//    }

    public ChatMessageModel(boolean incoming, String message, String photoToChat) {
        super();
        this.incoming = incoming;
        this.message = message;
        this.image_file = photoToChat;

    }
//    public ChatMessage(boolean incoming, String message, String photoToChat, String photo, String photoToChatThumb, String createdAt) {
//        super();
//        this.incoming = incoming;
//        this.message = message;
//        this.photoToChat = photoToChat;
//        this.photo = photo;
//        this.createdAt = createdAt;
//        this.photoToChatThumb = photoToChatThumb;
//    }

    public ChatMessageModel() {
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

}
