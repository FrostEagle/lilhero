package com.fininsight.littlehero.models.chatModels;


import com.fininsight.littlehero.models.messageModels.ChatMessageModel;

import java.util.ArrayList;

public class MessageListInChatModel {
    private String _id;
    private ArrayList<ChattersModel> chatters;
    private ArrayList<ChatMessageModel> messages;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public ArrayList<ChattersModel> getChatters() {
        return chatters;
    }

    public void setChatters(ArrayList<ChattersModel> chatters) {
        this.chatters = chatters;
    }

    public ArrayList<ChatMessageModel> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<ChatMessageModel> messages) {
        this.messages = messages;
    }
}
