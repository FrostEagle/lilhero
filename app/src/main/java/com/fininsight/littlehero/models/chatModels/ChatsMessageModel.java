package com.fininsight.littlehero.models.chatModels;

import java.util.ArrayList;



public class ChatsMessageModel {
    private String _id;
    private String chat_id;
    private String user_id;
    private String message;
    private String image_file;
    private ArrayList<String> read;
    private ArrayList<String> hidden;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage_file() {
        return image_file;
    }

    public void setImage_file(String image_file) {
        this.image_file = image_file;
    }

    public ArrayList<String> getRead() {
        return read;
    }

    public void setRead(ArrayList<String> read) {
        this.read = read;
    }

    public ArrayList<String> getHidden() {
        return hidden;
    }

    public void setHidden(ArrayList<String> hidden) {
        this.hidden = hidden;
    }
}
