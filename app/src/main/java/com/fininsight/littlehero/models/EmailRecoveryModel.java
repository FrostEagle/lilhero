package com.fininsight.littlehero.models;

/**
 * Created by FrostEagle on 26.10.2016.
 */

public class EmailRecoveryModel {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
