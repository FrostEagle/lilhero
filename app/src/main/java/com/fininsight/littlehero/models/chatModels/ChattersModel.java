package com.fininsight.littlehero.models.chatModels;

/**
 * Created by FrostEagle on 25.10.2016.
 */

public class ChattersModel {
    private String name;
    private String avatar_file;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar_file() {
        return avatar_file;
    }

    public void setAvatar_file(String avatar_file) {
        this.avatar_file = avatar_file;
    }
}
