package com.fininsight.littlehero.models;


import com.fininsight.littlehero.models.babyModels.ChildModel;

import java.util.List;

public class UserForFollower {
    private String _id;
    private boolean expert;
    private String login;
    private String email;
    private String name;
    private String bio;
    private String  cover_file;
    private String avatar_file;
    private int done_counter;
    private int  time_counter;
    private int  posts_counter;
    private int  following_counter;
    private int  followers_counter;
    private List<ChildModel> childs;
  //  private List<String> photos;

    public List<ChildModel> getChilds() {
        return childs;
    }

    public void setChilds(List<ChildModel> childs) {
        this.childs = childs;
    }

//    public List<String> getPhotos() {
//        return photos;
//    }
//
//    public void setPhotos(List<String> photos) {
//        this.photos = photos;
//    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public boolean isExpert() {
        return expert;
    }

    public void setExpert(boolean expert) {
        this.expert = expert;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getCover_file() {
        return cover_file;
    }

    public void setCover_file(String cover_file) {
        this.cover_file = cover_file;
    }

    public String getAvatar_file() {
        return avatar_file;
    }

    public void setAvatar_file(String avatar_file) {
        this.avatar_file = avatar_file;
    }

    public int getDone_counter() {
        return done_counter;
    }

    public void setDone_counter(int done_counter) {
        this.done_counter = done_counter;
    }

    public int getTime_counter() {
        return time_counter;
    }

    public void setTime_counter(int time_counter) {
        this.time_counter = time_counter;
    }

    public int getPosts_counter() {
        return posts_counter;
    }

    public void setPosts_counter(int posts_counter) {
        this.posts_counter = posts_counter;
    }

    public int getFollowing_counter() {
        return following_counter;
    }

    public void setFollowing_counter(int following_counter) {
        this.following_counter = following_counter;
    }

    public int getFollowers_counter() {
        return followers_counter;
    }

    public void setFollowers_counter(int followers_counter) {
        this.followers_counter = followers_counter;
    }
}
