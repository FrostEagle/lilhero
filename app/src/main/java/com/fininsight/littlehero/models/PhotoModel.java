package com.fininsight.littlehero.models;

/**
 * Created by FrostEagle on 13.10.2016.
 */

public class PhotoModel {
    private String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
