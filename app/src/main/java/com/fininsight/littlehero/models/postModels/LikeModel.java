package com.fininsight.littlehero.models.postModels;

/**
 * Created by FrostEagle on 13.10.2016.
 */

public class LikeModel {
    private String _id;
    private String user_id;
    private String post_id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }
}
