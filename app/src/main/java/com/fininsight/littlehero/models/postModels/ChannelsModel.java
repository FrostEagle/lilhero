package com.fininsight.littlehero.models.postModels;

import java.util.List;

/**
 * Created by FrostEagle on 20.10.2016.
 */

public class ChannelsModel {
    private List<ChannelModel> channels;

    public List<ChannelModel> getChannels() {
        return channels;
    }

    public void setChannels(List<ChannelModel> channels) {
        this.channels = channels;
    }
}
