package com.fininsight.littlehero.models;

/**
 * Created by FrostEagle on 12.10.2016.
 */

public class ErrorModel {
    private String error;
    private String error_description;

    public ErrorModel() {
    }

    public String status() {
        return error;
    }

    public String message() {
        return error_description;
    }
}
