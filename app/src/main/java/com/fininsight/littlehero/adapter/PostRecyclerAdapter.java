package com.fininsight.littlehero.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.MainActivity;
import com.fininsight.littlehero.ProfileFragment;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.application.GlobalValue;
import com.fininsight.littlehero.busProviderCallback.BusProvider;
import com.fininsight.littlehero.busProviderCallback.DeletePostClass;
import com.fininsight.littlehero.likedislike.LikeDislikeClass;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.postModels.PostModel;
import com.fininsight.littlehero.otherClass.PhotoPostPhotosViewerActivity;
import com.fininsight.littlehero.post.ViewPostFragment;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class PostRecyclerAdapter extends RecyclerView.Adapter<PostRecyclerAdapter.PostViewHolder>{

        private List<PostModel> postList = Collections.emptyList();
        private Context context;
        private android.app.FragmentTransaction fragTransaction;
        private AccessToken accessToken;
        private LikeDislikeClass likeDislike;
        private CommentAdapter comAdapter;


    public PostRecyclerAdapter(List<PostModel> list, Context context){
            this.postList = list;
            this.context = context;
        }

        @Override
        public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
         //Inflate the layout, initialize the View Holder
            View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_post_feed,parent,false);
            return new PostViewHolder(v);
        }
        @Override
        public void onBindViewHolder(final PostViewHolder holder, final int position){
            likeDislike = new LikeDislikeClass(context);
            final PostModel item = postList.get(position);


            if (item.getUser_id().equals(GlobalValue.userModel.getUser().get_id())) {
                holder.imgMooreOptions.setVisibility(View.VISIBLE);
            } else holder.imgMooreOptions.setVisibility(View.INVISIBLE);
//            if (item.getSuperman()==1) {
//                holder.imgSuperman.setVisibility(View.VISIBLE);
//            } else holder.imgSuperman.setVisibility(View.INVISIBLE);
            if(item.getComments()!=null && item.getComments().size()>0) {
                String pluralComments = context.getResources().getQuantityString(R.plurals.plurals_comment, item.getComments().size(), item.getComments().size());
                holder.tvCountComment.setText(pluralComments);
                comAdapter = new CommentAdapter(item.getComments(), context);
                holder.rvComment.setVisibility(View.VISIBLE);
                holder.rvPostComment.setAdapter(comAdapter);
                holder.rvPostComment.setLayoutManager(new LinearLayoutManager(context));
            } else holder.rvComment.setVisibility(View.GONE);
            if (item.isMe_like())
                holder.imgLike.setImageDrawable(context.getResources().getDrawable(R.drawable.like));
            else holder.imgLike.setImageDrawable(context.getResources().getDrawable(R.drawable.like_not));
            if (item.getAvatar_file()!=null) {
                Picasso.with(context).load("http://little-hero.gmg-spb.ru/images/avatars/" + item.getUser_login()).into(holder.avatarAuthor);
            } else holder.avatarAuthor.setImageDrawable(context.getResources().getDrawable(R.drawable.no_photo_avatar));
            holder.tvTimePost.setText(item.getCreated_at().substring(0,11));
            holder.tvNickAuthor.setText(item.getUser_login());
            holder.tvContentPost.setText(postList.get(position).getMessage());
            if (item.getPhotos() > 1) {
                holder.imgSecondPhoto.setVisibility(View.VISIBLE);
                Picasso.with(context).load("http://little-hero.gmg-spb.ru/images/postphotos/"+item.get_id()+"/0").into(holder.imgFirstPhoto);
                Picasso.with(context).load("http://little-hero.gmg-spb.ru/images/postphotos/"+item.get_id()+"/1").into(holder.imgSecondPhoto);
                holder.linerPostPhoto.setVisibility(View.VISIBLE);
            } else if (item.getPhotos()==1) {
                Picasso.with(context).load("http://little-hero.gmg-spb.ru/images/postphotos/"+item.get_id()+"/0").into(holder.imgFirstPhoto);
                holder.imgSecondPhoto.setVisibility(View.GONE);
                holder.linerPostPhoto.setVisibility(View.VISIBLE);
            }  else holder.linerPostPhoto.setVisibility(View.GONE);
            holder.imgLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (item.isMe_like()) {
                        likeDislike.unLikePost(item.get_id());
                        item.setMe_like(false);
                        holder.imgLike.setImageDrawable(context.getResources().getDrawable(R.drawable.like_not));
                    } else {
                        likeDislike.likePost(item.get_id());
                        item.setMe_like(true);
                        holder.imgLike.setImageDrawable(context.getResources().getDrawable(R.drawable.like));

                    }
                }
            });
            holder.linerPostPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent toViewer = new Intent(context, PhotoPostPhotosViewerActivity.class);
                    toViewer.putExtra("id", item.get_id());
                    toViewer.putExtra("count", item.getPhotos());
                    context.startActivity(toViewer);
                }
            });
            holder.imgMooreOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String[] listitems = {  "Редактировать пост", "Удалить пост", "Отмена"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setItems(listitems, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    dialog.cancel();
                                    break;
                                case 1:
                                    BusProvider.getInstance().post(new DeletePostClass(item.get_id()));
                                    break;
                                case 2:
                                    dialog.cancel();
                                    break;
                            }
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
            holder.rlCommentsClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MainActivity mainActivity = (MainActivity) context;
                    Bundle args = new Bundle();
                    args.putSerializable("item", item);
                    ViewPostFragment viewPostFrag = new ViewPostFragment();
                    viewPostFrag.setArguments(args);
                    FragmentTransaction fragTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
                    fragTransaction.add(R.id.frame_for_fragment, viewPostFrag);
                    fragTransaction.addToBackStack(null);
                    fragTransaction.commit();
                }
            });
            holder.rlCommentLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MainActivity mainActivity = (MainActivity) context;
                    Bundle args = new Bundle();
                    args.putSerializable("item", item);
                    ViewPostFragment viewPostFrag = new ViewPostFragment();
                    viewPostFrag.setArguments(args);
                    FragmentTransaction fragTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
                    fragTransaction.add(R.id.frame_for_fragment, viewPostFrag);
                    fragTransaction.addToBackStack(null);
                    fragTransaction.commit();
         //           context.startActivity(new Intent(context, ViewPostActivity.class));
//                    ViewPostFragment viewPostFrag = new ViewPostFragment();
//                    fragTransaction = ((Activity) context).getFragmentManager().beginTransaction();
//                    fragTransaction.replace(R.id.frame_for_fragment, viewPostFrag);
//                    fragTransaction.addToBackStack(null);
//                    fragTransaction.commit();
                }
            });
            holder.tvNickAuthor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MainActivity mainActivity = (MainActivity) context;
                    ProfileFragment profileFragment = new ProfileFragment();
                    FragmentTransaction fragTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
                    fragTransaction.add(R.id.frame_for_fragment, profileFragment);
                    Bundle args = new Bundle();
                    args.putString("id", item.getUser_id());
                    profileFragment.setArguments(args);
                    fragTransaction.addToBackStack(null);
                    fragTransaction.commit();
                }
            });

        }

        @Override
        public int getItemCount(){
        return postList.size();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
        }


    class PostViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout rlCardPost, rvComment, rlPostTask, rlCommentLike, rlCommentsClick;
        private TextView tvNickAuthor, tvTimePost, tvContentPost, tvCommentPost, tvContentTask, tvCountComment;
        private ImageView imgSuperman, imgMooreOptions, imgPhotoFirst, imgPhotoSecond, imgLike, imgFirstPhoto, imgSecondPhoto, imgAvatarTaskPost;
        private CircleImageView avatarAuthor;
        private RecyclerView rvPostComment;
        private LinearLayout linerPostPhoto;

        PostViewHolder(View itemView) {
            super(itemView);
            rlCommentLike = (RelativeLayout) itemView.findViewById(R.id.rl_comment_like);
            rvComment = (RelativeLayout) itemView.findViewById(R.id.rl_comment);
            rlCommentsClick = (RelativeLayout) itemView.findViewById(R.id.rl_commnets_click);
            rvPostComment = (RecyclerView) itemView.findViewById(R.id.rv_post_comment);
            tvNickAuthor = (TextView) itemView.findViewById(R.id.tv_name_author_post);
            avatarAuthor = (CircleImageView) itemView.findViewById(R.id.img_my_post_profile);
            rlCardPost = (RelativeLayout) itemView.findViewById(R.id.rl_cardView);
            tvCommentPost = (TextView) itemView.findViewById(R.id.tv_comment);
            tvCountComment = (TextView) itemView.findViewById(R.id.tv_count_comment);
            tvContentPost = (TextView) itemView.findViewById(R.id.tv_content_post);
            tvTimePost = (TextView) itemView.findViewById(R.id.tv_time_post);
            linerPostPhoto = (LinearLayout) itemView.findViewById(R.id.liner_photo_post);
            imgFirstPhoto = (ImageView) itemView.findViewById(R.id.img_first_photo_post);
            imgSecondPhoto = (ImageView) itemView.findViewById(R.id.img_second_photo_post);
            imgSuperman = (ImageView) itemView.findViewById(R.id.img_post_superman);
            imgMooreOptions = (ImageView) itemView.findViewById(R.id.img_moore_menu);
            imgLike = (ImageView) itemView.findViewById(R.id.img_like);
            rlPostTask = (RelativeLayout) itemView.findViewById(R.id.rl_post_task);
            tvContentTask = (TextView) itemView.findViewById(R.id.tv_content_task);
            imgAvatarTaskPost = (ImageView) itemView.findViewById(R.id.img_task_avatar);
        }
    }

}
