package com.fininsight.littlehero.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.models.postModels.CommentModel;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;


public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {

    List<CommentModel> commentList = Collections.emptyList();
    Context context;

    public CommentAdapter(List<CommentModel> list, Context context) {
        this.commentList = list;
        this.context = context;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_row, parent, false);
        return new CommentViewHolder(v);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(CommentViewHolder holder, final int position) {
        CommentModel commentItem = commentList.get(position);
        holder.nameAuthor.setText("@"+commentItem.getUser_login());
        Picasso.with(context).load("http://little-hero.gmg-spb.ru/images/avatars/"+commentItem.getUser_login()).into(holder.imgAvatar);
      //  holder.imgAvatar.setImageDrawable(context.getResources().getDrawable(commentList.get(position).getAvatar()));
        holder.time.setText(commentItem.getCreated_at().substring(0,11));
        holder.content.setText(commentItem.getMessage());
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent myIntent = new Intent(context, FriendProfileActivity.class);
//                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                myIntent.putExtra("id", followerList.get(position).getId());
//                context.startActivity(myIntent);
            }
        };
        holder.rlCard.setOnClickListener(clickListener);
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

//    // Insert a new item to the RecyclerView on a predefined position
//    public void insert(int position, User user) {
//        followerList.add(position, user);
//        notifyItemInserted(position);
//    }
//
//    // Remove a RecyclerView item containing a specified Data object
//    public void remove(User user) {
//        int position = followerList.indexOf(user);
//        followerList.remove(position);
//        notifyItemRemoved(position);
//    }

    public class CommentViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rlCard;
        private ImageView imgAvatar;
        private TextView time;
        private TextView content;
        private TextView nameAuthor;

        CommentViewHolder(View itemView) {
            super(itemView);
            rlCard = (RelativeLayout) itemView.findViewById(R.id.rl_card);
            imgAvatar = (ImageView) itemView.findViewById(R.id.img_profile_follower);
            time = (TextView) itemView.findViewById(R.id.tv_text_time_comment);
            nameAuthor = (TextView) itemView.findViewById(R.id.tv_follower_name);
            content = (TextView) itemView.findViewById(R.id.tv_content_comment);
        }
    }
}
