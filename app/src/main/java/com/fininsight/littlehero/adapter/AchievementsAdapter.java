package com.fininsight.littlehero.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.classForData.Achievements;

import java.util.Collections;
import java.util.List;


public class AchievementsAdapter extends RecyclerView.Adapter<AchievementsAdapter.AchievementsViewHolder> {

    List<Achievements> achievementsList = Collections.emptyList();
    Context context;

    public AchievementsAdapter(List<Achievements> list, Context context) {
        this.achievementsList = list;
        this.context = context;
    }

    @Override
    public AchievementsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_achievements, parent, false);
        return new AchievementsViewHolder(v);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(AchievementsViewHolder holder, final int position) {


        if (achievementsList.get(position).getAchievementsState()==1) {
            holder.imgAchievements.setBackground(context.getResources().getDrawable(R.drawable.achievements_pressed));
        } else {
            holder.imgAchievements.setBackground(context.getResources().getDrawable(R.drawable.achievements_default));
        }
        holder.titleAchievements.setText(achievementsList.get(position).getTitleAchievements());
//        if (followerList.get(position).getAvatarThumb() != null) {
//            Picasso.with(context).load(followerList.get(position).getAvatarThumb()).into(holder.avatar);
//        } else holder.avatar.setImageResource(R.drawable.photo_add);

//        if (followerList.get(position).getFriendsWho() == 1) {
//            holder.imgFollower.setBackground(context.getResources().getDrawable(R.drawable.follower_pressed));
//        } else
//            holder.imgFollower.setBackground(context.getResources().getDrawable(R.drawable.follower_default));
//        if (followerList.get(position).getSupermen() == 1) {
//            holder.imgSupermen.setVisibility(View.VISIBLE);
//        } else
//            holder.imgSupermen.setVisibility(View.INVISIBLE);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent myIntent = new Intent(context, FriendProfileActivity.class);
//                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                myIntent.putExtra("id", followerList.get(position).getId());
//                context.startActivity(myIntent);
            }
        };
        holder.rlCard.setOnClickListener(clickListener);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return achievementsList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

//    // Insert a new item to the RecyclerView on a predefined position
//    public void insert(int position, User user) {
//        followerList.add(position, user);
//        notifyItemInserted(position);
//    }
//
//    // Remove a RecyclerView item containing a specified Data object
//    public void remove(User user) {
//        int position = followerList.indexOf(user);
//        followerList.remove(position);
//        notifyItemRemoved(position);
//    }

    public class AchievementsViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rlCard;
        private TextView titleAchievements;
        private ImageView imgAchievements;



        AchievementsViewHolder(View itemView) {
            super(itemView);
            rlCard = (RelativeLayout) itemView.findViewById(R.id.rl_card);
            titleAchievements = (TextView) itemView.findViewById(R.id.tv_achievements_title);
            imgAchievements = (ImageView) itemView.findViewById(R.id.img_achievements);
        }
    }
}
