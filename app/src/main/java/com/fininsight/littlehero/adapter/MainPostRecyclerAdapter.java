package com.fininsight.littlehero.adapter;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.MainActivity;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.classForData.Post;
import com.fininsight.littlehero.post.ViewPostFragment;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainPostRecyclerAdapter extends RecyclerView.Adapter<MainPostRecyclerAdapter.PostViewHolder>{

        private List<Post> postList = Collections.emptyList();
        private Context context;
        private int like;
        private android.app.FragmentTransaction fragTransaction;

    public MainPostRecyclerAdapter(List<Post> list, Context context){
            this.postList = list;
            this.context = context;
        }

        @Override
        public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
         //Inflate the layout, initialize the View Holder
            View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_post_feed,parent,false);
            return new PostViewHolder(v);
        }
        @Override
        public void onBindViewHolder(final PostViewHolder holder, final int position){
            final Post item = postList.get(position);
//            if (item.getUser_id().equals(GlobalValue.userModel.getUser().get_id())) {
//                holder.imgMooreOptions.setVisibility(View.VISIBLE);
//            } else holder.imgMooreOptions.setVisibility(View.INVISIBLE);
            if (item.getSuperman()==1) {
                holder.imgSuperman.setVisibility(View.VISIBLE);
            } else holder.imgSuperman.setVisibility(View.INVISIBLE);
            holder.avatarAuthor.setImageDrawable(context.getResources().getDrawable(postList.get(position).getAvatarAuthorPost()));
            holder.tvTimePost.setText(postList.get(position).getTimePost());
            holder.tvNickAuthor.setText(postList.get(position).getAuthorName());
            holder.tvContentPost.setText(postList.get(position).getContentPost());
            if (item.getCountPhoto() > 1) {
                holder.imgFirstPhoto.setImageDrawable(context.getResources().getDrawable(postList.get(position).getFirstPhotoPost()));
                holder.imgSecondPhoto.setImageDrawable(context.getResources().getDrawable(postList.get(position).getSecondPhotoPost()));
                holder.linerPostPhoto.setVisibility(View.VISIBLE);
            } else if (item.getCountPhoto()==1) {
                Picasso.with(context).load("http://little-hero.gmg-spb.ru/images/postphotos/"+item.getIdPost()+"/0").into(holder.imgFirstPhoto);
                holder.linerPostPhoto.setVisibility(View.VISIBLE);
//                holder.imgFirstPhoto.setImageDrawable(context.getResources().getDrawable(postList.get(position).getFirstPhotoPost()));
//                holder.linerPostPhoto.setVisibility(View.VISIBLE);
            }  else holder.linerPostPhoto.setVisibility(View.GONE);
            holder.imgLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (like == 1) {
                        holder.imgLike.setImageDrawable(context.getResources().getDrawable(R.drawable.like));
                        like = 0;
                    } else {
                        holder.imgLike.setImageDrawable(context.getResources().getDrawable(R.drawable.like_not));
                        like = 1;
                    }
                }
            });
            holder.rlCommentLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MainActivity mainActivity = (MainActivity) context;
                    ViewPostFragment viewPostFrag = new ViewPostFragment();
                    FragmentTransaction fragTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
                    fragTransaction.replace(R.id.frame_for_fragment, viewPostFrag);
                    fragTransaction.addToBackStack(null);
                    fragTransaction.commit();
         //           context.startActivity(new Intent(context, ViewPostActivity.class));
//                    ViewPostFragment viewPostFrag = new ViewPostFragment();
//                    fragTransaction = ((Activity) context).getFragmentManager().beginTransaction();
//                    fragTransaction.replace(R.id.frame_for_fragment, viewPostFrag);
//                    fragTransaction.addToBackStack(null);
//                    fragTransaction.commit();
                }
            });

        }

        @Override
        public int getItemCount(){
        return postList.size();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
        }


    public class PostViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rlCardPost, rvComment, rlPostTask, rlCommentLike;
        private TextView tvNickAuthor, tvTimePost, tvContentPost, tvCommentPost, tvContentTask;
        private ImageView imgSuperman, imgMooreOptions, imgPhotoFirst, imgPhotoSecond, imgLike, imgFirstPhoto, imgSecondPhoto, imgAvatarTaskPost;
        private CircleImageView avatarAuthor;
        private RecyclerView rvPostComment;
        private LinearLayout linerPostPhoto;


        PostViewHolder(View itemView) {
            super(itemView);
            rlCommentLike = (RelativeLayout) itemView.findViewById(R.id.rl_comment_like);
            rvComment = (RelativeLayout) itemView.findViewById(R.id.rl_comment);
            rvPostComment = (RecyclerView) itemView.findViewById(R.id.rv_post_comment);
            tvNickAuthor = (TextView) itemView.findViewById(R.id.tv_name_author_post);
            avatarAuthor = (CircleImageView) itemView.findViewById(R.id.img_my_post_profile);
            rlCardPost = (RelativeLayout) itemView.findViewById(R.id.rl_cardView);
            tvCommentPost = (TextView) itemView.findViewById(R.id.tv_comment);
            tvContentPost = (TextView) itemView.findViewById(R.id.tv_content_post);
            tvTimePost = (TextView) itemView.findViewById(R.id.tv_time_post);
            linerPostPhoto = (LinearLayout) itemView.findViewById(R.id.liner_photo_post);
            imgFirstPhoto = (ImageView) itemView.findViewById(R.id.img_first_photo_post);
            imgSecondPhoto = (ImageView) itemView.findViewById(R.id.img_second_photo_post);
            imgSuperman = (ImageView) itemView.findViewById(R.id.img_post_superman);
            imgMooreOptions = (ImageView) itemView.findViewById(R.id.img_moore_menu);
            imgLike = (ImageView) itemView.findViewById(R.id.img_like);
            rlPostTask = (RelativeLayout) itemView.findViewById(R.id.rl_post_task);
            tvContentTask = (TextView) itemView.findViewById(R.id.tv_content_task);
            imgAvatarTaskPost = (ImageView) itemView.findViewById(R.id.img_task_avatar);
        }
    }
}
