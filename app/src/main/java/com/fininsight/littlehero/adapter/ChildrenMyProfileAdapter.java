package com.fininsight.littlehero.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.models.babyModels.ChildModel;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class ChildrenMyProfileAdapter extends RecyclerView.Adapter<ChildrenMyProfileAdapter.ChildrenViewHolder> {

    List<ChildModel> childList = Collections.emptyList();
    Context context;
    private String yearBaby;

    public ChildrenMyProfileAdapter(List<ChildModel> list, Context context) {
        this.childList = list;
        this.context = context;
    }

    @Override
    public ChildrenViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_children, parent, false);
        return new ChildrenViewHolder(v);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(ChildrenViewHolder holder, final int position) {
        final ChildModel item = childList.get(position);
        holder.nameChildren.setText(item.getName());
        int week = item.getDay()/7;
        if (item.isPregnancy()) {
            holder.imgSex.setVisibility(View.GONE);
            holder.nameChildren.setText("БЕРЕМЕННА");
            holder.titleDate.setText(String.valueOf(week)+" неделя");
            holder.imgBaby.setImageDrawable(context.getResources().getDrawable(R.drawable.baby_no_photo));
        } else {
            holder.imgSex.setVisibility(View.VISIBLE);
            if (item.getGender()==1) {
                int year = week/52;
                if (year<1) {
                    yearBaby = "до года";
                } else {
                    yearBaby = context.getResources().getQuantityString(R.plurals.plurals_year, year, year);
                }
                holder.titleDate.setText("Мальчик, "+yearBaby);
                holder.imgSex.setImageDrawable(context.getResources().getDrawable(R.drawable.boy_profile_sex));
            }
             else holder.imgSex.setImageDrawable(context.getResources().getDrawable(R.drawable.girl_profile_sex));
            if (item.getPhoto_file()!=null) {
                Picasso.with(context).load("http://little-hero.gmg-spb.ru/images/childs/"+item.get_id()).into(holder.imgBaby);
            } else  holder.imgBaby.setImageDrawable(context.getResources().getDrawable(R.drawable.no_photo_avatar));
        }


       // holder.titleWhatNeed.setText(whatList.get(position).getContent());

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent myIntent = new Intent(context, FriendProfileActivity.class);
//                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                myIntent.putExtra("id", followerList.get(position).getId());
//                context.startActivity(myIntent);
            }
        };
        holder.rlCard.setOnClickListener(clickListener);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return childList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

//    // Insert a new item to the RecyclerView on a predefined position
//    public void insert(int position, User user) {
//        followerList.add(position, user);
//        notifyItemInserted(position);
//    }
//
//    // Remove a RecyclerView item containing a specified Data object
//    public void remove(User user) {
//        int position = followerList.indexOf(user);
//        followerList.remove(position);
//        notifyItemRemoved(position);
//    }

    public class ChildrenViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rlCard;
        private TextView nameChildren;
        private TextView titleDate;
        private CircleImageView imgBaby;
        private CircleImageView imgSex;



        ChildrenViewHolder(View itemView) {
            super(itemView);
            rlCard = (RelativeLayout) itemView.findViewById(R.id.rl_card);
            nameChildren = (TextView) itemView.findViewById(R.id.tv_name);
            titleDate = (TextView) itemView.findViewById(R.id.tv_date_birth);
            imgBaby = (CircleImageView) itemView.findViewById(R.id.imgv_my_profile_baby_photo);
            imgSex = (CircleImageView) itemView.findViewById(R.id.img_sex);

        }
    }
}
