package com.fininsight.littlehero.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.MainActivity;
import com.fininsight.littlehero.ProfileFragment;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.followUnfollow.FollowUnfollowClass;
import com.fininsight.littlehero.models.FollowerModel;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class FollowerAdapter extends RecyclerView.Adapter<FollowerAdapter.FollowerViewHolder> {

    private List<FollowerModel> followerList = Collections.emptyList();
    Context context;
    private FollowUnfollowClass followUnfollow;

    public FollowerAdapter(List<FollowerModel> list, Context context) {
        this.followerList = list;
        this.context = context;
        followUnfollow = new FollowUnfollowClass(context);
    }

    @Override
    public FollowerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_follower, parent, false);
        return new FollowerViewHolder(v);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final FollowerViewHolder holder, final int position) {
        final FollowerModel item = followerList.get(position);
        holder.name.setText(item.getFollowing().getName());
        if (item.getFollowing().getAvatar_file()!=null) {
            Picasso.with(context).load("http://little-hero.gmg-spb.ru/images/avatars/"+item.getFollowing().getLogin()).into(holder.avatar);
        } else  Picasso.with(context).load(R.drawable.photo_add).into(holder.avatar);
//
////        if (followerList.get(position).getAvatarThumb() != null) {
////            Picasso.with(context).load(followerList.get(position).getAvatarThumb()).into(holder.avatar);
////        } else holder.avatar.setImageResource(R.drawable.photo_add);
//
        if (item.getRelation().equals("mutual")) {
            holder.imgFollower.setBackground(context.getResources().getDrawable(R.drawable.follower_pressed));
        } else
            holder.imgFollower.setBackground(context.getResources().getDrawable(R.drawable.follower_default));
        if (item.getFollowing().isExpert()) {
            holder.imgSupermen.setVisibility(View.VISIBLE);
        } else
            holder.imgSupermen.setVisibility(View.INVISIBLE);
        holder.imgFollower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.getRelation().equals("mutual")) {
                    followUnfollow.unfollowUser(item.getFollow_id());
                    followerList.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, followerList.size());
                    holder.imgFollower.setBackground(context.getResources().getDrawable(R.drawable.follower_default));
                }
            }
        });
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity) context;
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle args = new Bundle();
                args.putString("id", followerList.get(position).getFollow_id());
                profileFragment.setArguments(args);
                FragmentTransaction fragTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, profileFragment);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        };
        holder.rlCard.setOnClickListener(clickListener);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return followerList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

//    // Insert a new item to the RecyclerView on a predefined position
//    public void insert(int position, User user) {
//        followerList.add(position, user);
//        notifyItemInserted(position);
//    }
//
//    // Remove a RecyclerView item containing a specified Data object
//    public void remove(User user) {
//        int position = followerList.indexOf(user);
//        followerList.remove(position);
//        notifyItemRemoved(position);
//    }

    public class FollowerViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rlCard;
        private TextView name;
        private CircleImageView avatar;
        private ImageView imgFollower;
        private ImageView imgSupermen;


        FollowerViewHolder(View itemView) {
            super(itemView);
            rlCard = (RelativeLayout) itemView.findViewById(R.id.rl_card);
            imgSupermen = (ImageView) itemView.findViewById(R.id.img_follower_superman);
            name = (TextView) itemView.findViewById(R.id.tv_follower_name);
            avatar = (CircleImageView) itemView.findViewById(R.id.img_profile_follower);
            imgFollower = (ImageView) itemView.findViewById(R.id.imm_follower);
        }
    }
}
