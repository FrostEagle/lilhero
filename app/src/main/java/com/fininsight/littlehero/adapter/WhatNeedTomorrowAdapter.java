package com.fininsight.littlehero.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.classForData.WhatNeedTomorrow;

import java.util.Collections;
import java.util.List;


public class WhatNeedTomorrowAdapter extends RecyclerView.Adapter<WhatNeedTomorrowAdapter.WhatNeedTomorrowViewHolder> {

    List<WhatNeedTomorrow> whatList = Collections.emptyList();
    Context context;
    private WhatNeedAdapter adapterNeed;

    public WhatNeedTomorrowAdapter(List<WhatNeedTomorrow> list, Context context) {
        this.whatList = list;
        this.context = context;
    }

    @Override
    public WhatNeedTomorrowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout_what_need_tomorrow, parent, false);
        return new WhatNeedTomorrowViewHolder(v);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(WhatNeedTomorrowViewHolder holder, final int position) {
        holder.titleWhatNeed.setText(whatList.get(position).getContent());
        adapterNeed = new WhatNeedAdapter(whatList.get(position).getWhatNeedList() , context);
        holder.rvWhatNeed.setAdapter(adapterNeed);
        holder.rvWhatNeed.setLayoutManager(new LinearLayoutManager(context));
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent myIntent = new Intent(context, FriendProfileActivity.class);
//                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                myIntent.putExtra("id", followerList.get(position).getId());
//                context.startActivity(myIntent);
            }
        };
        holder.rlCard.setOnClickListener(clickListener);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return whatList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

//    // Insert a new item to the RecyclerView on a predefined position
//    public void insert(int position, User user) {
//        followerList.add(position, user);
//        notifyItemInserted(position);
//    }
//
//    // Remove a RecyclerView item containing a specified Data object
//    public void remove(User user) {
//        int position = followerList.indexOf(user);
//        followerList.remove(position);
//        notifyItemRemoved(position);
//    }

    public class WhatNeedTomorrowViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout rlCard;
        private TextView titleWhatNeed;
        private RecyclerView rvWhatNeed;



        WhatNeedTomorrowViewHolder(View itemView) {
            super(itemView);
            rlCard = (RelativeLayout) itemView.findViewById(R.id.rl_card);
            titleWhatNeed = (TextView) itemView.findViewById(R.id.tv_what_need);
            rvWhatNeed = (RecyclerView) itemView.findViewById(R.id.rv_what_need_tomorrow);

        }
    }
}
