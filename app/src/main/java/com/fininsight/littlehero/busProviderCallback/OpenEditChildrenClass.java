package com.fininsight.littlehero.busProviderCallback;



public class OpenEditChildrenClass {
    private int position;
    public OpenEditChildrenClass(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
