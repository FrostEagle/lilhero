package com.fininsight.littlehero.busProviderCallback;



public class DeletePostClass {
    private String  id;

    public DeletePostClass(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
