package com.fininsight.littlehero.fragmentFolower;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.FollowersAdapter;
import com.fininsight.littlehero.classForData.Follower;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.FollowersModel;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetFollowersClient;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowersFragment extends Fragment implements View.OnClickListener {
    private RecyclerView rvFollowers;
    private RelativeLayout rlBack;
    private List<FollowersModel> followerList;
    private FollowersAdapter adapter;
    private AccessToken accessToken;
    private Bundle args;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_followers, container, false);
        rlBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        rvFollowers = (RecyclerView) view.findViewById(R.id.rv_followers);
        followerList = new ArrayList<>();
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());
        GetFollowersClient getUserClient = ServiceGenerator.createService(GetFollowersClient.class, accessToken, getContext());
        args = getArguments();
        Call<ArrayList<FollowersModel>> call = getUserClient.getUserModels(args.getString("id"), 1);
        call.enqueue(new Callback<ArrayList<FollowersModel>>() {
            @Override
            public void onResponse(Call<ArrayList<FollowersModel>> call, Response<ArrayList<FollowersModel>> response) {
                ResponseBody errorBo = response.errorBody();
                if (response.isSuccessful()) {
                    followerList = response.body();
                    adapter = new FollowersAdapter(followerList, getContext());
                    rvFollowers.setAdapter(adapter);
                    rvFollowers.setLayoutManager(new LinearLayoutManager(getContext()));
                    rvFollowers.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).build());

                }
            }
            @Override
            public void onFailure(Call<ArrayList<FollowersModel>> call, Throwable t) {
            }
        });
        rlBack.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
    private List<Follower> followerListCreate() {
        List<Follower> data = new ArrayList<>();

        data.add(new Follower(1,"Jane",1,0,R.drawable.main_avatar));
        data.add(new Follower(2,"Sara",0,1,R.drawable.image));
        data.add(new Follower(3,"Elizabeth Taylor",0,0,R.drawable.image2));
        data.add(new Follower(4,"Ariel",1,1,R.drawable.image3));

        return data;
    }
}
