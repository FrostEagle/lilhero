package com.fininsight.littlehero.fragmentFolower;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.FollowerAdapter;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.FollowerModel;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetFollowingClient;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FollowerFragment extends Fragment implements View.OnClickListener{
    private RecyclerView rvFollowers;
    private RelativeLayout rlBack;
    private List<FollowerModel> followerList;
    private FollowerAdapter adapter;
    private AccessToken accessToken;
    private Bundle args;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_follower, container, false);
        rlBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        rvFollowers = (RecyclerView) view.findViewById(R.id.rv_followers);
        followerList = new ArrayList<>();
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());
        args = getArguments();
        GetFollowingClient getUserClient = ServiceGenerator.createService(GetFollowingClient.class, accessToken, getContext());
        Call<ArrayList<FollowerModel>> call = getUserClient.getUserModels(args.getString("id"), 1);
        call.enqueue(new Callback<ArrayList<FollowerModel>>() {
            @Override
            public void onResponse(Call<ArrayList<FollowerModel>> call, Response<ArrayList<FollowerModel>> response) {
                ResponseBody errorBo = response.errorBody();
                if (response.isSuccessful()) {
                    followerList = response.body();
                    adapter = new FollowerAdapter(followerList, getContext());
                    rvFollowers.setAdapter(adapter);
                    rvFollowers.setLayoutManager(new LinearLayoutManager(getContext()));
                    rvFollowers.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).build());

                }
            }
            @Override
            public void onFailure(Call<ArrayList<FollowerModel>> call, Throwable t) {
            }
        });

        rlBack.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }

}
