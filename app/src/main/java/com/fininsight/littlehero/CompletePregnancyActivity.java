package com.fininsight.littlehero;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.fininsight.littlehero.entering.StatusEnteringActivity;

public class CompletePregnancyActivity extends AppCompatActivity implements View.OnClickListener{

    private Animation animationClickYes, animationClickNo;
    private ImageView imgYesComplete, imgNoComplete;
    private RelativeLayout rlClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_pregnancy);
        rlClose = (RelativeLayout) findViewById(R.id.rl_close_popup);
        imgYesComplete = (ImageView) findViewById(R.id.img_complete_pregnancy);
        imgNoComplete = (ImageView) findViewById(R.id.img_complete_no_pregnancy);
        animationClickYes = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button_pregnant);
        animationClickNo = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button_pregnant);
        rlClose.setOnClickListener(this);
        imgNoComplete.setOnClickListener(this);
        imgYesComplete.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_close_popup:
                finish();
                break;
            case R.id.img_complete_no_pregnancy:
                imgNoComplete.startAnimation(animationClickNo);
                finish();
                break;
            case R.id.img_complete_pregnancy:
                imgYesComplete.startAnimation(animationClickYes);
                startActivity(new Intent(CompletePregnancyActivity.this, StatusEnteringActivity.class));
                finish();
                break;
        }
    }
}
