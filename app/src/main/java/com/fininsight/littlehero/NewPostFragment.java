package com.fininsight.littlehero;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


public class NewPostFragment extends Fragment implements View.OnClickListener{
    private ImageView imgBack;
    private ImageView addPhotoPost;
    private LinearLayout topLinearLayout;
    private int whatPhotoCount = 0;
    private RelativeLayout rlBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_new_post, container, false);
        rlBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        addPhotoPost = (ImageView) view.findViewById(R.id.add_photo_post);
        addPhotoPost.setOnClickListener(this);
        rlBack.setOnClickListener(this);
        HorizontalScrollView scrollView = (HorizontalScrollView) view.findViewById(R.id.sc_photo_post);

        topLinearLayout = new LinearLayout(getContext());
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
//        for (int i = 0; i < 5; i++){
//            final RelativeLayout relativeLayout = new RelativeLayout(getContext());
//            final ImageView imageView = new ImageView (getContext());
//            final ImageView imageViewDelete = new ImageView(getContext());
//            imageView.setTag(i);
//            imageViewDelete.setTag(i);
////            LinearLayout.LayoutParams layoutParams  = new LinearLayout.LayoutParams(150, 150);
////            layoutParams.setMargins(5,10,5,0);
//         //   relativeLayout.setLayoutParams(layoutParams);
//           // imageView.setLayoutParams(layoutParams);
//            imageView.setImageResource(R.drawable.photo_baby1);
//            imageViewDelete.setImageResource(R.drawable.delete_image_post);
//            DisplayMetrics metrics = this.getResources().getDisplayMetrics();
//            int screenWidth = metrics.widthPixels;
//            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getPx(85), getPx(85));
//            lp.setMargins(getPx(10),getPx(10),getPx(10),0);
//            RelativeLayout.LayoutParams lpDelete = new RelativeLayout.LayoutParams(getPx(25), getPx(25));
//            lpDelete.setMargins(getPx(77),getPx(3),0,0);
//            relativeLayout.addView(imageView,lp);
//            relativeLayout.addView(imageViewDelete,lpDelete);
//            topLinearLayout.addView(relativeLayout);
//            imageViewDelete     .setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    topLinearLayout.removeView((View) v.getParent());
//                    Log.e("Tag",""+imageView.getTag());
//                }
//            });
//        }
        scrollView.addView(topLinearLayout);



        return view;
    }
    //set dp calculate automatically
    public int getPx(int dimensionDp) {
        float density = getResources().getDisplayMetrics().density;
        return (int) (dimensionDp * density + 0.5f);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.add_photo_post:
                final RelativeLayout relativeLayout = new RelativeLayout(getContext());
                final ImageView imageView = new ImageView (getContext());
                final ImageView imageViewDelete = new ImageView(getContext());
                imageView.setTag(whatPhotoCount);
                imageViewDelete.setTag(whatPhotoCount);
//            LinearLayout.LayoutParams layoutParams  = new LinearLayout.LayoutParams(150, 150);
//            layoutParams.setMargins(5,10,5,0);
                //   relativeLayout.setLayoutParams(layoutParams);
                // imageView.setLayoutParams(layoutParams);
                imageView.setImageResource(R.drawable.photo_baby1);
                imageViewDelete.setImageResource(R.drawable.delete_image_post);
                DisplayMetrics metrics = this.getResources().getDisplayMetrics();
                int screenWidth = metrics.widthPixels;
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getPx(85), getPx(85));
                lp.setMargins(getPx(10),getPx(10),getPx(10),0);
                RelativeLayout.LayoutParams lpDelete = new RelativeLayout.LayoutParams(getPx(25), getPx(25));
                lpDelete.setMargins(getPx(77),getPx(3),0,0);
                relativeLayout.addView(imageView,lp);
                relativeLayout.addView(imageViewDelete,lpDelete);
                topLinearLayout.addView(relativeLayout);
                imageViewDelete     .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        topLinearLayout.removeView((View) v.getParent());
                        Log.e("Tag",""+imageView.getTag());
                    }
                });
                whatPhotoCount ++;

                break;
        }
    }
}
