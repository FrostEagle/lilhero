package com.fininsight.littlehero.expandableMemo;

import java.util.ArrayList;

/**
 * Created by FrostEagle on 24.08.2016.
 */
public class Parent {
    private String title;
    private int image;
    private int textColor;
    private ArrayList<Child> Items;

    public ArrayList<Child> getItems() {
        return Items;
    }

    public void setItems(ArrayList<Child> items) {
        Items = items;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }
}
