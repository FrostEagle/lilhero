package com.fininsight.littlehero.expandableMemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;

import com.fininsight.littlehero.R;

import java.util.ArrayList;


public class MemoryChildrenFragment extends Fragment implements View.OnClickListener{
    private ExpandListAdapter expAdapter;
    private ArrayList<Parent> expListItems;
    private ExpandableListView expandList;

    private RelativeLayout rlBack;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_memory_children, container, false);
        rlBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        expandList = (ExpandableListView) view.findViewById(R.id.expandable_listview);
        expListItems = SetStandardGroups();
        expAdapter = new ExpandListAdapter(getContext(), expListItems);
        expandList.setAdapter(expAdapter);

        rlBack.setOnClickListener(this);
        return view;
    }

    public ArrayList<Parent> SetStandardGroups() {

        ArrayList<Parent> list = new ArrayList<>();

        ArrayList<Child> child_list;
        Child child = new Child();
        child.setContent("wwwwwwww");
        child_list = new ArrayList<>();
        child_list.add(child);

        Parent parent = new Parent();
        parent.setTextColor(R.color.colorYellowTab);
        parent.setImage(R.drawable.memo_smile);
        parent.setTitle("Речь и эмоции");
        parent.setItems(child_list);

        Parent parent1 = new Parent();
        parent1.setTextColor(R.color.numberProfile);
        parent1.setImage(R.drawable.memo_brain);
        parent1.setTitle("Умственное развитие");
        parent1.setItems(child_list);

        Parent parent2 = new Parent();
        parent2.setTextColor(R.color.exit_app);
        parent2.setImage(R.drawable.memo_muscle);
        parent2.setTitle("Физическое развитие");
        parent2.setItems(child_list);

        Parent parent3 = new Parent();
        parent3.setTextColor(R.color.colorYellowTab);
        parent3.setImage(R.drawable.memo_smile);
        parent3.setTitle("Речь");
        parent3.setItems(child_list);

        list.add(parent);
        list.add(parent1);
        list.add(parent2);
        list.add(parent3);
        return list;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }

}
