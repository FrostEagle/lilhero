package com.fininsight.littlehero.expandableMemo;


public class Child {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
