package com.fininsight.littlehero.expandableMemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fininsight.littlehero.R;

import java.util.ArrayList;


public class ExpandListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Parent> parents;

    public ExpandListAdapter(Context context, ArrayList<Parent> parents) {
        this.context = context;
        this.parents = parents;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<Child> chList = parents.get(groupPosition).getItems();
        return chList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        Child child = (Child) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_row, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.tv_child_content);

        tv.setText(child.getContent().toString());


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Child> chList = parents.get(groupPosition).getItems();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return parents.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return parents.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup group) {
        Parent parent = (Parent) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.parent_row, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.tv_content_task);
        ImageView img = (ImageView) convertView.findViewById(R.id.img_parent_avatar);
        img.setImageDrawable(context.getResources().getDrawable(parent.getImage()));
        tv.setText(parent.getTitle());
        tv.setTextColor(context.getResources().getColor(parent.getTextColor()));
//        tv.setTextColor(parent.getTextColor());
//        if (parent.getTitle().equals("Умственное развитие")) {
//            tv.setTextColor(context.getResources().getColor(parent.getTextColor()));
//        }

        ImageView rightcheck = (ImageView) convertView.findViewById(R.id.img_arrow);
        if (!isExpanded) {
            rightcheck.setImageDrawable(context.getResources().getDrawable(R.drawable.arrow_down));
        } else {
            rightcheck.setImageDrawable(context.getResources().getDrawable(R.drawable.arrow_up));
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }



}
