package com.fininsight.littlehero;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class NoPaidActivity extends AppCompatActivity implements View.OnClickListener{


    private ImageView imgDetailPaid;
    private RelativeLayout rlClose;
    private Animation animationClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_paid);
        rlClose = (RelativeLayout) findViewById(R.id.rl_close_popup);
        imgDetailPaid = (ImageView) findViewById(R.id.img_detail_paid);
        animationClick = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.click_button);
        rlClose.setOnClickListener(this);
        imgDetailPaid.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_close_popup:
               finish();
                break;
            case R.id.img_detail_paid:
                imgDetailPaid.startAnimation(animationClick);
                startActivity(new Intent(NoPaidActivity.this, PaidActivity.class));
                overridePendingTransition(R.anim.animatino_move_from_bottom_activity, R.anim.animation_fadeout);
                finish();
                break;
        }
    }
;
}
