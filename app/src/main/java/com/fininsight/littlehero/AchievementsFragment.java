package com.fininsight.littlehero;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fininsight.littlehero.adapter.AchievementsAdapter;
import com.fininsight.littlehero.classForData.Achievements;

import java.util.ArrayList;
import java.util.List;


public class AchievementsFragment extends Fragment implements View.OnClickListener{

    private RecyclerView rvAchievements;
    private ImageView imgBack;
    private List<Achievements> achievementsList;
    private AchievementsAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_achievements, container, false);
        imgBack = (ImageView) view.findViewById(R.id.imgv_back);
        rvAchievements = (RecyclerView) view.findViewById(R.id.rv_achievements);
        achievementsList = achievementsListCreate();
        adapter = new AchievementsAdapter(achievementsList, getContext());
        rvAchievements.setAdapter(adapter);
       if (MainActivity.width>480) {
           rvAchievements.setLayoutManager(new GridLayoutManager(getContext(),3));
        } rvAchievements.setLayoutManager(new GridLayoutManager(getContext(),2));
     //   rvAchievements.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getContext()).build());
        imgBack.setOnClickListener(this);
        return view;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgv_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
    private List<Achievements> achievementsListCreate() {
        List<Achievements> data = new ArrayList<>();

        data.add(new Achievements(1,1, "Лучший комментарий"));
        data.add(new Achievements(2,1, "Без пропуска заданий"));
        data.add(new Achievements(3,0, "Максимум активности"));
        data.add(new Achievements(4,1, "Лучший комментарий"));
        data.add(new Achievements(5,0,  "Без пропуска заданий"));
        data.add(new Achievements(6,1, "Максимум активности"));
        data.add(new Achievements(7,1, "Лучший комментарий"));
        data.add(new Achievements(8,1,  "Без пропуска заданий"));
        data.add(new Achievements(9,1, "Максимум активности"));
        data.add(new Achievements(10,0,"Лучший комментарий"));
        data.add(new Achievements(11,0,  "Без пропуска заданий"));
        data.add(new Achievements(12,1, "Максимум активности"));
        data.add(new Achievements(13,1, "Лучший комментарий"));
        data.add(new Achievements(14,0,  "Без пропуска заданий"));
        data.add(new Achievements(15,1, "Лучший комментарий"));
        data.add(new Achievements(16,0, "Максимум активности"));
        data.add(new Achievements(17,0, "Лучший комментарий"));
        data.add(new Achievements(18,0,  "Без пропуска заданий"));
        data.add(new Achievements(19,1, "Лучший комментарий"));
        data.add(new Achievements(20,1,  "Без пропуска заданий"));
        data.add(new Achievements(21,1, "Лучший комментарий"));
        data.add(new Achievements(22,1, "Максимум активности"));
        data.add(new Achievements(23,1,  "Без пропуска заданий"));

        return data;
    }

}
