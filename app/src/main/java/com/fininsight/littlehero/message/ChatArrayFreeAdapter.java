package com.fininsight.littlehero.message;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.models.messageModels.ChatMessageModel;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

public class ChatArrayFreeAdapter extends ArrayAdapter<ChatMessageModel> {

    private TextView chatText;
    private RoundedImageView imageChat;
    private RoundedImageView imageMyChat;
    private ImageView imgPundikLeft;
    private ImageView imgPundikRight;
    private List<ChatMessageModel> chatMessageList = new ArrayList<>();
    private Context context;
    private ImageView imgAvatar;

    @Override
    public void add(ChatMessageModel object) {
        chatMessageList.add(object);
        super.add(object);
    }

    public void addAll(List<ChatMessageModel> messages) {
        chatMessageList.addAll(messages);
    }

    public ChatArrayFreeAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    public int getCount() {
        return this.chatMessageList.size();
    }

    public ChatMessageModel getItem(int index) {
        return this.chatMessageList.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ChatMessageModel chatMessageObj = getItem(position);
        View row;
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (chatMessageObj.incoming) {
            row = inflater.inflate(R.layout.right, parent, false);
            imgPundikRight = (ImageView) row.findViewById(R.id.img_pundik_free_tv);
            imageMyChat = (RoundedImageView) row.findViewById(R.id.msgImgRight);
            if (chatMessageObj.getImage_file() != null) {
                if (!chatMessageObj.getImage_file().equals("")) {
                    imageMyChat.setVisibility(View.VISIBLE);
                 //   Picasso.with(row.getContext()).load(chatMessageObj.getPhotoToChatThumb()).placeholder(R.drawable.progress_animation).into(imageMyChat);
//                    imageMyChat.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent intent = new Intent(rowToIntent.getContext(), PhotoViewerFromMessageActivity.class);
//                            intent.putExtra("photo", chatMessageObj.getPhotoToChat());
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            rowToIntent.getContext().startActivity(intent);
//                        }
//                    });
                } else {
                    imageMyChat.setVisibility(View.GONE);
                    chatText = (TextView) row.findViewById(R.id.msgr);
                    chatText.setVisibility(View.VISIBLE);
                    imgPundikRight.setVisibility(View.VISIBLE);
                    chatText.setText(chatMessageObj.message);
                }
            } else {
                imageMyChat.setVisibility(View.GONE);
                chatText = (TextView) row.findViewById(R.id.msgr);
                chatText.setVisibility(View.VISIBLE);
                chatText.setText(chatMessageObj.message);
            }
        } else {
            row = inflater.inflate(R.layout.left, parent, false);
            imgPundikLeft = (ImageView) row.findViewById(R.id.img_pundik_white);
            imageChat = (RoundedImageView) row.findViewById(R.id.msgImg);
            chatText = (TextView) row.findViewById(R.id.msgr);
            imgAvatar = (ImageView) row.findViewById(R.id.profile_chat);
            if (chatMessageObj.getImage_file() != null) {
                if (!chatMessageObj.getImage_file().equals("")) {
                    imageChat.setVisibility(View.VISIBLE);
//                    Picasso.with(row.getContext()).load(chatMessageObj.getPhotoToChatThumb()).placeholder(R.drawable.progress_animation).into(imageChat);
//                    imageChat.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent intent = new Intent(rowToIntent.getContext(), PhotoViewerFromMessageActivity.class);
//                            intent.putExtra("photo", chatMessageObj.getPhotoToChat());
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            rowToIntent.getContext().startActivity(intent);
//                        }
//                    });
                } else {
                    imageChat.setVisibility(View.GONE);
                    imgPundikLeft.setVisibility(View.VISIBLE);
                    chatText.setVisibility(View.VISIBLE);
                    imgAvatar.setVisibility(View.VISIBLE);
                    chatText.setText(chatMessageObj.message);
                }
            } else {
                imageChat.setVisibility(View.GONE);
                chatText = (TextView) row.findViewById(R.id.msgr);
                imgPundikLeft.setVisibility(View.VISIBLE);
                chatText.setVisibility(View.VISIBLE);
                imgAvatar.setVisibility(View.VISIBLE);
                chatText.setText(chatMessageObj.message);
            }
        }
        return row;
    }

}