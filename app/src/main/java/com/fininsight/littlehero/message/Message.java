package com.fininsight.littlehero.message;

/**
 * Created by Shakhov on 19.02.2016.
 */
public class Message {
    public String idUser;
    // expert - 0 - no, 1 - yes
    public int expert;
    public int online;
    public String name;
    public String message;
    // from = 0 somebody, 1 - my
    public int from;

    public int imageId;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public int getExpert() {
        return expert;
    }

    public void setExpert(int expert) {
        this.expert = expert;
    }

    public int getOnline() {
        return online;
    }

    public void setOnline(int online) {
        this.online = online;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public Message(int imageId, String name, String message, int from, int expert, int online) {
        this.imageId = imageId;
        this.name = name;
        this.message = message;
        this.from = from;
        this.expert = expert;
        this.online = online;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }





}
