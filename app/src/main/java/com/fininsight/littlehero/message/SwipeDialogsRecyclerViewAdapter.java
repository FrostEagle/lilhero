package com.fininsight.littlehero.message;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.fininsight.littlehero.R;
import com.fininsight.littlehero.application.GlobalValue;
import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.models.chatModels.ChatModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SwipeDialogsRecyclerViewAdapter extends RecyclerSwipeAdapter<SwipeDialogsRecyclerViewAdapter.SimpleViewHolder> {


    private Context mContext;
    private ArrayList<ChatModel> messageList;

    public SwipeDialogsRecyclerViewAdapter(Context context, ArrayList<ChatModel> objects) {
        this.mContext = context;
        this.messageList = objects;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.swipe_row_message_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        final ChatModel item = messageList.get(position);
//        if (item.getExpert()==1) {
//            viewHolder.tvName.setTextColor(mContext.getResources().getColor(R.color.white));
//            viewHolder.tvMessage.setTextColor(mContext.getResources().getColor(R.color.white));
//            viewHolder.imgFrom.setImageDrawable(mContext.getResources().getDrawable(R.drawable.dialog_reply_white));
//            viewHolder.imgDialogBgExpert.setVisibility(View.VISIBLE);
//        } else {
            viewHolder.tvName.setTextColor(mContext.getResources().getColor(R.color.text_black_color));
            viewHolder.tvMessage.setTextColor(mContext.getResources().getColor(R.color.text_grey));
            viewHolder.imgFrom.setImageDrawable(mContext.getResources().getDrawable(R.drawable.dialog_reply));
            viewHolder.imgDialogBgExpert.setVisibility(View.INVISIBLE);
        //}
//        if (item.getOnline()==1) viewHolder.imgOnline.setVisibility(View.VISIBLE);
//        else viewHolder.imgOnline.setVisibility(View.INVISIBLE);
        if(item.getLast_message().getUser_id().equals(GlobalValue.userModel.getUser().get_id())) viewHolder.imgFrom.setVisibility(View.VISIBLE);
        else viewHolder.imgFrom.setVisibility(View.GONE);
        viewHolder.tvName.setText(item.getChatters().get(0).getName());
        viewHolder.tvMessage.setText(item.getLast_message().getMessage());
        if (item.getChatters().get(0).getAvatar_file()!=null) {
            Picasso.with(mContext).load("http://little-hero.gmg-spb.ru/images/messages/"+item.getChatters().get(0).getAvatar_file()).into(viewHolder.civImageId);
        } else viewHolder.civImageId.setImageDrawable(mContext.getResources().getDrawable(R.drawable.main_avatar));

        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        // Drag From Right
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.rl_dialog_delete));

        viewHolder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                viewHolder.swipeLayout.close();
//                mItemManger.closeAllItems();
//                if (item.getExpert()==1) {
//                    Intent myIntent = new Intent(mContext, ChatExpertActivity.class);
//                    myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    mContext.startActivity(myIntent);
//                } else {
                    Intent myIntent = new Intent(mContext, ChatActivity.class);
                    myIntent.putExtra("id",item.get_id());
                    myIntent.putExtra("login","testLogin");
                    myIntent.putExtra("from","chat");
                    myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(myIntent);
              //  }

            }
        });
        viewHolder.rlDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Constants.BROADCAST_ACTION_UPDATE_FREE_CHAT);
                mContext.sendBroadcast(intent);
                mItemManger.removeShownLayouts(viewHolder.swipeLayout);
                messageList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, messageList.size());
                mItemManger.closeAllItems();
              //  Toast.makeText(view.getContext(), "Deleted " + viewHolder.tvName.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
        mItemManger.bindView(viewHolder.itemView, position);
    }
    @Override
    public int getItemCount() {
        return messageList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        private SwipeLayout swipeLayout;
        private TextView tvName, tvMessage;
        private CircleImageView civImageId;
        private RelativeLayout rlDelete;
        private ImageView imgOnline, imgDialogBgExpert, imgFrom;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            imgDialogBgExpert = (ImageView) itemView.findViewById(R.id.img_dialog_expert_bg);
            imgOnline = (ImageView) itemView.findViewById(R.id.img_dialog_online);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            imgFrom = (ImageView) itemView.findViewById(R.id.img_from_dialog);
            tvMessage = (TextView) itemView.findViewById(R.id.tvMessage);
            rlDelete = (RelativeLayout) itemView.findViewById(R.id.rl_dialog_delete);
            civImageId = (CircleImageView) itemView.findViewById(R.id.img_message_avatar);

        }
    }
}
