package com.fininsight.littlehero.message;


public class ChatMessage {
    public boolean incoming;
    public String message;
    private String idMessage;
    private String createdAt;
    private String photo;
    private String photoToChat;
    private String photoToChatThumb;


    public String getPhotoToChatThumb() {
        return photoToChatThumb;
    }

    public void setPhotoToChatThumb(String photoToChatThumb) {
        this.photoToChatThumb = photoToChatThumb;
    }
    public String getPhotoToChat() {
        return photoToChat;
    }

    public void setPhotoToChat(String photoToChat) {
        this.photoToChat = photoToChat;
    }


    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(String idMessage) {
        this.idMessage = idMessage;
    }

    public boolean isIncoming() {
        return incoming;
    }

    public void setIncoming(boolean incoming) {
        this.incoming = incoming;
    }

    public ChatMessage(boolean incoming, String message) {
        super();
        this.incoming = incoming;
        this.message = message;
    }    public ChatMessage(boolean incoming, String message, String createdAt) {
        super();
        this.incoming = incoming;
        this.message = message;
        this.createdAt = createdAt;
    }

    public ChatMessage(boolean incoming, String message, String photoToChat, String photo, String photoToChatThumb, String createdAt) {
        super();
        this.incoming = incoming;
        this.message = message;
        this.photoToChat = photoToChat;
        this.photo = photo;
        this.createdAt = createdAt;
        this.photoToChatThumb = photoToChatThumb;
    }

    public ChatMessage() {
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
