package com.fininsight.littlehero.message;

import android.annotation.TargetApi;
import android.database.DataSetObserver;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.chatModels.MessageListInChatModel;
import com.fininsight.littlehero.models.messageModels.ChatMessageModel;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.message.GetMessageClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.message.NewMessageClient;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener{
    private List<ChatMessageModel> listMessages;
    private RelativeLayout rlProfile;
    private RelativeLayout rlBack;
    private ChatArrayFreeAdapter chatArrayAdapter;
    private ListView listView;
    private EditText edtChatText;
    private TextView tvButtonSend;
    private AccessToken accessToken;
    private Call<ChatMessageModel> callMessage;
    private String message;
    private ImageView imgProfile;
    private TextView tvLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        listMessages = new ArrayList<>();
        rlBack = (RelativeLayout) findViewById(R.id.rl_back);
        tvButtonSend = (TextView) findViewById(R.id.tv_message_send);
        listView = (ListView) findViewById(R.id.msgview);
        edtChatText = (EditText) findViewById(R.id.edt_msg);
        imgProfile = (ImageView) findViewById(R.id.img_profile);
        tvLogin = (TextView) findViewById(R.id.tv_friends_header);
        if (getIntent().getStringExtra("from").equals("chat")) {
            getMessages(getIntent().getStringExtra("id"));
        }
       // if (getUserModels.getUser().getAvatar_file()!=null) {
            Picasso.with(ChatActivity.this).load("http://little-hero.gmg-spb.ru/images/avatars/"+getIntent().getStringExtra("login")).into(imgProfile);
      //  } else  imgAvatar.setImageDrawable(getResources().getDrawable(R.drawable.no_photo_avatar));
        tvLogin.setText(getIntent().getStringExtra("login"));
        chatArrayAdapter = new ChatArrayFreeAdapter(getApplicationContext(), R.layout.right);
        listView.setAdapter(chatArrayAdapter);
        accessToken = AccessTokenReturnObject.accessTokenReturn(ChatActivity.this);
        chatArrayAdapter.notifyDataSetChanged();
        edtChatText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 0) {
                    tvButtonSend.setClickable(false);
                    tvButtonSend.setTextColor(getResources().getColor(R.color.colorForLines));
                } else {
                    tvButtonSend.setClickable(true);
                    tvButtonSend.setTextColor(getResources().getColor(R.color.text_yellow_color));
                }
            }
        });
        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });
        chatArrayAdapter.addAll(listMessages);
        rlBack.setOnClickListener(this);
        tvButtonSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_profile_open:
                break;
            case R.id.rl_back:
                finish();
                break;
            case R.id.tv_message_send:
                message = edtChatText.getText().toString().trim();
                postMessage();
                chatArrayAdapter.add(new ChatMessageModel(true, message));
                edtChatText.setText("");
                break;
        }
    }
    private void getMessages(String id) {
        GetMessageClient getMessages = ServiceGenerator.createService(GetMessageClient.class, accessToken, ChatActivity.this);
        Call<MessageListInChatModel> call = getMessages.getMessage(id);
        call.enqueue(new Callback<MessageListInChatModel>() {
            @Override
            public void onResponse(Call<MessageListInChatModel> call, Response<MessageListInChatModel> response) {
                if (response.isSuccessful()) {
                    MessageListInChatModel model = response.body();
                    listMessages.addAll(model.getMessages());
                    chatArrayAdapter.addAll(listMessages);
//                    listPost = response.body();
//                    postAdapter = new PostRecyclerAdapter(listPost, getContext());
//                    rvPost.setAdapter(postAdapter);
//                    rvPost.setLayoutManager(new LinearLayoutManager(getContext()));
                }
            }
            @Override
            public void onFailure(Call<MessageListInChatModel> call, Throwable t) {
            }
        });
    }
    private void postMessage() {
        NewMessageClient postMessage = ServiceGenerator.createService(NewMessageClient.class, accessToken, ChatActivity.this);
      //  if (getIntent().getStringExtra("from").equals("chat")) {
       //     callMessage = postMessage.newMessageAddChatId(getIntent().getStringExtra("id"),edtChatText.getText().toString());
       // } else if (getIntent().getStringExtra("from").equals("user")) {
            callMessage = postMessage.newMessageAddWithUserId(getIntent().getStringExtra("id"),edtChatText.getText().toString());
      //  }
        callMessage.enqueue(new Callback<ChatMessageModel>() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<ChatMessageModel> call, Response<ChatMessageModel> response) {
                ResponseBody errorBo = response.errorBody();
                int responseCode = response.code();
                if (response.isSuccessful()) {

                }
            }
            @Override
            public void onFailure(Call<ChatMessageModel> call, Throwable t) {
            }
        });
    }
}
