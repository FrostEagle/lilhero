package com.fininsight.littlehero.message;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.ExpertProfileActivity;
import com.fininsight.littlehero.PaidActivity;
import com.fininsight.littlehero.R;

import java.util.ArrayList;
import java.util.List;

public class ChatExpertActivity extends AppCompatActivity implements View.OnClickListener {
    private List<ChatMessage> listMessages;
    private RelativeLayout rlProfile;
    private ChatArrayExpertAdapter chatArrayAdapter;
    private ListView listView;
    private EditText chatText;
    private TextView buttonSend;
    private Button  btnContinue;
    private RelativeLayout rlBack;
    private ImageView imgProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_expert);
        listMessages = new ArrayList<>();
        rlProfile = (RelativeLayout) findViewById(R.id.rl_profile_open);
        rlBack = (RelativeLayout) findViewById(R.id.rl_back);
        buttonSend = (TextView) findViewById(R.id.send);
        listView = (ListView) findViewById(R.id.msgview);
        chatText = (EditText) findViewById(R.id.msg);
        btnContinue = (Button) findViewById(R.id.btn_continue);
        imgProfile = (ImageView) findViewById(R.id.profile_image_add);
        imgProfile.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        addChat();
        chatArrayAdapter = new ChatArrayExpertAdapter(getApplicationContext(), R.layout.right);
        listView.setAdapter(chatArrayAdapter);
        chatArrayAdapter.notifyDataSetChanged();
        chatText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    buttonSend.setClickable(false);
                    buttonSend.setTextColor(getResources().getColor(R.color.colorForLines));
                } else {
                    buttonSend.setClickable(true);
                    buttonSend.setTextColor(getResources().getColor(R.color.text_yellow_color));
                }
            }
        });
        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });
        chatArrayAdapter.addAll(listMessages);

        btnContinue.setOnClickListener(this);
        rlProfile.setOnClickListener(this);
        rlBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_profile_open:
                startActivity(new Intent(ChatExpertActivity.this, ExpertProfileActivity.class));
                break;
            case R.id.rl_back:
                finish();
                break;
            case R.id.btn_continue:
                startActivity(new Intent(ChatExpertActivity.this, PaidActivity.class));
                break;

        }
    }
    private void addChat() {
        listMessages.add(new ChatMessage(true,"Здравствуйте Инна, помогите решить проблему с заданием"));
        listMessages.add(new ChatMessage(false,"Добрый день Анна, в чем проблема"));
        listMessages.add(new ChatMessage(true,"У меня закончился горох могу гречку"));
        listMessages.add(new ChatMessage(false,"Можно любую крупу"));
    }
}
