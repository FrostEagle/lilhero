package com.fininsight.littlehero;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.adapter.ChildrenMyProfileAdapter;
import com.fininsight.littlehero.expandableMemo.MemoryChildrenFragment;
import com.fininsight.littlehero.fragmentFolower.FollowerFragment;
import com.fininsight.littlehero.fragmentFolower.FollowersFragment;
import com.fininsight.littlehero.menu_tab_four.MyPostFragment;
import com.fininsight.littlehero.message.ChatActivity;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.DeletePostModel;
import com.fininsight.littlehero.models.FollowerModel;
import com.fininsight.littlehero.models.UserModel;
import com.fininsight.littlehero.models.babyModels.ChildModel;
import com.fininsight.littlehero.otherClass.PhotoProfileViewerActivity;
import com.fininsight.littlehero.photos.ProcessingPhotos;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.delete.UnFollowClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetUserProfileClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.FollowClient;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileFragment extends Fragment implements View.OnClickListener {
    private RelativeLayout rlBack, rlAchievementsAll;
    private View view;
    private Fragment fragment = null;
    private Class fragmentClass = null;
    private FragmentActivity myContext;
    private TextView tvMainName, tvNameSecond, tvCountFollowers, tvCountSubscriptions, tvCountPosts, tvForCountPosts, tvCountDone, tvTime;
    private FragmentTransaction fragTransaction;
    private LinearLayout linerAchievementsIcon, linerAchievementsIconText;
    private AccessToken accessToken;
    private RecyclerView rvKids;
    private List<ChildModel> childrenList;
    private ChildrenMyProfileAdapter adapter;
    private GetUserProfileClient getUserClient;
    private Call<UserModel> call;
    private LinearLayout topLinearLayout;
    private HorizontalScrollView scrollView;
    private int whatPhotoCount = 0;
    private String mCurrentPhotoPath;
    private Bitmap bmpPostPhoto;
    private ProcessingPhotos mProcessingPhotos;
    private Fragment fragmentActivity = this;
    private ImageView imgAvatar, imgCover, imgFollow, imgSendMessage, imgFollowActive;
    private Animation animationClickFollow, animationClickSendMessage;
    private Bundle args;
    private UserModel getUserModels;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        initUI();
        mProcessingPhotos = new ProcessingPhotos(getContext());
        childrenList = new ArrayList<>();
        args = getArguments();
        rvKids.setLayoutManager(new LinearLayoutManager(getContext()));
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());
        animationClickFollow = AnimationUtils.loadAnimation(getContext(), R.anim.click_button_pregnant);
        animationClickSendMessage = AnimationUtils.loadAnimation(getContext(), R.anim.click_button_pregnant);
//        if (GlobalValue.userModel!=null) {
//            initValueUI(GlobalValue.userModel);
//        } else {
        getUserClient = ServiceGenerator.createService(GetUserProfileClient.class, accessToken, getContext());
        call = getUserClient.getUserModels(args.getString("id"));
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                getUserModels = response.body();
                if (response.isSuccessful()) {
                    initValueUI(getUserModels);
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
            }
        });
        //  }
        imgFollowActive.setOnClickListener(this);
        tvCountPosts.setOnClickListener(this);
        tvForCountPosts.setOnClickListener(this);
        tvCountFollowers.setOnClickListener(this);
        tvCountSubscriptions.setOnClickListener(this);
        rlAchievementsAll.setOnClickListener(this);
        linerAchievementsIcon.setOnClickListener(this);
        linerAchievementsIconText.setOnClickListener(this);
        imgFollow.setOnClickListener(this);
        imgSendMessage.setOnClickListener(this);
        rlBack.setOnClickListener(this);
        return view;
    }

    private void initValueUI(UserModel getUserModels) {
        tvCountFollowers.setText(String.valueOf(getUserModels.getUser().getFollowers_counter()));
        tvCountSubscriptions.setText(String.valueOf(getUserModels.getUser().getFollowing_counter()));
        tvCountPosts.setText(String.valueOf(getUserModels.getUser().getPosts_counter()));
        tvMainName.setText(getUserModels.getUser().getName());
        tvCountDone.setText(String.valueOf(getUserModels.getUser().getDone_counter()));
        tvTime.setText(String.valueOf(getUserModels.getUser().getTime_counter()));
        tvNameSecond.setText(getUserModels.getUser().getLogin());
        if (getUserModels.getUser().getRelation().equals("mutual")) {
            imgFollow.setVisibility(View.GONE);
            imgFollowActive.setVisibility(View.VISIBLE);
        } else {
            imgFollowActive.setVisibility(View.GONE);
            imgFollow.setVisibility(View.VISIBLE);
        }
        //hack
        topLinearLayout = new LinearLayout(getContext());
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        scrollView.removeAllViews();
        scrollView.addView(topLinearLayout);
        if (getUserModels.getUser().getPhotos() > 0) {
            for (int i = 0; i < getUserModels.getUser().getPhotos(); i++) {
                photoToPostAddImageViewFromServer(getUserModels.getUser().getLogin(), i);
            }
        }
        if (getUserModels.getUser().getAvatar_file() != null) {
            Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/avatars/" + getUserModels.getUser().getLogin()).into(imgAvatar);
        } else imgAvatar.setImageDrawable(getResources().getDrawable(R.drawable.no_photo_avatar));
        if (getUserModels.getUser().getCover_file() != null) {
            Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/covers/" + getUserModels.getUser().getLogin()).into(imgCover);
        } else Picasso.with(getContext()).load(R.drawable.cover_profile_main).into(imgCover);
        childrenList = getUserModels.getUser().getChilds();
        adapter = new ChildrenMyProfileAdapter(childrenList, getContext());
        rvKids.setAdapter(adapter);
    }

    private void initUI() {
        imgFollowActive = (ImageView) view.findViewById(R.id.img_follow_active);
        tvCountDone = (TextView) view.findViewById(R.id.tv_card_count_profile);
        tvTime = (TextView) view.findViewById(R.id.tv_time_count_profile);
        rlBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        imgFollow = (ImageView) view.findViewById(R.id.img_follow);
        imgSendMessage = (ImageView) view.findViewById(R.id.img_send_message);
        imgAvatar = (ImageView) view.findViewById(R.id.img_avatar);
        imgCover = (ImageView) view.findViewById(R.id.img_cover);
        topLinearLayout = new LinearLayout(getContext());
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        scrollView = (HorizontalScrollView) view.findViewById(R.id.sc_photo);
        scrollView.addView(topLinearLayout);
        rvKids = (RecyclerView) view.findViewById(R.id.rv_kids);
        tvMainName = (TextView) view.findViewById(R.id.tv_main_name);
        tvNameSecond = (TextView) view.findViewById(R.id.tv_name_second);
        tvCountSubscriptions = (TextView) view.findViewById(R.id.tv_count_subscriptions);
        tvCountPosts = (TextView) view.findViewById(R.id.tv_count_notation_profile);
        tvForCountPosts = (TextView) view.findViewById(R.id.tv_for_profile_post);
        tvCountFollowers = (TextView) view.findViewById(R.id.tv_count_subscribers);
        linerAchievementsIcon = (LinearLayout) view.findViewById(R.id.liner_for_achivements_imgview);
        linerAchievementsIconText = (LinearLayout) view.findViewById(R.id.linerFor_text_below_achivements);
        rlAchievementsAll = (RelativeLayout) view.findViewById(R.id.rl_achievements_all);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_follow_active:
                imgFollowActive.startAnimation(animationClickFollow);
                String[] listitems = {"Отписаться", "Отмена"};
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setItems(listitems, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                deleteFollow();
                                break;
                            case 1:
                                dialog.cancel();
                                break;
                        }
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
                break;
            case R.id.img_follow:
                imgFollow.startAnimation(animationClickFollow);
                postFollow();
                break;
            case R.id.img_send_message:
                imgSendMessage.startAnimation(animationClickSendMessage);
                Intent myIntent = new Intent(getContext(), ChatActivity.class);
                myIntent.putExtra("id", getUserModels.getUser().get_id());
                myIntent.putExtra("login", getUserModels.getUser().getLogin());
                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myIntent);
                break;
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.rl_pamyatka:
                MemoryChildrenFragment memoFrag = new MemoryChildrenFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, memoFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.tv_for_profile_post:
                Bundle argsPost = new Bundle();
                argsPost.putString("id", getUserModels.getUser().get_id());
                argsPost.putString("login", getUserModels.getUser().getLogin());
                MyPostFragment postFrag = new MyPostFragment();
                postFrag.setArguments(argsPost);
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.add(R.id.frame_for_fragment, postFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.tv_count_notation_profile:
                Bundle argsPost1 = new Bundle();
                argsPost1.putString("id", getUserModels.getUser().get_id());
                argsPost1.putString("login", getUserModels.getUser().getLogin());
                MyPostFragment postFrag1 = new MyPostFragment();
                postFrag1.setArguments(argsPost1);
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.add(R.id.frame_for_fragment, postFrag1);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.rl_achievements_all:
                AchievementsFragment achievFrag = new AchievementsFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, achievFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.liner_for_achivements_imgview:
                AchievementsFragment achievFrag1 = new AchievementsFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, achievFrag1);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.linerFor_text_below_achivements:
                AchievementsFragment achievFrag2 = new AchievementsFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, achievFrag2);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.tv_count_subscribers:
                Bundle args = new Bundle();
                args.putString("id", getUserModels.getUser().get_id());
                FollowersFragment msFragFollowers = new FollowersFragment();
                msFragFollowers.setArguments(args);
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, msFragFollowers);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.tv_count_subscriptions:
                Bundle args1 = new Bundle();
                args1.putString("id", getUserModels.getUser().get_id());
                FollowerFragment msFragFollower = new FollowerFragment();
                msFragFollower.setArguments(args1);
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, msFragFollower);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
        }
    }

    private void postFollow() {
        FollowClient followClient = ServiceGenerator.createService(FollowClient.class, accessToken, getContext());
        Call<FollowerModel> callDevice = followClient.postFollower(args.getString("id"));
        callDevice.enqueue(new Callback<FollowerModel>() {
            @Override
            public void onResponse(Call<FollowerModel> call, Response<FollowerModel> response) {
                ResponseBody errorBo = response.errorBody();
                int responseCode = response.code();
                if (response.isSuccessful()) {
                    imgFollow.setVisibility(View.GONE);
                    imgFollowActive.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<FollowerModel> call, Throwable t) {
            }
        });
    }

    private void deleteFollow() {
        UnFollowClient followClient = ServiceGenerator.createService(UnFollowClient.class, accessToken, getContext());
        Call<DeletePostModel> callDevice = followClient.postFollower(args.getString("id"));
        callDevice.enqueue(new Callback<DeletePostModel>() {
            @Override
            public void onResponse(Call<DeletePostModel> call, Response<DeletePostModel> response) {
                ResponseBody errorBo = response.errorBody();
                int responseCode = response.code();
                if (response.isSuccessful()) {
                    imgFollowActive.setVisibility(View.GONE);
                    imgFollow.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<DeletePostModel> call, Throwable t) {
            }
        });
    }

    private void photoToPostAddImageViewFromServer(String id, int count) {
        final RelativeLayout relativeLayout = new RelativeLayout(getContext());
        final RoundedImageView imageView = new RoundedImageView(getContext());
        imageView.setTag(whatPhotoCount);
        imageView.setCornerRadius(10);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toViewer = new Intent(getContext(), PhotoProfileViewerActivity.class);
                toViewer.putExtra("id", getUserModels.getUser().getLogin());
                toViewer.putExtra("count", getUserModels.getUser().getPhotos());
                startActivity(toViewer);
            }
        });
        Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/photos/" + id + "/" + count).fit().into(imageView);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getPx(100), getPx(100));
        lp.setMargins(getPx(10), getPx(0), getPx(0), getPx(0));
        relativeLayout.addView(imageView, lp);
        topLinearLayout.addView(relativeLayout);
        whatPhotoCount++;
    }

    //set dp calculate automatically
    public int getPx(int dimensionDp) {
        float density = getResources().getDisplayMetrics().density;
        return (int) (dimensionDp * density + 0.5f);
    }

}
