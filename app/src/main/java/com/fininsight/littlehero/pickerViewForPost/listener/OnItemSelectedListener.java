package com.fininsight.littlehero.pickerViewForPost.listener;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
