package com.fininsight.littlehero;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.fragmentMenu.AllDialogsFragment;
import com.fininsight.littlehero.fragmentMenu.FeedNewsFragment;
import com.fininsight.littlehero.fragmentMenu.TaskMainFragment;
import com.gigamole.navigationtabbar.ntb.NavigationTabBar;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private View line1,line2, line3, line4;
    public static int width = 0;
    private Fragment fragment = null;
    private Class fragmentClass = null;
    private FragmentTransaction fragTransaction;
    private RelativeLayout rlPopup, rlPopupAchievements;
    private ImageView imgPopupClose, imgPaidClose, imgPopupAchievementsClose;
    private Animation animationMoveFromBottom, animationMoveFromBottomPaid, animationMoveFromBottomAchievements, animationMoveFromBottomDetail;
    private FragmentManager fragmentManager;
    private ViewPager viewPager;
    private NavigationTabBar navigationTabBar;
    private RelativeLayout rlPaid;
    private ImageView imgPaidBtnExpert, imgAcceptExpert;
    private ImageView imgPaidBtnNoExpert, imgAcceptNoExpert;
    //This view paid subscriptions header
    private RelativeLayout rlCardWithExpert, rlCardNoExpert;
    private ImageView imgCardNoExpert, imgCardExpert;
    private TextView tvCardNoExpert, tvCardExpert;
    private ScrollView scExpert, scNoExpert;
    private RelativeLayout rlDetailPaid;
   // private ImageView imgCloseDetail;
 //   private ImageView imgDetailPaid;
    //Neew for khnow its free acc or not
    public static int whatIsAccount;
    private RelativeLayout rlPaid3Day;
    private RelativeLayout rlImageClose3day;
    private ImageView imgDetailPaid3Day;
    private Animation animationMoveFromBottomPaid3Day;
    private Animation animationMoveFromBottomRateApp;
    private RelativeLayout rlImageCloseRate;
    private RelativeLayout rlRateApp;
    private ImageView imgRate;
    private SimpleRatingBar ratingBar;
    private boolean doubleBackToExitPressedOnce = false;



    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(brHide);
        unregisterReceiver(br);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
//            return;
//        }
//        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "Если вы хотите выйти из приложения нажмите еще раз", Toast.LENGTH_SHORT).show();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce=false;
//            }
//        }, 3000);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        DisplayMetrics displaymetrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
//        width = displaymetrics.widthPixels;
        initUI();
        whatIsAccount = 0;
        registerReceiver(br, new IntentFilter(Constants.BROADCAST_ACTION_UPDATE_DETAIL));
        registerReceiver(brHide, new IntentFilter(Constants.BROADCAST_ACTION_HIDE_BOTTOM_BAR));
        animationMoveFromBottom = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_from_bottom_popup);
        animationMoveFromBottomDetail = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_from_bottom_popup);
        animationMoveFromBottomAchievements = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_from_bottom_popup);
        animationMoveFromBottomPaid = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_from_bottom_popup);
        animationMoveFromBottomPaid3Day = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_from_bottom_popup);
        animationMoveFromBottomRateApp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animatino_move_from_bottom_popup);
        fragmentClass = MyProfileFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_for_fragment, fragment).commit();
        int secondsDelayed = 1;

//        new Handler().postDelayed(new Runnable() {
//            public void run() {
//                rlPopup.setVisibility(View.VISIBLE);
//                rlPopup.startAnimation(animationMoveFromBottom);
//            }
//        }, secondsDelayed * 9500);
//        new Handler().postDelayed(new Runnable() {
//            public void run() {
//                rlPopupAchievements.setVisibility(View.VISIBLE);
//                rlPopupAchievements.startAnimation(animationMoveFromBottomAchievements);
//            }
//        }, secondsDelayed * 14500);
//        startActivity(new Intent(MainActivity.this, PaidActivity.class));
//        rlPaid.setVisibility(View.VISIBLE);
//        rlPaid.startAnimation(animationMoveFromBottomPaid);
//        new Handler().postDelayed(new Runnable() {
//            public void run() {
//                rlPaid3Day.setVisibility(View.VISIBLE);
//                rlPaid3Day.startAnimation(animationMoveFromBottomPaid3Day);
//            }
//        }, secondsDelayed * 8500);
//        new Handler().postDelayed(new Runnable() {
//            public void run() {
//                rlRateApp.setVisibility(View.VISIBLE);
//                rlRateApp.startAnimation(animationMoveFromBottomRateApp);
//            }
//        }, secondsDelayed * 10500);
        imgPopupClose.setOnClickListener(this);
        imgPaidClose.setOnClickListener(this);
        imgPaidBtnExpert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlCardNoExpert.setVisibility(View.GONE);
                imgPaidBtnExpert.setVisibility(View.GONE);
                imgAcceptExpert.setVisibility(View.VISIBLE);
            }
        });;
        imgPaidBtnNoExpert.setOnClickListener(this);
        rlCardNoExpert.setOnClickListener(this);
        rlCardWithExpert.setOnClickListener(this);
        imgPopupAchievementsClose.setOnClickListener(this);
       // imgCloseDetail.setOnClickListener(this);
//        imgDetailPaid.setOnClickListener(this);
        imgDetailPaid3Day.setOnClickListener(this);
        rlImageClose3day.setOnClickListener(this);
        rlRateApp.setOnClickListener(this);
        rlImageCloseRate.setOnClickListener(this);
        imgRate.setOnClickListener(this);
    }

    private void initUI() {
        ratingBar = (SimpleRatingBar) findViewById(R.id.ratebar);
        imgRate = (ImageView) findViewById(R.id.img_detail_rate);
        rlRateApp = (RelativeLayout) findViewById(R.id.rl_pop_up_rate);
        rlImageCloseRate = (RelativeLayout) findViewById(R.id.rl_close_popup_rate);
        rlPaid3Day = (RelativeLayout) findViewById(R.id.rl_pop_up_paid3day);
        rlImageClose3day = (RelativeLayout) findViewById(R.id.rl_close_popup_paid3day);
      //  imgCloseDetail = (ImageView) findViewById(R.id.img_close_popup_detail_paid);
        imgDetailPaid3Day = (ImageView) findViewById(R.id.img_detail_paid3day);
     //   imgDetailPaid = (ImageView) findViewById(R.id.img_detail_paid);
        rlDetailPaid = (RelativeLayout) findViewById(R.id.rl_pop_up_detail_paid);
        imgPopupAchievementsClose = (ImageView) findViewById(R.id.img_close_popup_achievements);
        rlPopupAchievements = (RelativeLayout) findViewById(R.id.rl_pop_up_achievements);
        imgAcceptExpert = (ImageView) findViewById(R.id.img_paid_accept_expert);
        imgPaidBtnExpert = (ImageView) findViewById(R.id.img_btn_paid_subscr_expert);

        imgAcceptNoExpert = (ImageView) findViewById(R.id.img_paid_accept);
        imgPaidBtnNoExpert = (ImageView) findViewById(R.id.img_btn_paid_no_expert);

        scExpert = (ScrollView) findViewById(R.id.sc_expert_paid);
        scNoExpert = (ScrollView) findViewById(R.id.sc_no_expert_paid);

        rlCardNoExpert = (RelativeLayout) findViewById(R.id.rl_card_no_expert);
        rlCardWithExpert = (RelativeLayout) findViewById(R.id.rl_card_with_expert);

        tvCardNoExpert = (TextView) findViewById(R.id.tv_card_paid);
        tvCardExpert = (TextView) findViewById(R.id.tv_card_with_chat_paid);

        imgCardNoExpert = (ImageView) findViewById(R.id.img_card_no_expert);
        imgCardExpert = (ImageView) findViewById(R.id.img_card_chat_expert);

        rlPaid = (RelativeLayout) findViewById(R.id.rl_paid_subscr);
        imgPaidClose = (ImageView) findViewById(R.id.imgv_back_paid);

        imgPopupClose = (ImageView) findViewById(R.id.img_close_popup);
        rlPopup = (RelativeLayout) findViewById(R.id.rl_pop_up);
        line1  = findViewById(R.id.line1);
        line2  = findViewById(R.id.line2);
        line3  = findViewById(R.id.line3);
        line4  = findViewById(R.id.line4);
        viewPager = (ViewPager) findViewById(R.id.vp_horizontal_ntb);
        viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 4;
            }

            @Override
            public boolean isViewFromObject(final View view, final Object object) {
                return view.equals(object);
            }
            @Override
            public void destroyItem(final View container, final int position, final Object object) {
                ((ViewPager) container).removeView((View) object);
            }
            @Override
            public Object instantiateItem(final ViewGroup container, final int position) {
                final View view = LayoutInflater.from(getBaseContext()).inflate(R.layout.item_navigation_bar, null, false);
                container.addView(view);
                return view;
            }
        });
        navigationTabBar = (NavigationTabBar) findViewById(R.id.ntb_horizontal);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.bottom_first),
                        getResources().getColor(R.color.colorWhite))
                        .build()
        );
        models.add(new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.bottom_third_test),
                        getResources().getColor(R.color.colorWhite))
                        .build()
        );
        models.add(new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.bottom_second),
                        getResources().getColor(R.color.colorWhite))
                        .build()
        );
        models.add(new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.bottom_four),
                        getResources().getColor(R.color.colorWhite))
                        .build()
        );
        navigationTabBar.setOnTabBarSelectedIndexListener(new NavigationTabBar.OnTabBarSelectedIndexListener() {
            @Override
            public void onStartTabSelected(NavigationTabBar.Model model, int index) {
                switch (index) {
                    case 0:
                        fragmentClass = FeedNewsFragment.class;
                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        line1.setVisibility(View.VISIBLE);
                        line2.setVisibility(View.INVISIBLE);
                        line3.setVisibility(View.INVISIBLE);
                        line4.setVisibility(View.INVISIBLE);
                        break;
                    case 1:
                        fragmentClass = AllDialogsFragment.class;
                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        line1.setVisibility(View.INVISIBLE);
                        line2.setVisibility(View.VISIBLE);
                        line3.setVisibility(View.INVISIBLE);
                        line4.setVisibility(View.INVISIBLE);
                        break;
                    case 2:
                        fragmentClass = TaskMainFragment.class;
                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        line1.setVisibility(View.INVISIBLE);
                        line2.setVisibility(View.INVISIBLE);
                        line3.setVisibility(View.VISIBLE);
                        line4.setVisibility(View.INVISIBLE);
                        break;
                    case 3:
                        fragmentClass = MyProfileFragment.class;
                        try {
                            fragment = (Fragment) fragmentClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        line1.setVisibility(View.INVISIBLE);
                        line2.setVisibility(View.INVISIBLE);
                        line3.setVisibility(View.INVISIBLE);
                        line4.setVisibility(View.VISIBLE);
                        break;
                }
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.frame_for_fragment, fragment).commit();
            }
            @Override
            public void onEndTabSelected(NavigationTabBar.Model model, int index) {
            }
        });
        navigationTabBar.setBgColor(getResources().getColor(R.color.colorWhite));
        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager, 3);
    }
    private BroadcastReceiver br = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            rlDetailPaid.setVisibility(View.VISIBLE);
            rlDetailPaid.startAnimation(animationMoveFromBottomDetail);
        }
    };
    private BroadcastReceiver brHide = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            int status = intent.getIntExtra(Constants.PARAM_STATUS, 0);
            if (status == Constants.SHOW_BAR) {
                navigationTabBar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getPx(50)));
            } else {
                navigationTabBar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getPx(10)));
            }

        }
    };
    //set dp calculate automatically
    public int getPx(int dimensionDp) {
        float density = getResources().getDisplayMetrics().density;
        return (int) (dimensionDp * density + 0.5f);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_close_popup_rate:
                rlRateApp.clearAnimation();
                rlRateApp.setVisibility(View.GONE);
                break;
            case R.id.img_detail_rate:
                rlRateApp.clearAnimation();
                rlRateApp.setVisibility(View.GONE);
                startActivity(new Intent(MainActivity.this, RateAppActivity.class));
                break;
            case R.id.rl_close_popup_paid3day:
                rlPaid3Day.clearAnimation();
                rlPaid3Day.setVisibility(View.GONE);
                break;
            case R.id.img_detail_paid3day:
                rlPaid3Day.clearAnimation();
                rlPaid3Day.setVisibility(View.GONE);
                startActivity(new Intent(MainActivity.this, PaidActivity.class));
                break;
//            case R.id.img_close_popup_detail_paid:
//                rlDetailPaid.clearAnimation();
//                rlDetailPaid.setVisibility(View.GONE);
//                break;
//            case R.id.img_detail_paid:
//                rlDetailPaid.clearAnimation();
//                rlDetailPaid.setVisibility(View.GONE);
//                startActivity(new Intent(MainActivity.this, PaidActivity.class));
//                break;
            case R.id.img_close_popup:
                rlPopup.clearAnimation();
                rlPopup.setVisibility(View.GONE);
                break;
            case R.id.img_close_popup_achievements:
                rlPopupAchievements.clearAnimation();
                rlPopupAchievements.setVisibility(View.GONE);
                break;
            case R.id.imgv_back_paid:
                rlPaid.clearAnimation();
                rlPaid.setVisibility(View.GONE);
                break;
//            case R.id.img_btn_paid_subscr_expert:
//                Toast.makeText(MainActivity.this,"WWWWWWW",Toast.LENGTH_LONG).show();
//                rlCardNoExpert.setVisibility(View.GONE);
//                imgPaidBtnExpert.setVisibility(View.GONE);
//                imgAcceptExpert.setVisibility(View.VISIBLE);
            case R.id.img_btn_paid_no_expert:
                rlCardWithExpert.setVisibility(View.GONE);
                imgPaidBtnNoExpert.setVisibility(View.GONE);
                imgAcceptNoExpert.setVisibility(View.VISIBLE);
            case R.id.rl_card_no_expert:
                scExpert.setVisibility(View.GONE);
                scNoExpert.setVisibility(View.VISIBLE);
                imgCardNoExpert.setImageDrawable(getResources().getDrawable(R.drawable.card_promo_pressed));
                tvCardNoExpert.setTextColor(getResources().getColor(R.color.colorWhite));
                imgCardExpert.setImageDrawable((getResources().getDrawable(R.drawable.card_with_chat_default)));
                tvCardExpert.setTextColor(getResources().getColor(R.color.colorWhiteTrans));
                break;
            case R.id.rl_card_with_expert:
                scNoExpert.setVisibility(View.GONE);
                scExpert.setVisibility(View.VISIBLE);
                imgCardNoExpert.setImageDrawable(getResources().getDrawable(R.drawable.card_promo_default));
                tvCardNoExpert.setTextColor(getResources().getColor(R.color.colorWhiteTrans));
                imgCardExpert.setImageDrawable((getResources().getDrawable(R.drawable.card_with_chat_pressed)));
                tvCardExpert.setTextColor(getResources().getColor(R.color.colorWhite));
                break;
        }
    }
}
