package com.fininsight.littlehero;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.busProviderCallback.BusProvider;
import com.fininsight.littlehero.busProviderCallback.ClassForBusProvider;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.ErrorModel;
import com.fininsight.littlehero.models.postModels.ChannelsModel;
import com.fininsight.littlehero.models.postModels.PostModelTest;
import com.fininsight.littlehero.photos.ProcessingPhotos;
import com.fininsight.littlehero.pickerViewForPost.MyOptionsPickerView;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ErrorUtils;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetChannelsClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.NewPostCreateClient;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPostActivity extends AppCompatActivity implements View.OnClickListener{
    private RelativeLayout rlBack, rlSend;
    private ImageView addPhotoPost;
    private LinearLayout topLinearLayout;
    private int whatPhotoCount = 0;
    private TextView tvGroupPost;
    private String mCurrentPhotoPath;
    private Bitmap bmpToServer;
    private Bitmap bmpPostPhoto;
    private int SELECT_FILE = 1;
    private MyOptionsPickerView singlePicker;
    private View vMasker;
    private AccessToken accessToken;
    private ArrayList<String> items;
    private ProcessingPhotos mProcessingPhotos;
    private Call<PostModelTest> call;
    private EditText edtPostContent;
    private String channelId;
    private ChannelsModel getUserModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);
        vMasker = findViewById(R.id.vMasker);
        mProcessingPhotos = new ProcessingPhotos(NewPostActivity.this);
        rlBack = (RelativeLayout) findViewById(R.id.rl_back);
        rlSend = (RelativeLayout) findViewById(R.id.rl_send);
        addPhotoPost = (ImageView) findViewById(R.id.add_photo_post);
        edtPostContent = (EditText) findViewById(R.id.edt_post_comment);
        tvGroupPost = (TextView) findViewById(R.id.tv_group_post);
        HorizontalScrollView scrollView = (HorizontalScrollView) findViewById(R.id.sc_photo_post);
        topLinearLayout = new LinearLayout(NewPostActivity.this);
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        scrollView.addView(topLinearLayout);
        addPhotoPost.setOnClickListener(this);
        rlBack.setOnClickListener(this);
        rlSend.setOnClickListener(this);
        tvGroupPost.setOnClickListener(this);
        singlePicker = new MyOptionsPickerView(NewPostActivity.this);
        accessToken = AccessTokenReturnObject.accessTokenReturn(this);
//        if (GlobalValue.userModel!=null) {
//            initValueUI(GlobalValue.userModel);
//        } else {
        GetChannelsClient getChannelsClient = ServiceGenerator.createService(GetChannelsClient.class, accessToken, this);
        Call<ChannelsModel>call = getChannelsClient.getChanel();
        call.enqueue(new Callback<ChannelsModel>() {
            @Override
            public void onResponse(Call<ChannelsModel> call, Response<ChannelsModel> response) {
                getUserModels = response.body();
                if (response.isSuccessful()) {
                    for (int i = 0;i<getUserModels.getChannels().size();i++) {
                        items.add(getUserModels.getChannels().get(i).getTitle());
                    }
                    tvGroupPost.setText(items.get(0));
                    channelId = getUserModels.getChannels().get(0).get_id();

                }
            }
            @Override
            public void onFailure(Call<ChannelsModel> call, Throwable t) {
            }
        });
        items = new ArrayList<>();
        singlePicker.setPicker(items);
        singlePicker.setTitle("Выберите рубрику");
        singlePicker.setCyclic(false);
        singlePicker.setSelectOptions(0);
        singlePicker.setOnoptionsSelectListener(new MyOptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                //  singleTVOptions.setText("Single Picker " + items.get(options1));
                channelId = getUserModels.getChannels().get(options1).get_id();
                tvGroupPost.setText(items.get(options1));
                vMasker.setVisibility(View.GONE);
            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_send:
                postNewChildren();
                break;
            case R.id.rl_back:
                finish();
                break;
            case R.id.tv_group_post:
                singlePicker.show();
                break;
            case R.id.add_photo_post:
                selectImage();
                break;
        }
    }
    //Для создания ImageView
    private void photoToPostAddImageView() {
        final RelativeLayout relativeLayout = new RelativeLayout(NewPostActivity.this);
        final RoundedImageView imageView = new RoundedImageView (NewPostActivity.this);
        final ImageView imageViewDelete = new ImageView(NewPostActivity.this);
        imageView.setTag(whatPhotoCount);
        imageView.setCornerRadius(10);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageViewDelete.setTag(whatPhotoCount);
        imageView.setImageBitmap(bmpPostPhoto);
        imageViewDelete.setImageResource(R.drawable.delete_image_post);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(getPx(95), getPx(95));
        lp.setMargins(getPx(10),getPx(10),getPx(10),0);
        RelativeLayout.LayoutParams lpDelete = new RelativeLayout.LayoutParams(getPx(25), getPx(25));
        lpDelete.setMargins(getPx(87),getPx(3),0,0);
        relativeLayout.addView(imageView,lp);
        relativeLayout.addView(imageViewDelete,lpDelete);
        topLinearLayout.addView(relativeLayout);
        imageViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topLinearLayout.removeView((View) v.getParent());
                Log.e("Tag",""+imageView.getTag());
                whatPhotoCount--;
            }
        });
        whatPhotoCount ++;
    }

    //set dp calculate automatically
    public int getPx(int dimensionDp) {
        float density = getResources().getDisplayMetrics().density;
        return (int) (dimensionDp * density + 0.5f);
    }
    private void selectImage() {
        final CharSequence[] items = {"Сделать фото", "Выбрать фото из галереи", "Отмена"};
        AlertDialog.Builder builder = new AlertDialog.Builder(NewPostActivity.this);
        builder.setTitle("Добавьте фото!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Сделать фото")) {
                    if (ActivityCompat.checkSelfPermission(NewPostActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(NewPostActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                            File photoFile = null;
                            try {
                                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                photoFile = mProcessingPhotos.createImageFile();
                                Uri contentUri = Uri.fromFile(photoFile);
                                mediaScanIntent.setData(contentUri);
                                sendBroadcast(mediaScanIntent);
                            } catch (IOException ignored) {
                            }
                            if (photoFile != null) {
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                                startActivityForResult(takePictureIntent, 0);
                            }
                        }
                    }
                } else if (items[item].equals("Выбрать фото из галереи")) {
                    if (ActivityCompat.checkSelfPermission(NewPostActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(NewPostActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                    }
                } else if (items[item].equals("Отмена")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            int REQUEST_CAMERA = 0;
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                bmpPostPhoto = bm;
                rotateImageToServer(mCurrentPhotoPath);
                photoToPostAddImageView();
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                bmpPostPhoto = bm;
                mCurrentPhotoPath = selectedImagePath;
                rotateImageToServer(selectedImagePath);
                photoToPostAddImageView();
            }
        } else {
            File fdelete = new File(mCurrentPhotoPath);
            if (fdelete.exists()) {
                boolean deleted = fdelete.delete();
                if (deleted) {
                    Log.d("camera", "deleted");
                    this.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(mCurrentPhotoPath))));
                } else {
                    Log.d("camera", "not deleted");
                }
            }
        }
    }

    public void rotateImageToServer(String path) {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert ei != null;
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_270:
                bmpToServer = rotateImage(bmpPostPhoto, 270);
                break;
            case 1:
                bmpToServer = bmpPostPhoto;
                break;
            case 0:
                bmpToServer = bmpPostPhoto;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                bmpToServer = rotateImage(bmpPostPhoto, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bmpToServer = rotateImage(bmpPostPhoto, 180);
                break;
        }
    }

    public Bitmap rotateImage(Bitmap source, float angle) {
        Bitmap retVal;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        return retVal;
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                            photoFile = mProcessingPhotos.createImageFile();
                            Uri contentUri = Uri.fromFile(photoFile);
                            mediaScanIntent.setData(contentUri);
                            sendBroadcast(mediaScanIntent);
                        } catch (IOException ignored) {
                        }
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, 0);
                        }
                    }
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                }
                break;
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }
    private void postNewChildren() {
        NewPostCreateClient postClient = ServiceGenerator.createService(NewPostCreateClient.class, accessToken, this);
        if (mCurrentPhotoPath != null) {
            File destination = new File(mProcessingPhotos.compressImage(mCurrentPhotoPath));
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), destination);
            MultipartBody.Part body = MultipartBody.Part.createFormData("photos", destination.getName(), requestFile);
            RequestBody channels = RequestBody.create(MediaType.parse("text/plain"), channelId);
            RequestBody message = RequestBody.create(MediaType.parse("text/plain"), edtPostContent.getText().toString());
            call = postClient.newPostAdd(channels, message, body);
        } else {
            call = postClient.newPostAdd(channelId, edtPostContent.getText().toString());
        }
        call.enqueue(new Callback<PostModelTest>() {
            @Override
            public void onResponse(Call<PostModelTest> call, Response<PostModelTest> response) {
                if (response.errorBody() != null) {
                    ErrorModel error = ErrorUtils.parseError(response);
                    String errorText = error.status();
                }
                if (response.isSuccessful()) {
                    BusProvider.getInstance().post(new ClassForBusProvider());
                    finish();
                }
            }
            @Override
            public void onFailure(Call<PostModelTest> call, Throwable t) {
            }
        });

    }

}
