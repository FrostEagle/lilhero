package com.fininsight.littlehero.menu_tab_four;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.R;
import com.fininsight.littlehero.adapter.PostRecyclerAdapter;
import com.fininsight.littlehero.busProviderCallback.BusProvider;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.DeletePostModel;
import com.fininsight.littlehero.models.postModels.PostModel;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.delete.DeletePostsClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetPostsProfileClient;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyPostFragment extends Fragment implements View.OnClickListener {
    private View view;
    private FragmentTransaction fragTransaction;
    private RecyclerView rvPost;
    private List<PostModel> listPost;
    private PostRecyclerAdapter postAdapter;
    private AccessToken accessToken;
    private RelativeLayout rlBack;
    private Bundle args;
    private TextView tvUserName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_post, container, false);
        args = getArguments();
        rvPost = (RecyclerView) view.findViewById(R.id.rv_post);
        rlBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        tvUserName = (TextView) view.findViewById(R.id.tv_user_name);
        tvUserName.setText(args.getString("login"));
        listPost = new ArrayList<>();
        BusProvider.getInstance().register(this);
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());
        getPosts();
        rlBack.setOnClickListener(this);
        return view;
    }

    private void getPosts() {
        GetPostsProfileClient getPosts = ServiceGenerator.createService(GetPostsProfileClient.class, accessToken, getContext());
        Call<ArrayList<PostModel>> call = getPosts.getPost(args.getString("id"),1);
        call.enqueue(new Callback<ArrayList<PostModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PostModel>> call, Response<ArrayList<PostModel>> response) {
                if (response.isSuccessful()) {
                    listPost = response.body();
                    postAdapter = new PostRecyclerAdapter(listPost, getContext());
                    rvPost.setAdapter(postAdapter);
                    rvPost.setLayoutManager(new LinearLayoutManager(getContext()));
                }
            }
            @Override
            public void onFailure(Call<ArrayList<PostModel>> call, Throwable t) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        BusProvider.getInstance().unregister(this);
    }
    private void postFollow(String id) {
        DeletePostsClient deleteClient = ServiceGenerator.createService(DeletePostsClient.class, accessToken, getContext());
        Call<DeletePostModel> callDevice = deleteClient.deletePost(id);
        callDevice.enqueue(new Callback<DeletePostModel>() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<DeletePostModel> call, Response<DeletePostModel> response) {
                ResponseBody errorBo = response.errorBody();
                int responseCode = response.code();
                if (response.isSuccessful()) {
                    getPosts();
                }
            }
            @Override
            public void onFailure(Call<DeletePostModel> call, Throwable t) {
            }
        });
    }

}
