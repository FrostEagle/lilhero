package com.fininsight.littlehero;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fininsight.littlehero.adapter.ChildrenFromSettingsAdapter;
import com.fininsight.littlehero.application.GlobalValue;
import com.fininsight.littlehero.busProviderCallback.BusProvider;
import com.fininsight.littlehero.busProviderCallback.OpenEditChildrenClass;
import com.fininsight.littlehero.entering.SplashScreen;
import com.fininsight.littlehero.extras.Constants;
import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.ErrorModel;
import com.fininsight.littlehero.models.UserModel;
import com.fininsight.littlehero.models.babyModels.ChildModel;
import com.fininsight.littlehero.photos.ProcessingPhotos;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ErrorUtils;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.get.GetUserClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.EditUserClient;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.fininsight.littlehero.extras.Constants.SELECT_FILE;

public class MySettingsFragment extends Fragment implements View.OnClickListener{
    private ImageView imgVkSocial, imgFbSocial, imgOdnoklassnikiSocial, imgInstagramSocial;
    private RelativeLayout rlBack, rlSettingsSave;
    private TextView tvExitApp;
    private RelativeLayout rlChangePassword, rlKidAdd, rlEditKid, rlContinuePregnancy;
    private FragmentTransaction fragTransaction;
    private RelativeLayout rlPaid;
    private View view;
    private SharedPreferences prefs;
    private AccessToken accessToken;
    private GetUserClient getUserClient;
    private Call<UserModel> call;
    private EditText edtLogin, edtEmail, edtName, edtAbout;
    private String mCurrentPhotoPath, mCurrentPhotoPathAvatar, mCurrentPhotoPathCover;
    private CircleImageView imgAvatar;
    private com.makeramen.roundedimageview.RoundedImageView imgBackground;
    private ProcessingPhotos mProcessingPhotos;
    //if 1-cover 0-avatar
    private int imageWhatPhoto = 0;
    private List<ChildModel> childrenList;
    private RecyclerView rvKids;
    private ChildrenFromSettingsAdapter adapter;
    private boolean anychange, withPhoto, withCover, withPhotoCover;
    private String androidId;
    private ChildModel pregnancyModel;
    private TextView tvContinuePregnancy;
    private TextView tvPregnantStatuc;
    private UserModel getUserModels;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_my_settings, container, false);
        initUI();
        prefs = getContext().getSharedPreferences(Constants.TOKENREFRESH, Context.MODE_PRIVATE);
        androidId = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        accessToken = AccessTokenReturnObject.accessTokenReturn(getContext());
        mProcessingPhotos = new ProcessingPhotos(getContext());
//        if (GlobalValue.userModel!=null) {
            initValueUI(GlobalValue.userModel);
//        } else {
            getUserClient = ServiceGenerator.createService(GetUserClient.class, accessToken, getContext());
            call = getUserClient.getUserModels(androidId);
            call.enqueue(new Callback<UserModel>() {
                @Override
                public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                    getUserModels = response.body();
                    if (response.isSuccessful()) {
                        initValueUI(getUserModels);
                    }
                }
                @Override
                public void onFailure(Call<UserModel> call, Throwable t) {
                }
            });
      //  }
        childrenList = new ArrayList<>();
        rvKids.setLayoutManager(new LinearLayoutManager(getContext()));
        rlChangePassword.setOnClickListener(this);
        tvExitApp.setOnClickListener(this);
        rlBack.setOnClickListener(this);
        imgVkSocial.setOnClickListener(this);
        imgFbSocial.setOnClickListener(this);
        imgOdnoklassnikiSocial.setOnClickListener(this);
        imgInstagramSocial.setOnClickListener(this);
        rlKidAdd.setOnClickListener(this);
        rlEditKid.setOnClickListener(this);
        rlContinuePregnancy.setOnClickListener(this);
        rlPaid.setOnClickListener(this);
        imgAvatar.setOnClickListener(this);
        imgBackground.setOnClickListener(this);
        rlSettingsSave.setOnClickListener(this);
        edtName.addTextChangedListener(changeEdit);
        edtEmail.addTextChangedListener(changeEdit);
        edtLogin.addTextChangedListener(changeEdit);
        edtAbout.addTextChangedListener(changeEdit);

        return view;
    }

    private void initValueUI(UserModel getUserModels) {
       edtName.setText(getUserModels.getUser().getName());
       edtEmail.setText(getUserModels.getUser().getEmail());
       edtLogin.setText(getUserModels.getUser().getLogin());
       edtAbout.setText(getUserModels.getUser().getBio());
        if (getUserModels.getUser().getAvatar_file()!=null) {
            Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/avatars/"+getUserModels.getUser().getLogin()).into(imgAvatar);
        } else  Picasso.with(getContext()).load(R.drawable.main_avatar).into(imgAvatar);
        if (getUserModels.getUser().getCover_file()!=null) {
            Picasso.with(getContext()).load("http://little-hero.gmg-spb.ru/images/covers/"+getUserModels.getUser().getLogin()).into(imgBackground);
        } else  Picasso.with(getContext()).load(R.drawable.cover_profile_main).into(imgBackground);
        childrenList = getUserModels.getUser().getChilds();
        for (int i = 0; i<childrenList.size(); i++) {
            if (childrenList.get(i).isPregnancy()) {
                pregnancyModel = childrenList.get(i);
                childrenList.remove(i);
            }
        }
        if (pregnancyModel!=null) {
            tvContinuePregnancy.setText("Завершить беременность");
            tvPregnantStatuc.setText(String.valueOf(pregnancyModel.getDay() / 7) + " неделя");
        } else tvContinuePregnancy.setText("Добавить беременность");
        adapter = new ChildrenFromSettingsAdapter (childrenList, getContext());
        rvKids.setAdapter(adapter);
    }
    private void initUI() {
        rlSettingsSave = (RelativeLayout) view.findViewById(R.id.rl_setting_save);
        tvPregnantStatuc = (TextView) view.findViewById(R.id.tv_pregnant_status);
        tvContinuePregnancy = (TextView) view.findViewById(R.id.tv_continue_pregnancy);
        rvKids = (RecyclerView) view.findViewById(R.id.rv_kids);
        imgAvatar = (CircleImageView) view.findViewById(R.id.img_avatar);
        imgBackground = (com.makeramen.roundedimageview.RoundedImageView) view.findViewById(R.id.img_photo_background_profile);
        edtAbout = (EditText) view.findViewById(R.id.edt_about_me_settings);
        edtEmail = (EditText) view.findViewById(R.id.edt_email_my_settings);
        edtLogin = (EditText) view.findViewById(R.id.edt_login_my_settings);
        edtName = (EditText) view.findViewById(R.id.edt_my_name_settings);
        rlBack = (RelativeLayout) view.findViewById(R.id.rl_back);
        rlKidAdd = (RelativeLayout) view.findViewById(R.id.rl_add_kid_settings);
        rlPaid = (RelativeLayout) view.findViewById(R.id.rl_paid);
        rlEditKid = (RelativeLayout) view.findViewById(R.id.rl_edit_kid);
        imgVkSocial = (ImageView) view.findViewById(R.id.img_vk_social_settings);
        imgFbSocial = (ImageView) view.findViewById(R.id.img_facebook_social_settings);
        rlContinuePregnancy = (RelativeLayout) view.findViewById(R.id.rl_continue_pregnancy);
        imgOdnoklassnikiSocial = (ImageView) view.findViewById(R.id.img_odnoklassniki_social_settings);
        imgInstagramSocial = (ImageView) view.findViewById(R.id.img_instagram_social_settings);
        tvExitApp = (TextView) view.findViewById(R.id.tv_exip_app);
        rlChangePassword = (RelativeLayout) view.findViewById(R.id.rl_change_password_settings);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                        File photoFile = null;
                        try {
                            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                            photoFile = mProcessingPhotos.createImageFile();
                            Uri contentUri = Uri.fromFile(photoFile);
                            mediaScanIntent.setData(contentUri);
                            getContext().sendBroadcast(mediaScanIntent);
                        } catch (IOException ignored) {
                        }
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, 0);
                        }
                    }
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), Constants.SELECT_FILE);
                }
                break;
        }
    }
    public void selectImage() {
        final CharSequence[] items = {"Сделать фото", "Выбрать фото из галереи", "Отмена"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Добавьте фото!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Сделать фото")) {
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
                            File photoFile = null;
                            try {
                                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                                photoFile = mProcessingPhotos.createImageFile();
                                Uri contentUri = Uri.fromFile(photoFile);
                                mediaScanIntent.setData(contentUri);
                                getContext().sendBroadcast(mediaScanIntent);
                            } catch (IOException ignored) {
                            }
                            if (photoFile != null) {
                                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                                getActivityStarterFragment().startActivityForResult(takePictureIntent, 0);
                            }
                        }
                    }
                } else if (items[item].equals("Выбрать фото из галереи")) {
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        getActivityStarterFragment().startActivityForResult(Intent.createChooser(intent, "Select File"),  Constants.SELECT_FILE);
                    }
                } else if (items[item].equals("Отмена")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private Fragment getActivityStarterFragment() {
        if (getParentFragment() != null) {
            return getParentFragment();
        }
        return this;
    }
    public void callOnActivityResultOnChildFragments(Fragment parent, int requestCode, int resultCode, Intent data) {
        FragmentManager childFragmentManager = parent.getChildFragmentManager();
        if (childFragmentManager != null) {
            List<Fragment> childFragments = childFragmentManager.getFragments();
            if (childFragments == null) {
                return;
            }
            for (Fragment child : childFragments) {
                if (child != null && !child.isDetached() && !child.isRemoving()) {
                    child.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callOnActivityResultOnChildFragments(this, requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            int REQUEST_CAMERA = 0;
            if (requestCode == REQUEST_CAMERA) {
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(mCurrentPhotoPath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                if (imageWhatPhoto==1) {
                    mCurrentPhotoPathCover = mCurrentPhotoPath;
                    Bitmap myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathCover);
                    imgBackground.setImageBitmap(myBitmap);
                } else  {
                    mCurrentPhotoPathAvatar = mCurrentPhotoPath;
                    Bitmap myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPathAvatar);
                    imgAvatar.setImageBitmap(myBitmap);
                }
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(getContext(), selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);
                Bitmap bm;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                bm = BitmapFactory.decodeFile(selectedImagePath, options);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
                mCurrentPhotoPath = selectedImagePath;
                Bitmap myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                if (imageWhatPhoto==1) {
                    mCurrentPhotoPathCover = mCurrentPhotoPath;
                    imgBackground.setImageBitmap(myBitmap);
                } else  {
                    mCurrentPhotoPathAvatar = mCurrentPhotoPath;
                    imgAvatar.setImageBitmap(myBitmap);
                }
            }
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_setting_save:
                postEditUserInfo();
                break;
            case R.id.img_avatar:
                imageWhatPhoto=0;
                selectImage();
                break;
            case R.id.img_photo_background_profile:
                imageWhatPhoto=1;
                selectImage();
                break;
            case R.id.rl_paid:
                startActivity(new Intent(getContext(), PaidActivity.class));
                break;
            case R.id.rl_continue_pregnancy:
                startActivity(new Intent(getContext(), CompletePregnancyActivity.class));
                Activity activity = (Activity) getContext();
                activity.overridePendingTransition( R.anim.animatino_move_from_bottom_activity, R.anim.animation_fadeout);
                break;
            case R.id.rl_change_password_settings:
                ChangePasswordFragment changePass = new ChangePasswordFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, changePass );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.rl_back:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
            case R.id.img_vk_social_settings:
                imgVkSocial.setImageDrawable(getResources().getDrawable(R.drawable.vk_social_pressed));
                break;
            case R.id.img_facebook_social_settings:
                imgFbSocial.setImageDrawable(getResources().getDrawable(R.drawable.facebook_social_pressed));
                break;
            case R.id.img_odnoklassniki_social_settings:
                imgOdnoklassnikiSocial.setImageDrawable(getResources().getDrawable(R.drawable.odnoklassniki_social_pressed));
                break;
            case R.id.img_instagram_social_settings:
                imgInstagramSocial.setImageDrawable(getResources().getDrawable(R.drawable.instagramm_social_pressed));
                break;
            case R.id.tv_exip_app:
                prefs.edit().putBoolean(Constants.oauth_loggedin, true).apply();
                prefs.edit().putString(Constants.oauth_accesstoken, "").apply();
                prefs.edit().putString(Constants.oauth_refreshtoken, "").apply();
                prefs.edit().putString(Constants.oauth_tokentype, "").apply();
                startActivity(new Intent(getContext(), SplashScreen.class));
                getActivity().finish();
                break;
            case R.id.rl_add_kid_settings:
                AddChildrenFromSettingsFragment addChildFrag = new AddChildrenFromSettingsFragment();
                fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.frame_for_fragment, addChildFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                break;
            case R.id.rl_edit_kid:

                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }
    @Subscribe
    public void openMyMenu(OpenEditChildrenClass event) {

        ChildModel model = getUserModels.getUser().getChilds().get(event.getPosition());
        //locationEvents.add(0, event.toString());
        EditChildrenFromSettingsFragment editChildFrag = new EditChildrenFromSettingsFragment();
        Bundle args = new Bundle();
        args.putString("childId", model.get_id());
        args.putString("name", model.getName());
        args.putInt("gender", model.getGender());
        args.putString("date",model.getDate());
        editChildFrag.setArguments(args);
        fragTransaction = getFragmentManager().beginTransaction();
        fragTransaction.replace(R.id.frame_for_fragment, editChildFrag );
        fragTransaction.addToBackStack(null);
        fragTransaction.commit();
    }
    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        call.cancel();
    }

    private void postEditUserInfo() {
        EditUserClient editClient = ServiceGenerator.createService(EditUserClient.class, accessToken, getContext());
        if (mCurrentPhotoPathAvatar != null && mCurrentPhotoPathCover!=null && anychange) {
            File avatar = new File(mProcessingPhotos.compressImage(mCurrentPhotoPathAvatar));
            File cover = new File(mProcessingPhotos.compressImage(mCurrentPhotoPathCover));
            // create RequestBody instance from file
            RequestBody requestFileAvatar = RequestBody.create(MediaType.parse("multipart/form-data"), avatar);
            RequestBody requestFileCover = RequestBody.create(MediaType.parse("multipart/form-data"), cover);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part bodyAvatar = MultipartBody.Part.createFormData("avatar", avatar.getName(), requestFileAvatar);
            MultipartBody.Part bodyCover = MultipartBody.Part.createFormData("cover", cover.getName(), requestFileCover);
            RequestBody login = RequestBody.create(MediaType.parse("text/plain"), edtLogin.getText().toString());
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"),edtEmail.getText().toString());
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), edtName.getText().toString());
            RequestBody bio = RequestBody.create(MediaType.parse("text/plain"), edtAbout.getText().toString());
            call = editClient.editProfileWithPhotoCover(androidId, login, email, name, bio, bodyAvatar, bodyCover);
        } else if (mCurrentPhotoPathAvatar != null && mCurrentPhotoPathCover != null) {
            File avatar = new File(mProcessingPhotos.compressImage(mCurrentPhotoPathAvatar));
            File cover = new File(mProcessingPhotos.compressImage(mCurrentPhotoPathCover));
            // create RequestBody instance from file
            RequestBody requestFileAvatar = RequestBody.create(MediaType.parse("multipart/form-data"), avatar);
            RequestBody requestFileCover = RequestBody.create(MediaType.parse("multipart/form-data"), cover);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part bodyAvatar = MultipartBody.Part.createFormData("avatar", avatar.getName(), requestFileAvatar);
            MultipartBody.Part bodyCover = MultipartBody.Part.createFormData("cover", cover.getName(), requestFileCover);

            call = editClient.editProfileWithOnlyPhotoCover(androidId, bodyAvatar, bodyCover);
        } else if (mCurrentPhotoPathAvatar != null && anychange) {
            File avatar = new File(mProcessingPhotos.compressImage(mCurrentPhotoPathAvatar));
            // create RequestBody instance from file
            RequestBody requestFileAvatar = RequestBody.create(MediaType.parse("multipart/form-data"), avatar);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part bodyAvatar = MultipartBody.Part.createFormData("avatar", avatar.getName(), requestFileAvatar);
            RequestBody login = RequestBody.create(MediaType.parse("text/plain"), edtLogin.getText().toString());
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"),edtEmail.getText().toString());
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), edtName.getText().toString());
            RequestBody bio = RequestBody.create(MediaType.parse("text/plain"), edtAbout.getText().toString());
            call = editClient.editProfileWithPhotoAll(androidId, login, email, name, bio, bodyAvatar);

        } else if (mCurrentPhotoPathCover !=null && anychange) {
            File cover = new File(mProcessingPhotos.compressImage(mCurrentPhotoPathCover));
            // create RequestBody instance from file
            RequestBody requestFileCover = RequestBody.create(MediaType.parse("multipart/form-data"), cover);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part bodyCover = MultipartBody.Part.createFormData("cover", cover.getName(), requestFileCover);
            RequestBody login = RequestBody.create(MediaType.parse("text/plain"), edtLogin.getText().toString());
            RequestBody email = RequestBody.create(MediaType.parse("text/plain"),edtEmail.getText().toString());
            RequestBody name = RequestBody.create(MediaType.parse("text/plain"), edtName.getText().toString());
            RequestBody bio = RequestBody.create(MediaType.parse("text/plain"), edtAbout.getText().toString());
            call = editClient.editProfileWithCoverAll(androidId, login, email, name, bio, bodyCover);

        }else if (mCurrentPhotoPathAvatar != null) {
            File avatar = new File(mProcessingPhotos.compressImage(mCurrentPhotoPathAvatar));
            // create RequestBody instance from file
            RequestBody requestFileAvatar = RequestBody.create(MediaType.parse("multipart/form-data"), avatar);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part bodyAvatar = MultipartBody.Part.createFormData("avatar", avatar.getName(), requestFileAvatar);
            call = editClient.editProfileWithPhoto(androidId, bodyAvatar);

        } else if (mCurrentPhotoPathCover !=null ) {
            File cover = new File(mProcessingPhotos.compressImage(mCurrentPhotoPathCover));
            // create RequestBody instance from file
            RequestBody requestFileCover = RequestBody.create(MediaType.parse("multipart/form-data"), cover);
            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part bodyCover = MultipartBody.Part.createFormData("cover", cover.getName(), requestFileCover);

            call = editClient.editProfileWithCover(androidId, bodyCover);

        }

        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                ResponseBody errorBo = response.errorBody();
                int recponce = response.code();
                if (response.errorBody() != null) {
                    ErrorModel error = ErrorUtils.parseError(response);
//                                String errorText = error.status();
//                                if (errorText.equals("invalid_credentials")) {
                    //                              }
                }
                if (response.isSuccessful()) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
            }
        });

    }
    private TextWatcher changeEdit = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }
        @Override
        public void afterTextChanged(Editable s) {
           anychange = true;
        }
    };
}
