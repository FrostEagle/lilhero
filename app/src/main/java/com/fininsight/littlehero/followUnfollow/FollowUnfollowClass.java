package com.fininsight.littlehero.followUnfollow;

import android.content.Context;

import com.fininsight.littlehero.models.AccessToken;
import com.fininsight.littlehero.models.DeletePostModel;
import com.fininsight.littlehero.models.FollowerModel;
import com.fininsight.littlehero.serverAPI.AccessTokenReturnObject;
import com.fininsight.littlehero.serverAPI.ServiceGenerator;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.delete.UnFollowClient;
import com.fininsight.littlehero.serverAPI.classForRequestInterface.post.FollowClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by FrostEagle on 01.11.2016.
 */

public class FollowUnfollowClass {
    private AccessToken accessToken;
    private Context context;

    public FollowUnfollowClass(Context context) {
        this.context = context;
        accessToken = AccessTokenReturnObject.accessTokenReturn(context);
    }

    public void followUser(String id) {

        FollowClient followClient = ServiceGenerator.createService(FollowClient.class, accessToken, context);
        Call<FollowerModel> callDevice = followClient.postFollower(id);
        callDevice.enqueue(new Callback<FollowerModel>() {
            @Override
            public void onResponse(Call<FollowerModel> call, Response<FollowerModel> response) {
                ResponseBody errorBo = response.errorBody();
                int responseCode = response.code();
                if (response.isSuccessful()) {
                }
            }

            @Override
            public void onFailure(Call<FollowerModel> call, Throwable t) {
            }
        });
    }
    public void unfollowUser(String id) {
        UnFollowClient followClient = ServiceGenerator.createService(UnFollowClient.class, accessToken, context);
        Call<DeletePostModel> callDevice = followClient.postFollower(id);
        callDevice.enqueue(new Callback<DeletePostModel>() {
            @Override
            public void onResponse(Call<DeletePostModel> call, Response<DeletePostModel> response) {
                ResponseBody errorBo = response.errorBody();
                int responseCode = response.code();
                if (response.isSuccessful()) {
                }
            }

            @Override
            public void onFailure(Call<DeletePostModel> call, Throwable t) {
            }
        });
    }
}
